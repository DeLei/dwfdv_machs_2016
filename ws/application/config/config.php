<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------
 | Webservice - Obter TES e ST de produtos
 |--------------------------------------------------------------------------
 |
 | Descri��o:
 |
 */

$config['webservice'] = array (
	'IMPOSTOS'	=> '',
	'TES'	=> '',
	'MV'	=> ''
);

//
define('PERIODO_DADOS', strtotime('-6 months'));

define('PERIODO_DADOS_RELATORIOS', strtotime('-6 months'));

/*
|--------------------------------------------------------------------------
| Diret�rios para escrita
|--------------------------------------------------------------------------
|
| ws\pacotes
| ws\application\logs\pacotes
|
*/

/*
|--------------------------------------------------------------------------
| Vers�o do Web Service
|--------------------------------------------------------------------------
|
| Utilizado para atualizar a OFFLINE quando a vers�o for alterada
|
*/
define('VERSAO', '1.0.0.0');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
$config['base_url']	= 'http://localhost/dwfdv/ws/';


/*
 |--------------------------------------------------------------------------
 | Empresas Matriz do PROTHEUS
 |--------------------------------------------------------------------------
 |
 | Descri��o:
 |
 */
$config['empresa_matriz'] = '1';


/*
 |--------------------------------------------------------------------------
 | Empresas do PROTHEUS
 |--------------------------------------------------------------------------
 |
 | Descri��o:
 |
 */
$config['empresas'] = array (
	'1'	=> 'Machs Premium'
);

/*
 |--------------------------------------------------------------------------
 | Empresas. Unidades, Filiais no campo Filial da Tabela
 |--------------------------------------------------------------------------
 |
 |	Definir se ser� utilizado o campo de compartilhamento;
 |
 */
$config['euf'] = TRUE;

/*
 |--------------------------------------------------------------------------
 | Empresas do PROTHEUS usadas na EUF (Usadas somente quando existir EUF e regra de empresas apenas por 1 tabela)
 |--------------------------------------------------------------------------
 |
 | Descri��o:
 |
 */
$config['empresas_euf'] = NULL;

/*
 |--------------------------------------------------------------------------
 | Empresas. Unidades, Filiais no campo Filial da Tabela
 |--------------------------------------------------------------------------
 |
 | A posi��o come�a da esquerda para a direita. 
 | 1� Exemplo:
 | 		1� - E, 2� - U, 3� - F = array('E', 'U', 'F')
 |
 | 2� Exemplo:
 | 		1� - U, 2� - F, 3� - E = array('U', 'F', 'E')
 |
 */
$config['posicao_euf'] = array('E', 'F', 'U');

/*
 |--------------------------------------------------------------------------
 | Tabela de Pedidos da DEVELOPWEB
 |--------------------------------------------------------------------------
 |
 | Descri��o: Tabela onde ser�o gravados os pedidos tempor�rios do PORTAL
 |
 */
$config['pedidos_dw'] = array (
	'tabela' 	=> 'pedido_temporario',
	'campo'		=> ''
);

/*
 |--------------------------------------------------------------------------
 | Tabela de prospects da DEVELOPWEB
 |--------------------------------------------------------------------------
 |
 | Descri��o: Tabela onde ser�o gravados os prospects do PORTAL
 |
 */
$config['prospects'] = array (
	'tabela' 	=> 'prospect',
	'campo'		=> ''
);

/*
|--------------------------------------------------------------------------
| Gerar Arquivo
|--------------------------------------------------------------------------
|
| Criar arquivos na exporta��o
|
|	Default: TRUE
|
*/
$config['gerar_arquivo'] = TRUE;

/*
|--------------------------------------------------------------------------
| N�mero de "P�ginas ou Pacotes"
|--------------------------------------------------------------------------
|
| Utilizado para retornar o n�mero de dados por pacote
|
|	http://localhost/clientes/exportar?pacote=3 (O WS ir� retornar 50 registros do pacote 3)
|
|
*/
define('POR_PACOTE', 1000);

/*
|--------------------------------------------------------------------------
| Tempo de atualiza��o para caada Pacote
|--------------------------------------------------------------------------
|
| Utilizado para atualizar os pacotes json em um determinado tempo
| (7 * 24 * 60 * 60) => 7 Dias; 24 Horas; 60 minutos; 60 segundos
|
*/
$config['pacotes'] = array(
	'clientes' 						=> (1 * 1 * 1 * 60),
	'pedidos_pendentes' 			=> (1 * 1 * 1 * 60),
	'pedidos_processados' 			=> (1 * 1 * 1* 60),
	'pendencias_usuarios' 			=> (1 * 1 * 1 * 60),
	'historico_clientes' 			=> (1 * 1 * 1 * 60),
	'pendencias' 					=> (1 * 1 * 1 * 60),
	'pendencias_mensagens' 			=> (1 * 1 * 1 * 60),
	'agenda' 						=> (1 * 1 * 1 * 60),
	'titulos' 						=> (1 * 1 * 5 * 60),
	'notas_fiscais' 				=> (1 * 1 * 1 * 60),
	'orcamentos' 					=> (1 * 1 * 1 * 60),
	'prospects' 					=> (1 * 1 * 1 * 60),	
	'historico_prospects' 			=> (1 * 1 * 1 * 60),
	'eventos'			 			=> (1 * 1 * 1 * 60),
	'pedido_parcela'				=> (1 * 1 * 1 * 60),
	'representante_tabela_preco' 	=> (1 * 1 * 1 * 60)
	
);

/*
|--------------------------------------------------------------------------
| Pacotes Unicos
|--------------------------------------------------------------------------
|
| Pacotes que n�o est�o ligados por representantes. 
| Pacotes para todos os representantes
|  
|
*/
$config['pacotes_unicos'] = array(
	
	'empresas' 						=> (1 * 1 * 60 * 60),
	'filiais' 						=> (1 * 1 * 60 * 60),
	'condicoes_pagamento'			=> (1 * 1 * 1 * 60),
	'prazo_parcela'					=> (1 * 1 * 1 * 60),
	'formas_pagamento' 				=> (1 * 1 * 1 * 60),
	'noticias' 						=> (1 * 1 * 1 * 60),
	'produtos' 						=> (1 * 1 * 1 * 60),

	'tabelas_preco' 				=> (1 * 1 * 1 * 60),
	'derivacoes_produtos' 			=> (1 * 1 * 1 * 60),
	'municipios' 					=> (30 * 24 * 60 * 60),
	'transportadoras' 				=> (30 * 1 * 30 * 60),
	'classe_vendas_produtos'		=> (1 * 1 * 1 * 60),
	'acrescimo_familia'				=> (1 * 1 * 1 * 60),
	'desconto_familia'				=> (1 * 1 * 1 * 60),
	
	//'regra_desconto' 				=> (1 * 1 * 30 * 60),
	//'regioes_vendas'				=> (1 * 1 * 30 * 60),
	//'motivo_nao_venda'			=> (1 * 1 * 30 * 60)
);

/*
|--------------------------------------------------------------------------
| Hora da primeira atualiza��o 
|--------------------------------------------------------------------------
|
| Utilizado para definir a hora da cria��o dos arquivos (Apenas funciona na cria��o dos arquivos)
|
*/
$config['hora_atualizacao'] = '08:00'; //24horas

//----------------------------------------------------------------------------------------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| Index File
|--------------------------------------------------------------------------
|
| Typically this will be your index.php file, unless you've renamed it to
| something else. If you are using mod_rewrite to remove the page set this
| variable so that it is blank.
|
*/
$config['index_page'] = '';
//$config['index_page'] = 'index.php';

/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of 'AUTO' works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'			Default - auto detects
| 'PATH_INFO'		Uses the PATH_INFO
| 'QUERY_STRING'	Uses the QUERY_STRING
| 'REQUEST_URI'		Uses the REQUEST_URI
| 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
|
*/
$config['uri_protocol']	= 'AUTO';

/*
|--------------------------------------------------------------------------
| URL suffix
|--------------------------------------------------------------------------
|
| This option allows you to add a suffix to all URLs generated by CodeIgniter.
| For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/urls.html
*/

$config['url_suffix'] = '';

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language']	= 'english';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = 'UTF-8';

/*
|--------------------------------------------------------------------------
| Enable/Disable System Hooks
|--------------------------------------------------------------------------
|
| If you would like to use the 'hooks' feature you must enable it by
| setting this variable to TRUE (boolean).  See the user guide for details.
|
*/
$config['enable_hooks'] = FALSE;


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = 'MY_';


/*
|--------------------------------------------------------------------------
| Allowed URL Characters
|--------------------------------------------------------------------------
|
| This lets you specify with a regular expression which characters are permitted
| within your URLs.  When someone tries to submit a URL with disallowed
| characters they will get a warning message.
|
| As a security measure you are STRONGLY encouraged to restrict URLs to
| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
|
| Leave blank to allow all characters -- but only if you are insane.
|
| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
|
*/
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';


/*
|--------------------------------------------------------------------------
| Enable Query Strings
|--------------------------------------------------------------------------
|
| By default CodeIgniter uses search-engine friendly segment based URLs:
| example.com/who/what/where/
|
| By default CodeIgniter enables access to the $_GET array.  If for some
| reason you would like to disable it, set 'allow_get_array' to FALSE.
|
| You can optionally enable standard query string based URLs:
| example.com?who=me&what=something&where=here
|
| Options are: TRUE or FALSE (boolean)
|
| The other items let you set the query string 'words' that will
| invoke your controllers and its functions:
| example.com/index.php?c=controller&m=function
|
| Please note that some of the helpers won't work as expected when
| this feature is enabled, since CodeIgniter is designed primarily to
| use segment based URLs.
|
*/
$config['allow_get_array']		= TRUE;
$config['enable_query_strings'] = TRUE;
$config['controller_trigger']	= 'c';
$config['function_trigger']		= 'm';
$config['directory_trigger']	= 'd'; // experimental not currently in use

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = 0;

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| application/logs/ folder. Use a full server path with trailing slash.
|
*/
$config['log_path'] = '';

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = 'Y-m-d H:i:s';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/cache/ folder.  Use a full server path with trailing slash.
|
*/
$config['cache_path'] = '';

/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Session class you
| MUST set an encryption key.  See the user guide for info.
|
*/
$config['encryption_key'] = 'developweb_ws';

/*
|--------------------------------------------------------------------------
| Session Variables
|--------------------------------------------------------------------------
|
| 'sess_cookie_name'		= the name you want for the cookie
| 'sess_expiration'			= the number of SECONDS you want the session to last.
|   by default sessions last 7200 seconds (two hours).  Set to zero for no expiration.
| 'sess_expire_on_close'	= Whether to cause the session to expire automatically
|   when the browser window is closed
| 'sess_encrypt_cookie'		= Whether to encrypt the cookie
| 'sess_use_database'		= Whether to save the session data to a database
| 'sess_table_name'			= The name of the session database table
| 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
| 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
| 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
|
*/
$config['sess_cookie_name']		= 'ci_session';
$config['sess_expiration']		= 7200;
$config['sess_expire_on_close']	= FALSE;
$config['sess_encrypt_cookie']	= FALSE;
$config['sess_use_database']	= FALSE;
$config['sess_table_name']		= 'ci_sessions';
$config['sess_match_ip']		= FALSE;
$config['sess_match_useragent']	= TRUE;
$config['sess_time_to_update']	= 300;

/*
|--------------------------------------------------------------------------
| Cookie Related Variables
|--------------------------------------------------------------------------
|
| 'cookie_prefix' = Set a prefix if you need to avoid collisions
| 'cookie_domain' = Set to .your-domain.com for site-wide cookies
| 'cookie_path'   =  Typically will be a forward slash
|
*/
$config['cookie_prefix']	= '';
$config['cookie_domain']	= '';
$config['cookie_path']		= '/';

/*
|--------------------------------------------------------------------------
| Global XSS Filtering
|--------------------------------------------------------------------------
|
| Determines whether the XSS filter is always active when GET, POST or
| COOKIE data is encountered
|
*/
$config['global_xss_filtering'] = FALSE;

/*
|--------------------------------------------------------------------------
| Cross Site Request Forgery
|--------------------------------------------------------------------------
| Enables a CSRF cookie token to be set. When set to TRUE, token will be
| checked on a submitted form. If you are accepting user data, it is strongly
| recommended CSRF protection be enabled.
|
| 'csrf_token_name' = The token name
| 'csrf_cookie_name' = The cookie name
| 'csrf_expire' = The number in seconds the token should expire.
*/
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;

/*
|--------------------------------------------------------------------------
| Output Compression
|--------------------------------------------------------------------------
|
| Enables Gzip output compression for faster page loads.  When enabled,
| the output class will test whether your server supports Gzip.
| Even if it does, however, not all browsers support compression
| so enable only if you are reasonably sure your visitors can handle it.
|
| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
| means you are prematurely outputting something to your browser. It could
| even be a line of whitespace at the end of one of your scripts.  For
| compression to work, nothing can be sent before the output buffer is called
| by the output class.  Do not 'echo' any values with compression enabled.
|
*/
$config['compress_output'] = FALSE;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are 'local' or 'gmt'.  This pref tells the system whether to use
| your server's local time as the master 'now' reference, or convert it to
| GMT.  See the 'date helper' page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = 'local';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = FALSE;


/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '';


/* End of file config.php */
/* Location: ./application/config/config.php */