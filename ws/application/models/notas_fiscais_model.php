<?php

class Notas_fiscais_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
        
        $this->db_cliente = $this->load->database('db_cliente', TRUE);
        
    }
	
	/**
	* Metódo:		exportar_notas_fiscais
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de notas fiscais
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web / William Reis Fernandes
	* 
	*/
	function exportar_notas_fiscais($id = NULL, $pacote = NULL, $codigo_representante)
	{
		
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'nota_fiscal.numero', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar titulos
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web / William Reis Fernandes
	* 
	*/
	function consulta($dados = NULL)
	{
	
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
	
		// * Retornar todos os campos
		$select_notas_fiscais		= select_all($this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais'], 'nota');
		$select_itens_notas_fiscais	= select_all($this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais'], 'item');
		$select_produtos			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_clientes 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_condicao_pagamento	= select_all($this->_db_cliente['tabelas']['condicoes_pagamento'], $this->_db_cliente['campos']['condicoes_pagamento'], 'condicao_pagamento');
		$select_formas_pagamento	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras		= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		
		$select = array_merge(
			$select_notas_fiscais,
			$select_itens_notas_fiscais,
			$select_produtos, 
			$select_clientes,
			$select_condicao_pagamento,
			$select_formas_pagamento,
			$select_transportadoras
		);
		
		$select[] = "(RTRIM(LTRIM(cast(".$this->_db_cliente['campos']['notas_fiscais']['codigo']." as varchar(30))))||'-'||RTRIM(LTRIM(cast(".$this->_db_cliente['campos']['notas_fiscais']['serie']." as varchar(30))))) as codigo_generico";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial'], $codigo_empresa);
		
		//-----------------------------------------------	
		
		// Join
		//-----------------------------------------------		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['itens_notas_fiscais'], 
			euf(
				$this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['codigo'] . " AND " .
			$this->_db_cliente['campos']['itens_notas_fiscais']['serie'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['serie']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['itens_notas_fiscais']['codigo_produto']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['tabelas']['formas_pagamento'].'.'.$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['condicao_pagamento']
		, 'left outer');
		
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['condicoes_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['condicoes_pagamento'], $this->_db_cliente['campos']['condicoes_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['tabelas']['condicoes_pagamento'].'.'.$this->_db_cliente['campos']['condicoes_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['condicao_pagamento']
		, 'left outer');
		
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['tabelas']['clientes'] .'.'. $this->_db_cliente['campos']['clientes']['codigo'] . " = " . $this->_db_cliente['tabelas']['notas_fiscais'] .'.'.  $this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] . " AND " .
			$this->_db_cliente['campos']['clientes']['loja'] . " = " . $this->_db_cliente['tabelas']['notas_fiscais'] .'.'.  $this->_db_cliente['campos']['notas_fiscais']['loja_cliente']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['representantes'], 
			euf(
				$this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['tabelas']['representantes'].'.'.$this->_db_cliente['campos']['representantes']['codigo'] . " = " . $this->_db_cliente['tabelas']['notas_fiscais'] .'.'.  $this->_db_cliente['campos']['notas_fiscais']['codigo_representante']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['tabelas']['transportadoras'].'.'.$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['tabelas']['notas_fiscais'] .'.'.  $this->_db_cliente['campos']['notas_fiscais']['codigo_transportadora'],
			'left outer'
		);
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['recno'] . ' >', $id);
		}
				
		$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['data_emissao']." >= ", date('Ymd', PERIODO_DADOS));		
		$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['codigo_representante'], $codigo_representante);
		$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['delecao'] . ' IS NULL', null);
		//$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'] . '.' . $this->_db_cliente['campos']['notas_fiscais']['duplicata'] . ' !=', '');
		$this->db_cliente->where($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . ' IS NULL', null);
		
		//debug_pre($select);
		
		// Consulta
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['notas_fiscais']);
		
	
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['codigo_empresa']			= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}

}