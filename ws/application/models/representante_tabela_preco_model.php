<?php

class Representante_tabela_preco_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    
	function exportar_representante_tabela_preco($id = NULL, $pacote = NULL,$codigo_representante)
	{
		
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'representante_tabela_preco.id', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados)
	{
		
		$id 					= $dados['id'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		$codigo_representante 	= $dados['codigo_representante'];
		
		// * Retornar todos os campos
		$select = select_all($this->_db_cliente['tabelas']['representante_tabela_preco'], $this->_db_cliente['campos']['representante_tabela_preco'], NULL, FALSE, 'filial');
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['representante_tabela_preco'].".".$this->_db_cliente['campos']['representante_tabela_preco']['representante_codigo'], $codigo_representante);
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['representante_tabela_preco']['recno'] . ' >', $id);
		}
				
		
		$this->db_cliente->where($this->_db_cliente['campos']['representante_tabela_preco']['ativo'], 'A');
		
	
		
	
		
		

	
	
		// Consulta
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['representante_tabela_preco']);
//debug_pre($this->db_cliente->_compile_select());
		
	}

	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id, $codigo_representante)
	{	
		$parametros_consulta['id'] 					= $id;
		$parametros_consulta['codigo_empresa']		= NULL;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	/**
	* Met�do:		obter_dados
	* 
	* Descri��o:	Fun��o Utilizada para retornar os dados da consulta
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function obter_dados($dados)
	{
		$this->consulta($dados);
		
		//$this->db_cliente->get()->result_array();
		
		//debug_pre($this->db_cliente->last_query());
		
		return $this->db_cliente->get()->result_array();
	}
	
}