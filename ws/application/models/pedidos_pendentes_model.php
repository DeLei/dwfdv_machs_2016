<?php

class Pedidos_pendentes_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
	
	/**
	* Met�do:		exportar_pedidos
	* 
	* Descri��o:	Fun��o Utilizada para pegar retornar dados dos Pedidos
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function exportar_pedidos($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		$parametros_consulta['id'] 					 = $id;
	
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['tabelas']['pedidos_dw'] . '.' . $this->mapeamento['campos']['pedidos_dw']['id_pedido'], $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
		
		$id 					= $dados['id'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw'], 'pedido');
		$select_cliente 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_produto 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_condicoes_pagamento = select_all($this->_db_cliente['tabelas']['condicoes_pagamento'], $this->_db_cliente['campos']['condicoes_pagamento'], 'condicoes_pagamento');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		$select_derivacoes_produtos = select_all($this->_db_cliente['tabelas']['derivacoes_produtos'], $this->_db_cliente['campos']['derivacoes_produtos'], 'derivacao');
		$select_classe_produtos 	= select_all($this->_db_cliente['tabelas']['classe_venda_produtos'], $this->_db_cliente['campos']['classe_venda_produtos'], 'classe');

		$select = array_merge(
			$select_pedido, 
			$select_cliente,
			$select_produto,
			$select_condicoes_pagamento,
			$select_formas_pagamento,
			$select_transportadoras
			//$select_derivacoes_produtos,
			//$select_classe_produtos
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = N�o Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		$select[] = "'0' as converter_pedido_orcamento";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial'], $codigo_empresa);

		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ' >', $id);
		}
				
		$this->db_cliente->where_in($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'], array('A', 'R', 'B'));
		
		$this->db_cliente->where("({$this->_db_cliente['tabelas']['pedidos_dw']}.{$this->_db_cliente['campos']['pedidos_dw']['tipo_venda']} != '*' OR  {$this->_db_cliente['tabelas']['pedidos_dw']}.{$this->_db_cliente['campos']['pedidos_dw']['tipo_venda']} IS NULl)"); // Buscar Pedidos (* � or�amento)
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['delecao'] . ' IS NULL', null);
		
		//-----------------------------------------------
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'] . ' AND ' .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'] . ' AND ' .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . " IS NULL"
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " IS NULL"
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['condicoes_pagamento'], 
			$this->_db_cliente['tabelas']['condicoes_pagamento'] . '.' . $this->_db_cliente['campos']['condicoes_pagamento']['codigo'] . " = " . $this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['condicoes_pagamento'] . "." . $this->_db_cliente['campos']['condicoes_pagamento']['delecao'] . " IS NULL"
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['formas_pagamento'] . '.' . $this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['forma_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " IS NULL"
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['transportadoras'] . '.' . $this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['tabelas']['pedidos_dw'] . '.'. $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left', false);
		
		/*
		//-- Join	
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['derivacoes_produtos'], 
			$this->_db_cliente['tabelas']['derivacoes_produtos'] . '.' . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_empresa'] . ' = ' . $this->_db_cliente['tabelas']['pedidos_dw'] . '.'  . $this->_db_cliente['campos']['pedidos_dw']['codigo_empresa'] . ' AND ' .
			$this->_db_cliente['tabelas']['derivacoes_produtos'] . '.' . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_produto'] . ' = ' . $this->_db_cliente['tabelas']['pedidos_dw'] . '.'  . $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . ' AND ' .
			$this->_db_cliente['tabelas']['derivacoes_produtos'] . '.' . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_derivacao'] . ' = ' . $this->_db_cliente['tabelas']['pedidos_dw'] . '.'  . $this->_db_cliente['campos']['pedidos_dw']['derivacao']
		);

		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['classe_venda_produtos'], 
			$this->_db_cliente['tabelas']['classe_venda_produtos'] . '.' . $this->_db_cliente['campos']['classe_venda_produtos']['codigo_classe'] . ' = ' . $this->_db_cliente['tabelas']['derivacoes_produtos'] . '.'  . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_classe']
		);
		*/
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['pedidos_dw']);
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_empresa']		 = NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	//--------------
	//------------------
	//--------------------
	//------------------
	//--------------
	/**
	* Met�do:		importar
	* 
	* Descri��o:	Fun��o Utilizada para inserir pedidos no banco, e inserir LOGS
	* 
	* Data:			08/09/2012
	* Modifica��o:	08/09/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW for�a de vendas
	* @param		string 		$id_usuario					- ID do usu�rio
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{

		$dadosPedidos = array();

		foreach(json_decode($dados) as $dadoPedido) {
			if (!(array_key_exists($dadoPedido->pedido_id_pedido, $dadosPedidos))) {
				$dadosPedidos[$dadoPedido->pedido_id_pedido] = array();
			}
			$dadosPedidos[$dadoPedido->pedido_id_pedido][] = $dadoPedido;
		}	
		
		foreach($dadosPedidos as $pedidoProcessar) {
		
			
		
			$this->load->model('sincronizacoes_model');
			
			$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pedidos_dw', json_encode($pedidoProcessar), $id_usuario, $codigo_representante);
			
			$notificacao = '';

			if($id_sincronizacao)
			{
				$pedido = $pedidoProcessar;

				$pedidosBatch = array();

				if ($this->pedido_existe($pedido[0]->pedido_id_pedido)) {

					$notificacao .= 'O Pedido ' . $pedido[0]->pedido_id_pedido . ' foi enviado anteriormente.';

				} else {

					foreach($pedido as $pedidoItem) {
						
						$pedidosBatch[] = array(

							/* Informa��es de Cabe�alho */
							'codigo'					=> 	$pedidoItem->pedido_id_pedido,
							'tipo_pedido'				=> 	$pedidoItem->pedido_tipo_venda,
							'codigo_representante_1'	=> 	$pedidoItem->pedido_codigo_representante,
							'latitude' 					=> 	$pedidoItem->pedido_latitude,
							'longitude' 				=> 	$pedidoItem->pedido_longitude,
							'versao' 					=> 	$pedidoItem->pedido_versao,
							'data_emissao' 				=> 	$pedidoItem->pedido_data_emissao,
							'time_emissao' 				=> 	(int)$pedidoItem->pedido_time_emissao,
							'status' 					=> 	$pedidoItem->pedido_status,
							'tipo_frete' 				=> 	$pedidoItem->pedido_tipo_frete,
							'empresa' 					=> 	$pedidoItem->pedido_codigo_empresa,
							'filial' 					=> 	$pedidoItem->pedido_filial,
							'codigo_usuario' 			=> 	$pedidoItem->pedido_id_usuario,
							'codigo_cliente' 			=> 	$pedidoItem->pedido_codigo_cliente,
							'codigo_cliente_entrega'	=> 	$pedidoItem->pedido_codigo_cliente,
							'loja_cliente' 				=> 	$pedidoItem->pedido_loja_cliente,
							'loja_cliente_entrega' 		=> 	$pedidoItem->pedido_loja_cliente,
							'codigo_tabela_preco' 		=> 	$pedidoItem->pedido_tabela_precos,
							'codigo_condicao_pagamento' => 	$pedidoItem->pedido_condicao_pagamento,
							'pedido_cliente' 			=> 	$pedidoItem->pedido_pedido_cliente,
							'observacao_comercial'		=> 	$pedidoItem->pedido_observacao_comercial,
							'mensagem_nota'				=> 	$pedidoItem->pedido_mensagem_nota,
							'forma_pagamento'			=> 	$pedidoItem->pedido_forma_pagamento,
							'motivo_reprovacao'			=> 	$pedidoItem->pedido_motivo_reprovacao,
							'data_entrega'				=>  $pedidoItem->pedido_data_entrega,
							/* Informa��es de Item */
							'sequencial_item' 			=> 	$pedidoItem->pedido_numero_item,
							'codigo_produto' 			=> 	$pedidoItem->pedido_codigo_produto,
							'derivacao' 				=> 	$pedidoItem->produto_derivacao,
							'preco_venda' 				=> 	$pedidoItem->pedido_preco_venda,
							'preco_unitario' 			=> 	$pedidoItem->pedido_preco_unitario,
							'quantidade' 				=> 	$pedidoItem->pedido_quantidade,
							'modificador'				=> 	$pedidoItem->pedido_modificador,
							'percentual_modificador' 	=> 	$pedidoItem->pedido_percentual_modificador,
							'acrescimo_condicao_pagamento'	=> 	$pedidoItem->pedido_acrescimo_condicao_pagamento,
							'bloquear_pedido'			=> 	$pedidoItem->pedido_bloquear_pedido,
							'motivo_bloqueio'			=> 	$pedidoItem->pedido_motivo_bloqueio,
							'acrescimo_condicao_pagamento'	=> 	$pedidoItem->pedido_acrescimo_condicao_pagamento,
							'percentual_comissao_1'		=>  $pedidoItem->pedido_comissao
						);
					}
				}
				
				try
				{		
					$this->db_cliente->insert_batch('pedido_temporario', $pedidosBatch);
				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pedido)); 
				}	
			}
		}

		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
				
		if($dados_erros)
		{
			$codigos_erro = array();
		
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->pedido_id_pedido, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->pedido_id_pedido;
					$nome_erro[] = $dados_json->pedido_id_pedido . ' - ' . $dados_json->cliente_nome;
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');//, 'notificacao' => $notificacao);
		}
	}
	
	function pedido_existe($codigo)
	{
		$pedido = $this->db_cliente->select('codigo')
									->from('pedido_temporario')
									->where(array(
										'codigo' => $codigo
									))
									->limit(1)->get()->result();
		
		return $pedido;
	}

	function validar_pedido($id_pedido, $codigo_produto)
	{
		$pedido = $this->db_cliente->select($this->mapeamento['campos']['pedidos_dw']['id_pedido'])
									->from($this->mapeamento['tabelas']['pedidos_dw'])
									->where(array(
										$this->mapeamento['campos']['pedidos_dw']['id_pedido'] 		=> $id_pedido,
										$this->mapeamento['campos']['pedidos_dw']['codigo_produto'] => $codigo_produto
									))
									->limit(1)
									->get()
									->result();
		
		return $pedido;
	}

	/**
	* Met�do:		obter_campos_valores
	* 
	* Descri��o:	Fun��o Utilizada para retornar os campo com valores
	* 
	* Data:			21/09/2012
	* Modifica��o:	21/09/2012
	* 
	* @access		public
	* @param		array 		$pedido				- Dados dos Pedidos 
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function obter_campos_valores($pedido, $gerar_recno = TRUE)
	{
		foreach($pedido as $indice => $valor)
		{
			if($indice == 'filial')
			{
				continue;
			}

			$indice = str_replace("pedido_", "", $indice, $count);
			
			if($count > 1)
			{
				$indice = "pedido_" . $indice;
			}

			if($gerar_recno)
			{
				if($indice == 'chave') // gerar Recno
				{
					$valor = $this->gerar_recno();
				}
			}

			if(!empty($this->mapeamento['campos']['pedidos_dw'][$indice]))
			{
				
				if(empty($valor)) // N�o podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if(strtoupper($valor) == 'UNDEFINED') // N�o podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if($indice == 'time_emissao')
				{
					$valor = (int) $valor;
				}
				
				if($indice == 'total_desconto_item') //  Calculando o valor do desconto
				{
					
					//Aplicar desconto em cima do pre�o de tabela que pedido_preco_unitario
					if($pedido->pedido_desconto1 > 0)
					{
						$pedido->pedido_preco_unitario = $pedido->pedido_preco_unitario - ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto1 / 100));
					}
					
					//Calcular o valor do desconto
					$valor = $pedido->pedido_quantidade * ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto_item / 100));
				}
			
				$valores[$this->mapeamento['campos']['pedidos_dw'][$indice]] = $valor;
			}
			
		}
		
		$valores[$this->mapeamento['campos']['pedidos_dw']['delecao']] = ' ';
		
		return $valores;
	}
	
	function gerar_recno()
	{
		$dados = $this->db_cliente
			->select('MAX(' . $this->mapeamento['campos']['pedidos_dw']['chave'] . ')+1 AS chave')
			->from($this->mapeamento['tabelas']['pedidos_dw'])
			->get()
			->row_array();
			
		if($dados['chave'])
		{
			return $dados['chave'];
		}
		else
		{
			return 1;
		}
	}
}