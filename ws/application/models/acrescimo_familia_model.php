<?php

class Acrescimo_familia_model extends CI_Model {

    function __construct()
    {
        parent::__construct();		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    	
	function exportar_acrescimo_familia($id = NULL, $pacote = NULL)
	{
		$parametros_consulta['id'] = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'id', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que ser� executado para retornar os acr�scimos das fam�lias de produtos.
	* 
	* Data:			12/06/2015
	* Modifica��o:	12/06/2015
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];
	
		// Campos para o SELECT
		// * Retornar todos os campos
		$select = select_all($this->_db_cliente['tabelas']['acrescimo_familia'], $this->_db_cliente['campos']['acrescimo_familia'], NULL, FALSE, 'filial');
			
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['acrescimo_familia']['id'] . ' >', $id);
		}
	
		// Consulta
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['acrescimo_familia']);
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de acr�scimo de familias de produtos.
	* 
	* Data:			12/06/2015
	* Modifica��o:	12/06/2015
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
}