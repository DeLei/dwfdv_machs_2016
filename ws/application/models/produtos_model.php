<?php

class Produtos_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_produtos($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] 					 = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'tabela_preco_item.id', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_empresa 		= NULL;
		
	
		//Obter Tabelas Pre�os
		$tabelas_preco = $this->obter_tabelas_preco();
	
	
		// * Retornar todos os campos
		$select_produto = select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_estoque = select_all($this->_db_cliente['tabelas']['produtos_estoque'], $this->_db_cliente['campos']['produtos_estoque'], 'etq');
		$select_produto_tabela_preco = select_all($this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos'], 'ptp');
		$select_tabela_preco = select_all($this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos'], 'tb');
		$select_derivacoes_produtos = select_all($this->_db_cliente['tabelas']['derivacoes_produtos'], $this->_db_cliente['campos']['derivacoes_produtos'], 'derivacao');
		$select_derivacoes_classes_produtos = select_all($this->_db_cliente['tabelas']['classe_venda_produtos'], $this->_db_cliente['campos']['classe_venda_produtos'], 'derivacao_classe');
		
		$select = array_merge(
			$select_produto, 
			$select_estoque,
			$select_produto_tabela_preco,
			$select_tabela_preco
			//$select_derivacoes_produtos,
			//$select_derivacoes_classes_produtos
		);
		
		// * Obter quantidade disponivel
		$select[] = '(0) AS quantidade_disponivel_estoque';
		
		$select += formatar_euf($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], $codigo_empresa);
		
		//Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_estoque'], euf(
				$this->_db_cliente['tabelas']['produtos_estoque'], $this->_db_cliente['campos']['produtos_estoque']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			$this->_db_cliente['tabelas']['produtos_estoque'] . '.' . $this->_db_cliente['campos']['produtos_estoque']['codigo'] . ' = ' . $this->_db_cliente['tabelas']['produtos'] . '.'. $this->_db_cliente['campos']['produtos']['codigo']
		, 'left');
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			$this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto'] . ' = ' . $this->_db_cliente['tabelas']['produtos'] . '.'  . $this->_db_cliente['campos']['produtos']['codigo'] . ' AND ' .
			$this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_derivacao'] . ' = ' . $this->_db_cliente['tabelas']['produtos'] . '.'  . $this->_db_cliente['campos']['produtos']['derivacao']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial']
			) .
			$this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['tabelas_precos']['codigo'] . ' = ' . $this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_tabela_precos'] . ' AND ' . $this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['tabelas_precos']['ativo'] . " = '1'"
		);
		
		/*
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['derivacoes_produtos'], 
			$this->_db_cliente['tabelas']['derivacoes_produtos'] . '.' . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_empresa'] . ' = ' . $this->_db_cliente['tabelas']['produtos'] . '.'  . $this->_db_cliente['campos']['produtos']['empresa'] . ' AND ' .
			$this->_db_cliente['tabelas']['derivacoes_produtos'] . '.' . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_produto'] . ' = ' . $this->_db_cliente['tabelas']['produtos'] . '.'  . $this->_db_cliente['campos']['produtos']['codigo'] . ' AND ' .
			$this->_db_cliente['tabelas']['derivacoes_produtos'] . '.' . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_derivacao'] . ' = ' . $this->_db_cliente['tabelas']['produtos'] . '.'  . $this->_db_cliente['campos']['produtos']['derivacao']
		);
	
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['classe_venda_produtos'], 
			$this->_db_cliente['tabelas']['classe_venda_produtos'] . '.' . $this->_db_cliente['campos']['classe_venda_produtos']['codigo_classe'] . ' = ' . $this->_db_cliente['tabelas']['derivacoes_produtos'] . '.'  . $this->_db_cliente['campos']['derivacoes_produtos']['codigo_classe']
		);
		*/
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['recno'] . ' >', $id);
		}
		
		$this->db_cliente->where('tabela_preco_item.situacao', 'A');

		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['inativo'] . ' !=', '1');
		//$this->db_cliente->where($this->_db_cliente['tabelas']['derivacoes_produtos'] . '.' . $this->_db_cliente['campos']['derivacoes_produtos']['ativo'], 'A');
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['delecao'] . ' IS NULL', null);
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_estoque'] . '.' . $this->_db_cliente['campos']['produtos_estoque']['delecao'] . ' IS NULL', null);
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' IS NULL', null);
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' IS NULL', null);
		
		if (count($tabelas_preco) > 0) {
			$this->db_cliente->where_in($this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['tabelas_precos']['codigo'], $tabelas_preco);
		}
	
		// Consulta
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['produtos']);
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	/**
	* Met�do:		obter_tabelas_preco
	* 
	* Descri��o:	Fun��o Utilizada para retornar os codigos da tabelas de pre�o
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function obter_tabelas_preco()
	{
		// Load Tabelas de Pre�o
		$this->load->model('tabelas_preco_model');
	
		$this->tabelas_preco_model->_db_cliente = $this->_db_cliente;
	
		$parametros_consulta['id'] = NULL;
		$parametros_consulta['codigo_empresa'] = NULL;
	
		$tabelas_preco = $this->tabelas_preco_model->obter_dados($parametros_consulta);
		
		$codigos_tabelas_preco = array();
		
		foreach($tabelas_preco as $tp)
		{
			$codigos_tabelas_preco[] = $tp['codigo'];
		}
		
		return $codigos_tabelas_preco;

	}
}