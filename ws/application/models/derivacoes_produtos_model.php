<?php

class Derivacoes_produtos_model extends CI_Model {


    function __construct()
    {
        parent::__construct();

        $this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	/**
	* Metódo:		exportar_derivacoes_produtos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Derivações de Produtos
	* 
	* Data:			19/08/2014
	* Modificação:	19/08/2014
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_derivacoes_produtos($id = NULL, $pacote = NULL)
	{
		$parametros_consulta['id'] = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar as Derivações de Produtos
	* 
	* Data:			19/08/2014
	* Modificação:	19/08/2014
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
		$id = $dados['id'];
	
		$this->db_cliente->select('codigo_empresa');	
		$this->db_cliente->select('codigo_produto');	
		$this->db_cliente->select('codigo_derivacao');	
		$this->db_cliente->select('descricao_derivacao');	
			
		// Consulta
		$this->db_cliente->from('derivacoes_produtos');
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}

}