<?php

class Pedido_parcela_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
	
	/**
	* Met�do:		exportar_parcelas
	* 
	* Descri��o:	Fun��o Utilizada para pegar retornar dados das Parcelas
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function exportar_parcelas($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		$parametros_consulta['id'] 					 	=	$id;
		$parametros_consulta['codigo_representante']	=	$codigo_representante;
	
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['tabelas']['pedido_parcela'] . '.' . $this->mapeamento['campos']['pedido_parcela']['id_pedido'], $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que ser� executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
		
		$id 					= $dados['id'];
		$codigo_empresa 		= $dados['codigo_empresa'];
				
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = N�o Exportado)
		$select = select_all($this->_db_cliente['tabelas']['pedido_parcela'], $this->_db_cliente['campos']['pedido_parcela'], 'pedido_parcela');
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
				
		$this->db_cliente->join($this->_db_cliente['tabelas']['pedidos_dw'], "{$this->_db_cliente['tabelas']['pedidos_dw']}.{$this->_db_cliente['campos']['pedidos_dw']['codigo']} = {$this->_db_cliente['tabelas']['pedido_parcela']}.{$this->_db_cliente['campos']['pedido_parcela']['id_pedido']}", "LEFT OUTER");
		$this->db_cliente->join($this->_db_cliente['tabelas']['pedidos'], "{$this->_db_cliente['tabelas']['pedidos']}.{$this->_db_cliente['campos']['pedidos']['codigo']} = {$this->_db_cliente['tabelas']['pedido_parcela']}.{$this->_db_cliente['campos']['pedido_parcela']['id_pedido']}", "LEFT OUTER");
		$this->db_cliente->where("(({$this->_db_cliente['campos']['pedidos_dw']['codigo_representante']} = '{$dados['codigo_representante']}') OR ({$this->_db_cliente['campos']['pedidos']['codigo_representante']} = '{$dados['codigo_representante']}'))");
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['pedido_parcela']);
		
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de parcelas
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['id'] 					 		= $id;
		$parametros_consulta['codigo_empresa']		 		= NULL;
		$parametros_consulta['codigo_representante']	 	= $codigo_representante;
	
		return retornar_total($this, $parametros_consulta);
	}

	/**
	* Met�do:		importar
	* 
	* Descri��o:	Fun��o Utilizada para inserir pedidos no banco, e inserir LOGS
	* 
	* Data:			08/09/2012
	* Modifica��o:	08/09/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW for�a de vendas
	* @param		string 		$id_usuario					- ID do usu�rio
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
	
		$dadosParcelas = array();

		foreach(json_decode($dados) as $dadoParcela) {
			if (!(array_key_exists($dadoParcela->pedido_parcela_id_pedido, $dadosParcelas))) {
				$dadosParcelas[$dadoParcela->pedido_parcela_id_pedido] = array();
			}
			$dadosParcelas[$dadoParcela->pedido_parcela_id_pedido][] = $dadoParcela;
		}

		foreach($dadosParcelas as $parcelaProcessar) {
			$this->load->model('sincronizacoes_model');
			
			$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('parcelas', json_encode($parcelaProcessar), $id_usuario, $codigo_representante);
			
			$notificacao = '';

			if($id_sincronizacao)
			{
				$parcela = $parcelaProcessar;

				$parcelasBatch = array();

				if ($this->pedido_parcela_existe($parcela[0]->pedido_parcela_id_pedido, $parcela[0]->pedido_parcela_numero_parcela)) {

					$notificacao .= 'A parcela ' . $parcela[0]->pedido_parcela_numero_parcela . ' do Pedido ' . $parcela[0]->pedido_parcela_id_pedido . ' foi enviado anteriormente.';

				} else {

					foreach($parcela as $parcelaItem) {
						$parcelasBatch[] = array(
							'id_pedido' 		 => $parcelaItem->pedido_parcela_id_pedido,
							'numero_parcela'	 =>	(int)$parcelaItem->pedido_parcela_numero_parcela,
							'valor_parcela'		 =>	(float)$parcelaItem->pedido_parcela_valor_parcela,
							'data_vencimento' 	 => $parcelaItem->pedido_parcela_data_vencimento
						);
					}
				}
				
				try
				{		
					$this->db_cliente->insert_batch('pedido_parcela', $parcelasBatch);
				}
				catch(Exception $e)
				{
				debug_pre($e);
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($parcela)); 
				}	
			}
		}

		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
				
		if($dados_erros)
		{
			$codigos_erro = array();
		
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->pedido_parcela_id_pedido, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->pedido_parcela_id_pedido;
					$nome_erro[] = $dados_json->pedido_parcela_id_pedido . ' - ' . $dados_json->pedido_parcela_numero_parcela;
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');//, 'notificacao' => $notificacao);
		}
	}
	
	function pedido_parcela_existe($id_pedido, $numero_parcela)
	{
		$parcela = $this->db_cliente->select('id_pedido')
									->from('pedido_parcela')
									->where(array(
										'id_pedido' => $id_pedido,
										'numero_parcela' => (int)$numero_parcela
									))
									->limit(1)->get()->result();
		
		return $parcela;
	}

}