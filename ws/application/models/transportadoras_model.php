<?php

class Transportadoras_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    
	
	/**
	* Met�do:		exportar_transportadoras
	* 
	* Descri��o:	Fun��o Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function exportar_transportadoras($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'transportadora.codigo', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];

	
		// Campos para o SELECT
		$select = select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], NULL, FALSE, 'filial');
		
		$select += formatar_euf($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], $codigo_empresa);
		
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['transportadoras']['codigo'] . ' >', $id);
		}
		$this->db_cliente->where($this->_db_cliente['campos']['transportadoras']['delecao'] . ' IS NULL', null);
	
	
		// Consulta
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['transportadoras']);
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] 					= $id;
		$parametros_consulta['codigo_empresa']		= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	

}