<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function mapeamento($config, $codigo_empresa)
{
	$cod = $codigo_empresa;

	// Tabela de pedidos definada no arquivo "config/config.php"
	$pedido_tabela = $config['pedidos_dw']['tabela'];
	$pedido_campo = $config['pedidos_dw']['campo'];

	// Tabela de prospects definada no arquivo "config/config.php"
	$prospects_tabela = $config['prospects']['tabela'];
	$prospects_campo = $config['prospects']['campo'];

	$config['db_cliente'] = array(
		'configuracoes' => array(
			'pedidos' => array(
				'recno' => 'gerar' # auto - Para RECNO automatico gerado pelo banco | gerar - Para gerar um RECNO
			)
		),
		'tabelas' => array(
			'pedidos'	 						=> 'pedido',
			'itens_pedidos' 					=> 'pedido_item',
			'pedido_parcela' 					=> 'pedido_parcela',
			'prazo_parcela' 					=> 'prazo_parcela',
			'titulos' 							=> 'titulo',
			'clientes' 							=> 'cliente',
			'produtos' 							=> 'produto',
			'acrescimo_familia' 				=> 'acrescimo_familia',
			'desconto_familia' 					=> 'desconto_familia',
			'condicoes_pagamento' 				=> 'condicao_pagamento',
			'formas_pagamento' 					=> 'forma_pagamento',
			'transportadoras' 					=> 'transportadora',
			'notas_fiscais' 					=> 'nota_fiscal',
			'itens_notas_fiscais' 				=> 'nota_fiscal_item',
			'pedidos_dw' 						=> 'pedido_temporario',
			'cadastro_municipios'				=> 'municipio',
			'cadastro_pais'						=> 'pais',
			'tabelas_precos' 					=> 'tabela_preco',
			'produtos_tabelas_precos' 			=> 'tabela_preco_item',
			'premiacoes' 						=> 'premiacao',
			'metas' 							=> 'meta',
			'representantes' 					=> 'representante',
			'representante_tabela_preco'		=> 'representante_tabela_preco',
			'conversao_unidade_medida'			=> 'conversao_unidade_medida',
			'produtos_estoque' 					=> 'produtos_estoque',
			'prospects'							=> 'prospect',
			'grupos_produtos' 					=> 'produto_grupo',
			'euf'								=> 'compartilhamento_filial',
			'derivacoes_produtos'				=> 'derivacoes_produtos',
			'classe_venda_produtos'				=> 'classe_venda_produtos',
			'regiao_venda'						=> 'regiao_venda', // E150/2015  
			'motivo_nao_venda'					=> 'motivo_nao_venda', // E150/2015  
			'pedido_venda'						=> 'pedido_venda', // E150/2015  
			'tipo_entrada_saida' 				=> 'SF4'.$cod, //NAO UTILIZA
			'regra_desconto'					=> 'ACO'.$cod, //NAO UTILIZA
			'generica'							=> 'SX5'.$cod, // tabela genérica de dados do ERP
			'representantes_pedidos' 			=> '', // em branco para protheus
			'opcionais_produtos' 				=> '', //NAO UTILIZA		
		),

		'campos' => array(
			'pedidos' => array(
				'filial' 						=> 'filial',
				'codigo_representante' 			=> 'codigo_representante',
				'data_emissao'					=> 'data_emissao',
				'codigo' 						=> 'numero',
				'tipo' 							=> 'tipo',
				'codigo_cliente' 				=> 'codigo_cliente',
				//'codigo_ordem_compra' 		=> 'C5_PEDCLI',
				'loja_cliente' 					=> 'loja_cliente',
				'codigo_nota_fiscal' 			=> 'numero_nota_fiscal',
				'serie_nota_fiscal' 			=> 'serie_nota_fiscal',
				'codigo_forma_pagamento' 		=> 'codigo_condicao_pagamento',
				'forma_pagamento'				=> 'forma_pagamento',
				'codigo_transportadora' 		=> 'codigo_transportadora',
				'tipo_frete' 					=> 'tipo_frete',
				'tabela_precos' 				=> 'codigo_tabela_preco',
				'mensagem' 						=> 'mensagem_nota_fiscal',
				//'data_entrega' 				=> 'C5_FECENT',
				'cliente_entrega' 				=> 'codigo_cliente_entrega',
				'loja_entrega' 					=> 'loja_cliente_entrega',
				'desconto1' 					=> 'percentual_desconto_1',
				'desconto2' 					=> 'percentual_desconto_2',
				'desconto3' 					=> 'percentual_desconto_3',
				'desconto4' 					=> 'percentual_desconto_4',
				'delecao' 						=> 'deletado'
				),
			'itens_pedidos' => array(
				'filial' 						=> 'filial',
				'codigo_pedido' 				=> 'numero_pedido',
				'valor_total_item' 				=> 'valor_total_com_descontos',
				'total_desconto_item' 			=> 'valor_total_descontos',
				'quantidade_vendida_produto'	=> 'quantidade_vendida',
				'quantidade_faturada_produto' 	=> 'quantidade_faturada',
				'preco_produto' 				=> 'preco_unitario',
				'preco_unitario' 				=> 'preco_venda',
				'codigo_item' 					=> 'sequencial_item',
				'tes'		 					=> 'produto_identificador_tipo_saida',
				'codigo_produto' 				=> 'codigo_produto',
				'descricao_produto' 			=> 'descricao_produto',
				'unidade_medida_produto' 		=> 'unidade_medida',
				'pedido_cliente' 				=> 'pedido_cliente',
				'codigo_cliente' 				=> 'codigo_cliente',
				'codigo_loja'					=> 'loja_cliente',
				'local'							=> 'local',
				'derivacao'						=> 'derivacao',
				'codigo_tabela_preco'			=> 'codigo_tabela_preco',
				'delecao' 						=> 'deletado',
				
				),	
			'pedido_parcela' => array(
				'id'				=>	'id',
				'id_pedido'			=>	'id_pedido',
				'numero_parcela'	=>	'numero_parcela',
				'valor_parcela' 	=>	'valor_parcela',
				'data_vencimento'	=>	'data_vencimento'
			),
			'prazo_parcela' => array(
				'id'						=>	'id',
				'codigo_condicao_pagamento'	=>	'codigo_condicao_pagamento',
				'numero_parcela'			=>	'numero_parcela',
				'quantidade_dias'			=>	'quantidade_dias'
			),
			'titulos' => array(
				'filial' 				=> 'filial',
				'codigo_pedido'			=> 'numero_pedido',
				'valor' 				=> 'valor_total',
				'prefixo' 				=> 'prefixo',
				'data_emissao' 			=> 'data_emissao',
				'codigo_cliente' 		=> 'codigo_cliente',
				'loja_cliente' 			=> 'loja_cliente',
				'data_vencimento' 		=> 'data_vencimento',
				'data_vencimento_real'	=> 'data_vencimento_real',
				'data_baixa' 			=> 'data_baixa',
				'codigo' 				=> 'codigo',
				'codigo_representante'	=> 'codigo_representante',
				'parcela'				=> 'parcela',
				'saldo' 				=> 'valor_saldo',
				'delecao' 				=> 'deletado',
				'serie' 				=> 'serie_nota_fiscal',
				'tipo' 					=> 'identificador_tipo',
				'nota'					=> 'numero_nota_fiscal'
				),
			'clientes' => array( //SA1010
				'codigo' 					=> 'codigo',
				'loja' 						=> 'loja',
				'filial'					=> 'filial',
				'nome'						=> 'nome',
				'cpf' 						=> 'cgc',
				'pessoa_contato' 			=> 'contato_pessoa',
				'endereco' 					=> 'endereco_logradouro',
				'site'	 					=> 'website',
				'bairro' 					=> 'endereco_bairro',
				'cep' 						=> 'endereco_cep',
				'cidade' 					=> 'endereco_municipio',
				'estado' 					=> 'endereco_estado',
				'codigo_municipio' 			=> 'codigo_municipio',
				'ddd' 						=> 'contato_telefone_ddd',
				'telefone' 					=> 'contato_telefone',
				'email' 					=> 'contato_email',
				'limite_credito' 			=> 'limite_credito_disponivel',
				'pedidos_liberados' 		=> 'valor_total_pedidos_liberados',
				'total_titulos_aberto' 		=> 'total_titulos_abertos',
				'codigo_representante' 		=> 'codigo_representante',
				'condicao_pagamento' 		=> 'codigo_condicao_pagamento',
				'ultima_compra'				=> 'data_ultima_compra',
				'data_cadastro' 			=> '', //TODO
				'data_primeira_compra' 		=> 'data_primeira_compra',
				'chave_primaria' 			=> 'id',
				'tipo_pessoa' 				=> 'tipo_pessoa',
				'nome_fantasia' 			=> 'nome_fantasia',
				
				'rg' 						=> 'registro_geral',
				'inscricao_estadual' 		=> 'inscricao_estadual',
				'inscricao_municipal' 		=> 'inscricao_municipal',
				'inscricao_rural' 			=> 'inscricao_rural',
				
				'valor_maior_atraso_dia' 	=> 'dias_maior_atraso_pagamento',
				'media_atraso_dia' 			=> 'dias_media_atraso_pagamento',
				'valor_pago_atraso'	 		=> '', //TODO
				'total_titulos_vencidos'	=> 'total_titulos_ventidos',				
				'desconto' 					=> 'percentual_desconto',
				'numero' 					=> '',
				'observacao' 				=> 'observacao',
				'tipo' 						=> 'tipo',
				'tabela_preco' 				=> 'codigo_tabela_preco',
				'transportadora'			=> 'codigo_transportadora',
				'tipo_frete' 				=> 'tipo_frete',
				'situacao' 					=> 'inativo',
				'status'					=> 'status',
				'delecao' 					=> 'deletado',
				'codigo_icms_st_cliente' 	=> 'codigo_icms_st_cliente',
				),
			'produtos' => array(
				'codigo'				=> 'codigo',
				'filial'				=> 'filial',
				'codigo_real' 			=> 'codigo',
				'codigo_grupo' 			=> 'codigo_produto_grupo',
				'codigo_classe' 		=> '',
				'descricao' 			=> 'descricao',
				'empresa'				=> 'empresa',
			
				'quantidade_embalagem' 	=> 'quantidade_embalagem',
				'unidade_medida' 		=> 'unidade_medida',
				'converter'		 		=> 'valor_conversao',
				'tipo_converter' 		=> 'tipo_conversao',
			
				'derivacao' 			=> 'derivacao',

				'ipi' 					=> 'percentual_ipi',
				'icms' 					=> 'percentual_icms',
				'cofins' 				=> 'percentual_cofins',
				'pis' 					=> 'percentual_pis',
			
				'delecao' 				=> 'deletado',
				'inativo' 				=> 'inativo',
				'peso' 					=> 'peso',
				'locpad' 				=> 'origem_produto',
				'tipo_entrada_saida' 	=> 'identificador_tipo_saida',
				'disponivel_venda'		=> '',
				'segunda_unidade_medida' => 'segunda_unidade_medida',
				'familia'				=>	'familia',
			),
			'acrescimo_familia'	=>	array(
				'id'					=>	'id',
				'empresa'				=>	'empresa',
				'filial'				=>	'filial',
				'familia_produto'		=>	'familia_produto',
				'faixa_inicial'			=>	'faixa_inicial',
				'faixa_final'			=>	'faixa_final',
				'percentual_comissao'	=>	'percentual_comissao'
			),
			'desconto_familia'	=>	array(
				'id'					=>	'id',
				'empresa'				=>	'empresa',
				'filial'				=>	'filial',
				'familia_produto'		=>	'familia_produto',
				'faixa_inicial'			=>	'faixa_inicial',
				'faixa_final'			=>	'faixa_final',
				'percentual_comissao'	=>	'percentual_comissao'
			),
			'condicoes_pagamento'	=>	array(
				'codigo' 				=> 	'codigo',
				'descricao'			 	=> 	'descricao',
				'quantidade_parcelas'	=>	'quantidade_parcelas',
				'prazo_medio'			=>	'prazo_medio',
				'delecao' 				=> 	'deletado'
			),
			'formas_pagamento' => array(
				'codigo' 		=> 	'codigo',
				'descricao' 	=> 	'descricao',
				'delecao' 		=> 	'deletado'
			),
			'transportadoras' => array(
				'codigo' 	=> 'codigo',
				'nome' 		=> 'nome',
				'telefone' 	=> 'telefone',
				'filial'	=> 'filial',
				'delecao' 	=> 'deletado',
				),
			'notas_fiscais' => array(
				'codigo' 				=> 'numero',
				'serie' 				=> 'serie',
				'data_emissao' 			=> 'data_emissao',
				'hora_emissao' 			=> 'hora_emissao',
				'data_entrega' 			=> 'data_entrega',
				'valor_total' 			=> 'valor_total_faturado',
				'valor_icms' 			=> 'valor_total_icms',
				'valor_ipi' 			=> 'valor_total_ipi',
				'duplicata' 			=> 'codigo_titulo',
				'codigo_cliente' 		=> 'codigo_cliente',
				'loja_cliente'			=> 'loja_cliente',
				'codigo_transportadora' => 'codigo_transportadora',
				'codigo_representante' 	=> 'codigo_representante',
				'base_calculo_icms' 	=> 'base_icms',
				'base_calculo_icms_st' 	=> 'base_icms_st',
				'valor_icms_st' 		=> 'valor_total_icms_retido',
				'valor_total_bruto' 	=> 'valor_total_bruto',
				'valor_frete' 			=> 'valor_total_frete',
				'valor_seguro' 			=> 'valor_total_seguro',
				'valor_desconto' 		=> 'valor_total_desconto',
				'despesas_necessarias' 	=> 'valor_total_despesa',
				'condicao_pagamento' 	=> 'codigo_condicao_pagamento',
				'valor_mercadoria' 		=> 'valor_total_mercadoria',
				'tipo_frete' 			=> 'tipo_frete',
				'filial' 				=> 'filial',
				'delecao' 				=> 'deletado',
				'volume_1' 				=> 'quantidade_embalagens_especie',
				'especie' 				=> 'descricao_especie',
				'peso_bruto' 			=> 'peso_bruto',
				'peso_liquido' 			=> 'peso_liquido',
				//'situacao'				=> 'situacao',
				'chave_nfe' 			=> 'chave_nfe'
				),
			'itens_notas_fiscais' => array(
				'codigo_nota_fiscal' 		=> 'numero_nota_fiscal',
				'serie' 					=> 'serie_nota_fiscal',
				'codigo_pedido' 			=> 'numero_pedido',
				'cfop'						=> 'codigo_fiscal',
				'codigo_produto' 			=> 'codigo_produto',
				'codigo_cliente' 			=> 'codigo_cliente',
				'loja_cliente' 				=> 'loja_cliente',
				'preco_unitario' 			=> 'preco_venda',
				'unidade_medida' 			=> 'unidade_medida',
				'quantidade_entrega' 		=> 'quantidade_entregue',
				'quantidade_vendida' 		=> 'quantidade_vendida',
				'total_faturado' 			=> 'valor_total_faturado',
				'filial' 					=> 'filial',
				'ipi' 						=> 'valor_total_ipi',
				'base_calc_icms' 			=> 'base_icms',
				'aliq_icms' 				=> 'percentual_aliquota_icms',
				'aliq_ipi' 					=> 'percentual_aliquota_ipi',
				'valor_icms' 				=> 'valor_total_icms',
				'st' 						=> 'valor_total_icms_retido',
				'item' 						=> 'sequencial_item_pedido',
				'numero_item_nota'			=> 'sequencial_item',			
				'valor_bruto'				=> 'valor_total_bruto',				
				'delecao' 					=> 'deletado'
				),
			'pedidos_dw' => array(
				'codigo_empresa'				=> 'empresa',
				'filial'						=> 'filial',
				'id_pedido' 					=> 'id',
				'data_emissao' 					=> 'data_emissao',
				'time_emissao' 					=> 'time_emissao',
				'time_importacao' 				=> 'time_importacao',
				'time_conversao' 				=> 'time_conversao',
				'id_usuario' 					=> 'codigo_usuario',
				'derivacao'						=> 'derivacao',
				'codigo_representante' 			=> 'codigo_representante_1',
				'codigo_representante2' 		=> 'codigo_representante_2',
				'codigo_representante3' 		=> 'codigo_representante_3',
				'codigo_representante4' 		=> 'codigo_representante_4',
				'codigo_representante5' 		=> 'codigo_representante_5',
				'comissao' 						=> 'percentual_comissao_1',
				'comissao2' 					=> 'percentual_comissao_2',
				'comissao3' 					=> 'percentual_comissao_3',
				'comissao4' 					=> 'percentual_comissao_4',
				'comissao5' 					=> 'percentual_comissao_5',
				'desconto_item'					=> 'percentual_desconto_item',
				'desconto1' 					=> 'percentual_desconto_1',
				'desconto2' 					=> 'percentual_desconto_2',
				'desconto3' 					=> 'percentual_desconto_3',
				'desconto4' 					=> 'percentual_desconto_4',
				'tabela_precos' 				=> 'codigo_tabela_preco',
				'id_feira' 						=> 'codigo_feira',
				'status' 						=> 'status',
				'motivo_reprovacao' 			=> 'motivo_reprovacao',
				'motivo_bloqueio' 				=> 'motivo_bloqueio',
				'codigo_cliente' 				=> 'codigo_cliente',
				'loja_cliente' 					=> 'loja_cliente',
				'id_prospects' 					=> 'cgc_prospect',
				'condicao_pagamento'			=> 'codigo_condicao_pagamento',
				'acrescimo_condicao_pagamento'	=> 'acrescimo_condicao_pagamento',
				'forma_pagamento'				=> 'forma_pagamento',
				'tipo_frete' 					=> 'tipo_frete',
				'dirigido_redespacho'	  		=> 'frete_dirigido_redespacho',
				'codigo_transportadora' 		=> 'codigo_transportadora',
				'despesa' 						=> 'valor_despesa',
				'tipo_venda' 					=> 'tipo_pedido',
				'tipo_cliente'	  				=> 'tipo_cliente',
				'mensagem_nota'					=> 'mensagem_nota',
				'cliente_entrega' 				=> 'codigo_cliente_entrega',
				'loja_entrega' 					=> 'loja_cliente_entrega',
				'data_entrega' 					=> 'data_entrega',
				'pedido_cliente' 				=> 'pedido_cliente',
				'codigo_produto' 				=> 'codigo_produto',
				'numero_item' 					=> 'sequencial_item',
				'unidade_medida' 				=> 'produto_unidade_medida',
				'preco_unitario' 				=> 'preco_unitario',
				'preco_venda' 					=> 'preco_venda',
				'quantidade' 					=> 'quantidade',
				'total_desconto_item'			=> 'valor_total_desconto_item',
				'tipo_entrada_saida'			=> 'produto_identificador_tipo_saida',
				'codigo_fiscal'					=> 'codigo_fiscal',
				'ipi'							=> 'valor_total_ipi_item',
				'st'							=> 'valor_total_st_item',
				'icms'							=> 'valor_total_icms_item',
				'local'							=> 'local',
				'latitude'						=> 'latitude',
				'longitude'						=> 'longitude',
				'versao'						=> 'versao',
				'codigo'						=> 'codigo',
				'observacao_comercial'			=> 'observacao_comercial',
				'modificador'					=> 'modificador',
				'percentual_modificador'		=> 'percentual_modificador',
				'bloquear_pedido'				=> 'bloquear_pedido',
				'delecao' 						=> 'deletado',
				'chave' 						=> 'id'
			),
			'cadastro_municipios' => array(
				'codigo' 	=> 'codigo',
				'nome'		=> 'nome',
				'uf' 		=> 'estado',
				'filial' 	=> 'filial'
			),
			'cadastro_pais' => array(
				'filial' 		=> 'filial',
				'codigo' 		=> 'codigo',
				'descricao' 	=> 'descricao',
				'sigla' 		=> 'sigla'
			),
			'tabelas_precos' => array(
				'filial' 				=> 'filial',
				'codigo' 				=> 'codigo',
				'descricao' 			=> 'descricao',
				'condicao_pagamento' 	=> 'codigo_condicao_pagamento',
				'vigencia_inicio' 		=> 'data_vigencia_inicio',
				'vigencia_final' 		=> 'data_vigencia_final',
				'delecao' 				=> 'deletado',
				'irregular'				=> 'irregular',
				'ativo' 				=> 'ativo',
			),
			'produtos_tabelas_precos' => array(
				'codigo_tabela_precos' 	=> 'codigo_tabela_preco',
				'codigo_produto' 		=> 'codigo_produto',
				'preco' 				=> 'preco_venda',
				'codigo_derivacao'		=> 'codigo_derivacao',
				'filial' 				=> 'filial',
				'delecao' 				=> 'deletado'
			),
			'premiacoes' => array(
				'filial'				=> 'filial',
				'codigo_representante'	=> 'codigo_representante',
				'data_emissao' 			=> 'data',
				'valor' 				=> 'valor',
				'delecao' 				=> 'deletado'
				),
			'metas' => array(
				'filial' 				=> 'filial',
				'codigo_representante' 	=> 'codigo_representante',
				'data' 					=> 'data',
				'valor' 				=> 'valor',
				'delecao' 				=> 'deletado'
				),
			'representantes' => array(
				'codigo'			=> 'codigo',
				'filial'			=> 'filial',
				'senha' 			=> 'senha',
				'nome' 				=> 'nome',
				'cpf' 				=> 'cgc',
				'rg' 				=> 'registro_geral',
				'endereco' 			=> 'endereco_logradouro',
				'bairro' 			=> 'endereco_bairro',
				'cep' 				=> 'endereco_cep',
				'cidade' 			=> 'endereco_municipio',
				'estado' 			=> 'endereco_estado',
				'telefone' 			=> 'telefone',
				'email' 			=> 'email',
				'comissao' 			=> 'percentual_comissao',
				'grupo' 			=> 'identificador_grupo',
				'codigo_supervisor' => 'codigo_representante_supervisor',
				'delecao' 			=> 'deletado'
			),
			'representante_tabela_preco' => array(
				'codigo'				=> 'id',
				'representante_codigo' 	=> 'representante_codigo', 
                'tabela_preco_empresa' 	=> 'tabela_preco_empresa',
                'tabela_preco_codigo' 	=>'tabela_preco_codigo',
                'ativo' 			=>'ativo'
				
			),
			'conversao_unidade_medida' => array(
				'id' => 'id',
				'unidade_medida' => 'unidade_medida',
				'segunda_unidade_medida' => 'segunda_unidade_medida',
				'tipo_conversao' => 'tipo_conversao',
				'valor_conversao' => 'valor_conversao'			
			),
			'produtos_estoque' => array(
				'filial' 					=> 'filial',
				'codigo' 					=> 'codigo_produto',
				'quantidade_disponivel' 	=> '', //NAO UTILIZA
				'quantidade_atual' 			=> 'quantidade_atual',
				'quantidade_pedidos_venda' 	=> 'quantidade_pedido_venda',
				'quantidade_empenhada' 		=> 'quantidade_empenhada',
				'quantidade_reservada' 		=> 'quantidade_reserva',
				'ano' 						=> '', //NAO UTILIZA
				'mes' 						=> '', //NAO UTILIZA
				'preco' 					=> '', //NAO UTILIZA
				'delecao' 					=> 'deletado'
				),
			'prospects' => array (
				'codigo_empresa'			=> 'empresa',
				'codigo'					=> 'codigo',
				'codigo_loja' 				=> 'loja',
				'data_emissao' 				=> 'data_emissao',
				'time_emissao' 				=> 'time_emissao',
				'tipo_pessoa'				=> 'tipo_pessoa',
				'nome' 						=> 'nome',
				'nome_fantasia'				=> 'nome_fantasia',
				'data_nascimento' 			=> 'data_nascimento',
				'cep' 						=> 'endereco_cep',
				'endereco' 					=> 'endereco_logradouro',
				'complemento' 				=> 'endereco_complemento',
				'bairro' 					=> 'endereco_bairro',
				'codigo_municipio' 			=> 'codigo_municipio',
				'estado' 					=> 'endereco_estado',
				'pais' 						=> 'codigo_pais',
				'ddd' 						=> 'ddd',
				'telefone' 					=> 'telefone',
				'telex' 					=> 'telex',
				'fax' 						=> 'fax',
				
				'nome_contato'				=> 'contato_nome',
				'cargo_contato'				=> 'contato_cargo',
				'email_contato'				=> 'contato_email',
				'telefone_contato'			=> 'contato_telefone',
				
				'cgc' 						=> 'cgc',
				'inscricao_estadual' 		=> 'inscricao_estadual',
				'inscricao_municipal' 		=> 'inscricao_municipal',
				'inscricao_rural' 			=> 'inscricao_rural',
				'rg' 						=> 'registro_geral',
				'tipo' 						=> 'tipo',
				'email' 					=> 'email',
				'site' 						=> 'website',
				'bloqueado' 				=> 'inativo',
				'codigo_representante' 		=> 'codigo_representante',
				'status' 					=> 'status',
				'codigo_cliente' 			=> 'codigo_cliente',
				'time_importacao' 			=> 'time_importacao',
				'id_feira' 					=> 'codigo_feira',
				'filial' 					=> 'filial',
				'observacao' 				=> 'observacao',
				'latitude' 					=> 'latitude',
				'longitude' 				=> 'longitude',
				'versao' 					=> 'versao',
				'delecao' 					=> 'deletado',
				//'recno' 					=> 'R_E_C_N_O_'
			),
			'grupos_produtos' => array(
				'codigo' 		=> 'codigo',
				'filial' 		=> 'filial',
				'descricao'		=> 'descricao',
				'delecao' 		=> 'deletado'
				),
			'euf' => array(
				'chave' 				=> 'chave',
				'modo_filial' 			=> 'modo_filial',
				'modo_unidade' 			=> 'modo_unidade',
				'modo_empresa' 			=> 'modo_empresa',
				'tamanho_filial' 		=> 'tamanho_filial',
				'tamanho_empresa' 		=> 'tamanho_empresa',
				'tamanho_unidade' 		=> 'tamanho_unidade',
				'delecao' 				=> 'deletado'
			),
			'derivacoes_produtos' => array(
				'codigo_empresa'		=> 'codigo_empresa',
				'codigo_produto'		=> 'codigo_produto',
				'codigo_derivacao'		=> 'codigo_derivacao',
				'codigo_classe'			=> 'codigo_classe',
				'descricao_derivacao'	=> 'descricao_derivacao',
				'ativo'					=> 'ativo'
			),
			'classe_venda_produtos' => array(
				'codigo_classe' 		=> 'codigo_classe',
				'descricao_classe'		=> 'descricao_classe'
			),

			// E150/2015
			'regiao_venda' => array(
				'id' 		=> 'id',
				'codigo' 		=> 'codigo',
				'nome' 		=> 'nome',
				'data_validade' 		=> 'data_validade',
				'ativa'		=> 'ativa'
			),
			
			// E150/2015
			'motivo_nao_venda' => array(
				'id' 		=> 'id',
				'codigo' 		=> 'codigo',
				'nome' 		=> 'nome',				
				'ativa'		=> 'ativa'
			),
			
			
			// E150/2015
			'pedido_venda' => array(
				'id' 		=> 'id',
				'codigo_cliente' 		=> 'codigo_cliente',
				'codigo_empresa' 		=> 'codigo_empresa',				
				'codigo_filial'			=> 'codigo_filial',
				'codigo_vendedor'		=> 'codigo_vendedor',
				'data'					=> 'data',
				'codigo_motivo'			=> 'codigo_motivo',
				'codigo_pedido'			=> 'codigo_pedido',
				'importado'				=> 'importado',
				'codigo_gerado_erp'			=> 'codigo_gerado_erp',
			),
			'regra_desconto' => array (//NAO UTILIZA
				'filial' 				=> 'ACO_FILIAL',
				'codigo'				=> 'ACO_CODREG',
				'descricao'				=> 'ACO_DESCRI',
				'codigo_cliente'		=> 'ACO_CODCLI',
				'codigo_loja'			=> 'ACO_LOJA',
				'tabela_preco'			=> 'ACO_CODTAB',
				'condicao_pagamento'	=> 'ACO_CONDPG',
				'forma_pagamento'		=> 'ACO_FORMPG',
				'percentual_desconto' 	=> 'ACO_PERDES',
				'vigencia_inicio' 		=> 'ACO_DATDE',
				'vigencia_final' 		=> 'ACO_DATATE',
				'delecao' 				=> 'D_E_L_E_T_'
			),
			'generica' => array(//NAO UTILIZA
				'filial' 				=> 'X5_FILIAL',
				'tabela' 				=> 'X5_TABELA',
				'chave' 				=> 'X5_CHAVE',
				'descricao' 			=> 'X5_DESCRI',
				'descricao_espanhol' 	=> 'X5_DESCSPA',
				'descricao_ingles' 		=> 'X5_DESCENG'
			),
			'representantes_pedidos' => array( //NAO UTILIZA
				'codigo_representante' 	=> '',
				'codigo_pedido' 		=> '',
				),
			'opcionais_produtos' => array(//NAO UTILIZA
				'codigo_produto' 	=> '',
				'codigo' 			=> '',
				'descricao' 		=> ''
				),
			'tipo_entrada_saida' => array(//NAO UTILIZA
				'filial' 					=> 'F4_FILIAL',
				'codigo' 					=> 'F4_CODIGO',
				'cf' 						=> 'F4_CF',
				'base_icm' 					=> 'F4_BASEICM',
				'base_st'					=> 'F4_BSICMST',
				'tipo_entrada_saida_ipi' 	=> 'F4_IPI', 
				'delecao' 					=> 'D_E_L_E_T_'
				)
			)
		);
		
	
	return $config['db_cliente'];

}