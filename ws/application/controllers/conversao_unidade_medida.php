<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Conversao_unidade_medida extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('conversao_unidade_medida_model');
    }

	function exportar_get()
	{
		$conversao_unidade_medida = $this->conversao_unidade_medida_model->exportar_conversao_unidade_medida($this->input->get('id'), $this->input->get('pacote'));
		
		if($conversao_unidade_medida)
        {
            $this->response($conversao_unidade_medida, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Tabelas de preço!'), 404);
        }
	}
	
	
	function total_get()
	{
		$total['total'] = $this->conversao_unidade_medida_model->retornar_total($this->input->get('id'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Tabelas de preço!'), 404);
        }
	}
	
}