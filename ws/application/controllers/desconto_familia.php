<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Desconto_familia extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('desconto_familia_model');
    }

	function exportar_get()
	{
		$dados = $this->desconto_familia_model->exportar_desconto_familia($this->input->get('id'), $this->input->get('pacote'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Descontos das Famílias!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->desconto_familia_model->retornar_total($this->input->get('id'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Descontos das Famílias!'), 404);
        }
	}
	
}