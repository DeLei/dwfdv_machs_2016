<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Pendencias_Usuarios extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('pendencias_usuarios_model');
    }

	function exportar_get()
	{
		$dados = $this->pendencias_usuarios_model->exportar_usuarios($this->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'), TRUE);
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar os usuários para Pendência!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->pendencias_usuarios_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar o Total de usuários para Pendência!'), 404);
        }
	}
	
}