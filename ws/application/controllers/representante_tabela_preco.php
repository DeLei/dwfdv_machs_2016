<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Representante_tabela_preco extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('representante_tabela_preco_model');
    }

	function exportar_get()
	{
		$representante_tabela_preco = $this->representante_tabela_preco_model->exportar_representante_tabela_preco($this->input->get('id'), $this->input->get('pacote'),$this->input->get('codigo_representante'));
		
		if($representante_tabela_preco)
        {
            $this->response($representante_tabela_preco, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Tabelas de preço!'), 404);
        }
	}
	
	
	function total_get()
	{
		$total['total'] = $this->representante_tabela_preco_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Tabelas de preço!'), 404);
        }
	}
	
}