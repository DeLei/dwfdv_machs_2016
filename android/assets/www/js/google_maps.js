$(document).ready(function() {
	db.transaction(function(x) {
		var fragment = parse_url(location.href).fragment;			
			fragment = fragment.split('_');
		
		x.executeSql(
			'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [fragment[0], fragment[1]], function(x, dados) {
				if (dados.rows.length)
				{
					cliente = dados.rows.item(0);
					
					ver_endereco = cliente.endereco+', ' +cliente.bairro+', '+formatar_cep(cliente.cep)+', '+cliente.cidade+' - '+cliente.estado+', Brasil';

					var directionsService = new google.maps.DirectionsService(),
						directionsDisplay = new google.maps.DirectionsRenderer(),
						createMap = function (start) {
							
						
							var travel = {
									origin : (start.coords)? new google.maps.LatLng(start.lat, start.lng) : start.address,
									destination : ver_endereco,
									travelMode : google.maps.DirectionsTravelMode.DRIVING
									// Exchanging DRIVING to WALKING above can prove quite amusing :-)
								},
								mapOptions = {
									zoom: 10,
									// Default view: downtown Stockholm
									center : (start.coords)? new google.maps.LatLng(start.lat, start.lng) : null,
									mapTypeId: google.maps.MapTypeId.ROADMAP
								};

							map = new google.maps.Map(document.getElementById("map"), mapOptions);
							directionsDisplay.setMap(map);
							directionsDisplay.setPanel(document.getElementById("map-directions"));
							directionsService.route(travel, function(result, status) {
								if (status === google.maps.DirectionsStatus.OK) {
									directionsDisplay.setDirections(result);
								}
							});
						};

						// Check for geolocation support	
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function (position) {
									// Success!
									createMap({
										coords : true,
										lat : position.coords.latitude,
										lng : position.coords.longitude
									});
								}, 
								function () {
									// Gelocation fallback: Defaults to Stockholm, Sweden
									createMap({
										coords : false,
										address : "Itajai - SC, Brasil"
									});
								}
							);
						}
						else 
						{
							// No geolocation fallback: Defaults to Lisbon, Portugal
							createMap({
								coords : false,
								address : "Itajai - SC, Brasil"
							});
						}	
			}
		});
	});
});