/*
Utilizado para execução de todas as consultas no DW Força de Vendas.
Caso ocorra algum gerar log em tabela para enviar durante sincronização.
*/

function doQuery(x, query, values, successHandler, errorAction) 
{
    x.executeSql(query, values, successHandler, errorHandler);
    function errorHandler(transaction, error) {
        alert("Error : " + error.message + " in " + query);
        if(errorAction){
        	errorAction(transaction, error);
        } 
    }
}