$(document).ready(function() {
	exibirParcelas();
	var orcamento = parse_url(location.href).fragment;
	if (orcamento == 'O') {
		sessionStorage['sessao_tipo'] = 'sessao_orcamento';
	}
	$("#btn_continuar").on('click', function() {
		salvarParcelas();
		validarParcelas(function(validacao) {
			if (validacao) {
				var orcamento = parse_url(location.href).fragment;
				location.href = "pedidos_adicionar.html#" + orcamento;
			}
		});
	});
});

function calcularAcrescimoCondicaoPagamento(data_entrega) {
	if (obter_valor_sessao('tabela_precos') != obter_valor_sessao('tabela_preco_sugerida')) {
		calcularPrazoMedio(data_entrega, function(prazo_medio) {
			if(prazo_medio > 7) {
				var multiplicador = Math.ceil((prazo_medio / 7) - 1);
				var acrescimo_condicao_pagamento = multiplicador * 0.70;
				acrescimo_condicao_pagamento = number_format(acrescimo_condicao_pagamento, 3, '.', ',');
				salvar_sessao('acrescimo_condicao_pagamento', acrescimo_condicao_pagamento);
			} else {
				salvar_sessao('acrescimo_condicao_pagamento', 0);
			}
		});
	} else {
		salvar_sessao('acrescimo_condicao_pagamento', 0);
	}
}

function calcularPrazoMedio(data_entrega, callback) {
	var prazo_medio = 0;
	var data_atual = data_normal2objDate(data_entrega);
	var parcelas = obter_valor_sessao('parcelas');
	if (parcelas) {
		var numero_parcelas = Object.keys(parcelas).length;
		$.each(parcelas, function(index, parcela) {
			var data_parcela = parcela.data;
			data_parcela = data_parcela.split("-");
			data_parcela = data_parcela[1] + '/' + data_parcela[2] + '/' + data_parcela[0];
			data_parcela = new Date(data_parcela);
			var dias = Math.abs(data_atual - data_parcela);
			dias = Math.round(dias / (1000 * 60 * 60 * 24));
			prazo_medio = prazo_medio + dias;
		});
		prazo_medio = Math.floor(prazo_medio / numero_parcelas);
		salvar_sessao('prazo_medio', prazo_medio);
		callback(prazo_medio);
	}
}

function calcularValorParcela(total_pedido) {
	var parcelas = obter_valor_sessao('parcelas');
	var numero_parcelas = Object.keys(parcelas).length;
	var valor_parcelas = total_pedido / numero_parcelas;
	valor_parcelas = intval((valor_parcelas * 100)) / 100;
	$.each(parcelas, function(index, parcela) {
		parcela.valor = valor_parcelas;
	});
	if (valor_parcelas * numero_parcelas != total_pedido) {
		var diferenca_total = total_pedido - (valor_parcelas * numero_parcelas);
		diferenca_total = number_format(diferenca_total, 3, '.', ',');
		parcelas[numero_parcelas - 1].valor += parseFloat(diferenca_total);
	}
	salvar_sessao('parcelas', parcelas);
}

function exibirParcelas() {
	var parcelas = obter_valor_sessao('parcelas');
	$.each(parcelas, function(index, parcela) {
		$('#parcela').append('<p><b>Parcela ' + parcela.numero + ':</b> <input value="' + protheus_data2data_normal(parcela.data.replace(/-/g, '')) + '" type="text" class="datepicker_data_minima_hoje" size="9" readonly></p>');
	});
	
	
	var data_entrega = obter_valor_sessao('data_entrega');
	
	console.log(data_entrega);
	data_entrega = data_normal2objDate(data_entrega);
	
	
	
	$('.datepicker_data_minima_hoje').datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: data_entrega
	});
}

function exibirTabelaParcelas() {
	tabela = '';
	$('.tabela_parcelas').html('');
	var parcelas = obter_valor_sessao('parcelas');
	$.each(parcelas, function(index, parcela) {
		tabela += '<tr>'
		tabela += '<td align="right">' + parcela.numero + '</td>';
		tabela += '<td align="center">' + protheus_data2data_normal(parcela.data.replace(/-/g, '')) + '</td>';
		tabela += '<td align="right">' + number_format(parcela.valor, 3, ',', '.') + '</td>';
		tabela += '</tr>'
	});
	$('.tabela_parcelas').append(tabela);
}

function gerarParcelas(data_entrega, numero_parcelas) {
	obterDatasVencimento(function(prazos) {
		if (!obter_valor_sessao('parcelas_edicao') || obter_valor_sessao('prazos_edicao')) {
			var parcelas = [];
			var parcela = new Object;
			var teste = [];
			
			var data_inicial = data_normal2objDate(data_entrega);
			data_inicial.setDate(data_inicial.getDate()+prazos[0]);
						
			for (i = 0; i < numero_parcelas; i++) {				
				parcela.numero = (i + 1);
				if (i == 0) {
					parcela.data = data_inicial.toYMD();
				} else if (prazos[1]) {
					console.log(strtotime(dataNormalToDate(parcelas[i - 1].data)));
					parcela.data = date('Y-m-d', strtotime('+' + prazos[1] + ' days', strtotime(dataNormalToDate(parcelas[i - 1].data))));
				} else {
					parcela.data = date('Y-m-d', strtotime('+' + prazos[0] + ' days', strtotime(dataNormalToDate(parcelas[i - 1].data))));
				}
				parcela.valor = 0;
				parcelas.push(parcela);
				parcela = {};
			}
			salvar_sessao('parcelas', parcelas);
		}
		salvar_sessao('prazos_edicao', true);
	});
}

function obterDatasVencimento(callback) {
	var condicao_pagamento = obter_valor_sessao('condicao_pagamento');
	var where = "WHERE codigo_condicao_pagamento = '" + condicao_pagamento + "'";
	var prazos = [];
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM prazo_parcela ' + where, [], function(x, dados) {
			var total_itens = dados.rows.length;
			if (total_itens) {
				for (var i = 0; i < total_itens; i++) {
					prazos.push(parseInt(dados.rows.item(i).quantidade_dias));
				}
			}
			callback(prazos);
		})
	});
}

function salvarParcelas() {
	var parcelas = obter_valor_sessao('parcelas');
	$('.datepicker_data_minima_hoje').each(function(index) {
		parcelas[index].data = $(this).val().replace(/\//g, '');
		parcelas[index].data = parcelas[index].data.substring(4, 8) + '-' + parcelas[index].data.substring(2, 4) + '-' +  parcelas[index].data.substring(0, 2);
	});
	salvar_sessao('parcelas', parcelas);
}

function validarParcelas(callback) {
	var validacao = true;
	var parcelas = obter_valor_sessao('parcelas');
	for (var i = 0; i < Object.keys(parcelas).length; i++) {
		for (var j = 0; j < Object.keys(parcelas).length; j++) {
			if (parcelas[i].numero < parcelas[j].numero) {
				if (data_normal2protheus(parcelas[i].data) > data_normal2protheus(parcelas[j].data)) {
					validacao = false;
					mensagem('A parcela ' + parcelas[i].numero + ' possui uma data de vencimento maior do que a parcela ' + parcelas[j].numero + '.');
				}
			}
		}
	}
	if (validacao) {
		callback(true)
	}
}