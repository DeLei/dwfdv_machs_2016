$(document).ready(function() {

	// Desativar todos os campos quando clicar em id_informacao_cliente
	$('#id_outras_informacoes').click(function(){
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',2);
		
		ativar_tabs();
	});

	/**
	* 
	* tipo_frete
	*
	* Descrição:	Utilizado para obter o tipo frete
	*
	*/
	$('select[name=tipo_frete]').change(function(){
		obter_dirigido_redespacho($(this).val());
	});
	
	/**
	* 
	* ID:			trocar_cliente
	*
	* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
	*
	*/
	$('#trocar_cliente_entrega').live('click', function(){
		
		salvar_sessao('descricao_cliente_entrega', null);
		salvar_sessao('codigo_cliente_entrega', null);
		salvar_sessao('loja_cliente_entrega', null);
		
		$('input[name="cliente_entrega"]').val("");
		$('input[name="codigo_cliente_entrega"]').val("");
		$('input[name="loja_cliente_entrega"]').val("");
		
		$('.endereco_entrega').html('N/A');
		$('.bairro_entrega').html('N/A');
		$('.cep_entrega').html('N/A');
		$('.cidade_entrega').html('N/A');
		$('.estado_entrega').html('N/A');
		
		$('input[name="cliente_entrega"]').removeAttr('disabled');
		
	});
	
	$('#avancar_passo_3').click(function(e){
		e.preventDefault();
		
		/*
		if(!$('select[name=tipo_frete]').val())
		{
			mensagem('Selecione um <b>Tipo de Frete</b>.');
		}
		else if(!$('input[name=transportadora]').val())
		{
			mensagem('Selecione uma <b>Transportadora</b>.');
		}
		*/
		if(!$('input[name=codigo_cliente_entrega]').val() && !obter_valor_sessao('codigo_prospect'))
		{
			mensagem('Selecione um <b>Cliente de Entrega</b>.');
		}
		else
		{	
			//salvar_dados_transportadoras($('input[name=codigo_transportadora]').val());
			salvar_sessao('observacao_comercial', $('textarea[name=observacao_comercial]').val());
			salvar_sessao('mennota', $('textarea[name=mennota]').val());
			salvar_sessao('pedido_cliente', $('input[name=pedido_cliente]').val());
				
			var clienteEntrega = $('input[name=codigo_cliente_entrega]').val();
						
			salvar_sessao('codigo_cliente_entrega', clienteEntrega);			
			
			// Ativando Botao "Finalizar"
			$('#id_finalizar').removeClass('tab_desativada');
			
			ativar_tabs();
			
			//Chamando as funções do arquivo finalizar
			finalizar();
			
			// Acionando (Click) botao "finalizar"
			$("#id_finalizar").click();
		}
		
	});
		
	// Pegando valores da sessao e adicionado no html
	$('input[name=pedido_cliente]').val(obter_valor_sessao('pedido_cliente') ? obter_valor_sessao('pedido_cliente') : '');
	//$('input[name=data_entrega]').val(obter_valor_sessao('data_entrega') ? obter_valor_sessao('data_entrega') : date('d/m/Y', time() + 3600 * 24 * 14));
	$('input[name=codigo_cliente_entrega]').val(obter_valor_sessao('codigo_cliente_entrega') ? obter_valor_sessao('codigo_cliente_entrega') : '');
	$('textarea[name=mennota]').val(obter_valor_sessao('mennota')? obter_valor_sessao('mennota') : '');
	$('textarea[name=observacao_comercial]').val(obter_valor_sessao('observacao_comercial') ? obter_valor_sessao('observacao_comercial') : '');
	
});

function outras_informacoes()
{
	
	obter_eventos();
	
	obter_tipo_frete();
	
	obter_dirigido_redespacho(obter_valor_sessao('dirigido_redespacho'));
	
	obter_transportadoras(obter_valor_sessao('filial'));
	
	obter_clientes_entrega();
	
	// Rotina para orçamento e prospect(Não Não existe cliente de entrega para prospect.)
	if(obter_valor_sessao('codigo_prospect'))
	{
		$('input[name=cliente_entrega]').val('Não existe cliente de entrega para prospect.');
		$('input[name=cliente_entrega]').attr('disabled', 'disabled');
		$('#trocar_cliente_entrega').hide();
	}
	else
	{
		$('#trocar_cliente_entrega').show();
	}
}

/**
* Metódo:		obter_eventos
* 
* Descrição:	Função Utilizada para retornar Eventos
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_eventos()
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM eventos', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=evento]').append('<option value="">Selecione...</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=evento]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
				}
				
				// Selecionar campo se existir na sessão
				if(obter_valor_sessao('evento'))
				{
					$('select[name="evento"] option[value="' + obter_valor_sessao('evento') + '"]').attr('selected', 'selected');
				}
				
			}
		);
	});
}

/**
* Metódo:		obter_tipo_frete
* 
* Descrição:	Função Utilizada para retornar Tipos de Frete
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_frete()
{
	$('select[name=tipo_frete]').empty();
	$('select[name=tipo_frete]').append('<option value="">Selecione...</option>');
	$('select[name=tipo_frete]').append('<option value="F">FOB</option>');
	$('select[name=tipo_frete]').append('<option value="C">CIF</option>');
	
	var tipo_frete 	= obter_valor_sessao('tipo_frete');
	
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		var sessao_pedido 	= [];		
			sessao_pedido 	= unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		
		if(sessao_pedido.dados_cliente)
		{
			
			if(sessao_pedido.dados_cliente.tipo_frete)
			{
				tipo_frete = sessao_pedido.dados_cliente.tipo_frete;
			}
		}	
	}
	
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('tipo_frete'))
	{
		$('select[name="tipo_frete"] option[value="' + obter_valor_sessao('tipo_frete') + '"]').attr('selected', 'selected');

		//Alterar
		obter_dirigido_redespacho(obter_valor_sessao('tipo_frete'));
	}
	else
	{
		$('select[name="tipo_frete"] option[value="' + tipo_frete + '"]').attr('selected', 'selected');

		//Alterar
		obter_dirigido_redespacho(tipo_frete);
	}
}

/**
* Metódo:		obter_dirigido_redespacho
* 
* Descrição:	Função Utilizada para retornar tipo de frete (Dirigido ou Redespacho)
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @param		string 			tipo	- Tipo de Frete
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_dirigido_redespacho(tipo)
{
	if(tipo)
	{
		$('select[name=dirigido_redespacho]').empty();
		$('select[name=dirigido_redespacho]').removeAttr('disabled');
	
		//if(tipo == 'F')
		//{
			//$('select[name=dirigido_redespacho]').append('<option value="0">Não Dirigido</option>');
			//$('select[name=dirigido_redespacho]').append('<option value="1">Dirigido</option>');
		//}
		//else
		//{
			$('select[name=dirigido_redespacho]').append('<option value="0">Sem Redespacho</option>');
			$('select[name=dirigido_redespacho]').append('<option value="1">Redespacho</option>');
		//}
		
		// Selecionar campo se existir na sessão
		if(obter_valor_sessao('dirigido_redespacho'))
		{
			$('select[name="dirigido_redespacho"] option[value="' + obter_valor_sessao('dirigido_redespacho') + '"]').attr('selected', 'selected');
		}
		
		$('select[name=dirigido_redespacho]').change();
	}
	else
	{
		$('select[name=dirigido_redespacho]').html('<option value="0">Selecione Tipo de Frete</option>');
		$('select[name=dirigido_redespacho]').attr('disabled', 'disabled');
	}
}

//////////////////////////////////////////////////////////////////////////////
// TRANSPORTADORAS
//////////////////////////////////////////////////////////////////////////////

/**
* Metódo:		obter_transportadoras
* 
* Descrição:	Função Utilizada para retornar Transportadoras
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_transportadoras(filial)
{
	if(info.empresa)
	{
		var wheres = " WHERE empresa = '" + info.empresa + "'";
	}
	if(filial)
	{
		wheres += " AND (filial = '" + filial + "' OR filial = '')";
	}
	
	

	var transportadora 	= '';
	
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		var sessao_pedido 	= [];		
			sessao_pedido 	= unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		
		if(obter_valor_sessao('codigo_transportadora')) {
			transportadora = obter_valor_sessao('codigo_transportadora');
		} else if(sessao_pedido.dados_cliente) {
			
			if(sessao_pedido.dados_cliente.transportadora)
			{
				transportadora = sessao_pedido.dados_cliente.transportadora;
			}
		}
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM transportadoras ' + wheres, [], function(x, dados) {
				if (dados.rows.length)
				{
					var transportadoras = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						transportadoras.push({ 
												label: 		dado.codigo + ' - ' + dado.nome + ' - Telefone: '+(dado.telefone ? dado.telefone : 'N/A'), 
												codigo:	 	dado.codigo, 
												nome:	 	dado.nome
											});
						
						//Selecionar transportadora padrão
						if(transportadora == dado.codigo)
						{
							var label = dado.codigo + ' - ' + dado.nome + ' - Telefone: '+(dado.telefone ? dado.telefone : 'N/A');
							$('input[name=transportadora]').val(label);
							$('input[name=codigo_transportadora]').val(dado.codigo);
							$('input[name=nome_transportadora]').val(dado.nome);
							
							salvar_sessao('transportadora', 		label);
							salvar_sessao('codigo_transportadora', 	dado.codigo);
							salvar_sessao('nome_transportadora', 	dado.nome);
							
							$('input[name=transportadora]').attr('disabled', 'disabled');
							$('#trocar_transportadora').show();
							
							
							
						}
					}
					
					buscar_transportadoras(transportadoras);
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('codigo_transportadora'))
					{
						$('input[name=codigo_transportadora]').val(obter_valor_sessao('codigo_transportadora'));
						$('input[name=nome_transportadora]').val(obter_valor_sessao('nome_transportadora'));						
					}					
				}
			}
		);
	});

}
/**
* Metódo:		buscar_transportadoras
* 
* Descrição:	Função Utilizada para buscar as transportadoras digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos as transportadoras 
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_transportadoras(transportadoras)
{
	
	
	$('input[name=transportadora]').autocomplete({
		minLength: 3,
		source: transportadoras,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=transportadora]').val(ui.item.label);
			$('input[name=codigo_transportadora]').val(ui.item.codigo);
			$('input[name=nome_transportadora]').val(ui.item.nome);
			
			salvar_sessao('transportadora', ui.item.label);
			salvar_sessao('codigo_transportadora', ui.item.codigo);
			salvar_sessao('nome_transportadora', ui.item.nome);

			$('input[name=transportadora]').attr('disabled', 'disabled');
			$('#trocar_transportadora').show();
			
			return false;
		}
	});
}
/**
* 
* ID:			trocar_cliente
*
* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
*
*/
$('#trocar_transportadora').live('click', function(){
	
	salvar_sessao('transportadora', null);
	salvar_sessao('codigo_transportadora', null);
	salvar_sessao('nome_transportadora', null);

	$('input[name="transportadora"]').val("");
	$('input[name="codigo_transportadora"]').val("");
	$('input[name="nome_transportadora"]').val("");
	
	$('input[name=transportadora]').removeAttr('disabled');
	
	
	$(this).hide();	
});
//////////////////////////////////////////////////////////////////////////////
//TRANSPORTADORAS
//////////////////////////////////////////////////////////////////////////////

/**
* Metódo:		obter_clientes_entrega
* 
* Descrição:	Função Utilizada para retornar Todos os Clientes
* 
* Data:			25/10/2013
* Modificação:	25/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_clientes_entrega()
{

	if(info.empresa)
	{
		var where = " WHERE empresa = '" + info.empresa + "'";
	}

	db.transaction(function(x) {
		x.executeSql('SELECT * FROM clientes ' + where, [], function(x, dados) {
				if (dados.rows.length)
				{
					var clientes = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						clientes.push({ label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf, codigo: dado.codigo, loja: dado.loja, endereco: dado.endereco, bairro: dado.bairro, cep: dado.cep, cidade: dado.cidade, estado: dado.estado});
					}
					
					buscar_clientes_entrega(clientes);
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('codigo_cliente_entrega'))
					{
						$('input[name=cliente_entrega]').val(obter_valor_sessao('descricao_cliente_entrega'));
						$('input[name=codigo_cliente_entrega]').val(obter_valor_sessao('codigo_cliente_entrega'));
						$('input[name=loja_cliente_entrega]').val(obter_valor_sessao('loja_cliente_entrega'));
						
						exibir_informacoes_cliente_entrega(obter_valor_sessao('codigo_cliente_entrega'), obter_valor_sessao('loja_cliente_entrega'));
						
						// Bloquear campo quando selecionar cliente
						$('input[name=cliente_entrega]').attr('disabled', 'disabled');
					}
				
				}
			}
		);
	});

}

/**
* Metódo:		buscar_clientes_entrega
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_clientes_entrega(clientes)
{
	$('input[name=cliente_entrega]').autocomplete({
		minLength: 3,
		source: clientes,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=cliente_entrega]').val(ui.item.label);
			$('input[name=codigo_cliente_entrega]').val(ui.item.codigo);
			$('input[name=loja_cliente_entrega]').val(ui.item.loja);
			
			salvar_sessao('descricao_cliente_entrega', ui.item.label);
			salvar_sessao('codigo_cliente_entrega', ui.item.codigo);
			salvar_sessao('loja_cliente_entrega', ui.item.loja);
			
			exibir_informacoes_cliente_entrega(ui.item.codigo, ui.item.loja);
			
			// Bloquear campo quando selecionar cliente
			$('input[name=cliente_entrega]').attr('disabled', 'disabled');

			return false;
		}
	});
}


/**
* Metódo:		exibir_informacoes_cliente_entrega
* 
* Descrição:	Função Utilizada para exibir endereço do cliente de entrega
* 
* Data:			26/10/2013
* Modificação:	26/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_cliente_entrega(codigo, loja)
{
	if(codigo && loja)
	{
		if(info.empresa)
		{
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM clientes WHERE codigo = ? AND loja = ? ' + where, [codigo, loja], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						$('.endereco_entrega').html(dado.endereco ? dado.endereco : 'N/A');
						$('.bairro_entrega').html(dado.bairro ? dado.bairro : 'N/A');
						$('.cep_entrega').html(dado.cep ? dado.cep : 'N/A');
						$('.cidade_entrega').html(dado.cidade ? dado.cidade : 'N/A');
						$('.estado_entrega').html(dado.estado ? dado.estado : 'N/A');
						
					}
					
				}
			);
		});
	}
}




/**
* Metódo:		salvar_dados_transportadoras
* 
* Descrição:	Função Utilizada para salvar dados da transportadora
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo forma de pagamento
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_transportadoras(codigo)
{
	if(codigo)
	{
		if(info.empresa)
		{
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM transportadoras WHERE codigo = ? ' + where, [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);

						salvar_sessao('dados_transportadoras', dado);
						
					}
					
				}
			);
		});
	}
}
