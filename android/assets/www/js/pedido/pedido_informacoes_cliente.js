tabelas_precos_sistema = [];
	
$(document).ready(function() {

    if (!localStorage.getItem('data_atualizacao_estoque')) {
        localStorage.setItem('data_atualizacao_estoque', 0);
    }

    if (localStorage.getItem('data_atualizacao_estoque') <= time()) {
        var mensagem_html = 'Deseja atualizar o estoque dos produtos?<br /><br />';
        mensagem_html += '<div style="float:left; padding: 10px 5px 0 0;">Sincronizar: </div>';
        mensagem_html += '		<form><select id="data_atualizacao_estoque" name="data_atualizacao_estoque">';
        mensagem_html += '			<option selected value="agora">Agora</option>';
        mensagem_html += '			<option value="' + (60 * 60) + '">1 hora</option>';
        mensagem_html += '			<option value="' + (60 * 60 * 4) + '">4 horas</option>';
        mensagem_html += '			<option value="' + (60 * 60 * 24) + '">1 dia</option>';
        mensagem_html += '			<option value="' + (60 * 60 * 24 * 7) + '">1 semana</option>';
        mensagem_html += '		</select></form>';

        confirmar(mensagem_html,
            function() {
                $(this).dialog('close');

                if ($('#data_atualizacao_estoque').val() == 'agora') {
                    localStorage.setItem('data_atualizacao_estoque', time() + (60 * 60));

                    location.href = "sincronizar.html?produtos=sincronizar";
                } else {
                    localStorage.setItem('data_atualizacao_estoque', time() + parseInt($('#data_atualizacao_estoque').val()));
                }

                $("#confirmar_dialog").remove();
            });
    }
    
    var orcamento = parse_url(location.href).fragment;

    // Salvando o Tipo de Sessaão (Orçamento ou pedido)
    if (orcamento == 'O') {
        sessionStorage['sessao_tipo'] = 'sessao_orcamento';

        // Atalho de criar orçamento no espelho do prospect (retornar prospect selecionado)
        if (sessionStorage[sessionStorage['sessao_tipo']]) {
            var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);

            if (sessao_pedido.codigo_prospect) {
                $('select[name="tabela_precos"]').removeAttr('disabled');
                $('select[name=forma_pagamento]').removeAttr('disabled');
            }
        }

        // Trocando a descrição de Pedido para Orçamento
        $('.descricao_pedido').html('Orçamento');

        // Exibindo as opções de Cliente e Prospect
        $('#cliente_prospect').show();
    } else {
        sessionStorage['sessao_tipo'] = 'sessao_pedido';
    }
   
	// Machs apenas utiliza Venda Normal
   // salvar_sessao('tipo_pedido', 'N');
    
    // Machs possui apenas uma filial
    salvar_sessao('filial', '1');
    
    // Ativando Biblioteca "TABS"
    $("#tabs").tabs();

    // Desativar todos os campos quando clicar em id_informacao_cliente
    $('#id_informacao_cliente').click(function() {

        $('#id_adicionar_produtos').addClass('tab_desativada');
        $('#id_outras_informacoes').addClass('tab_desativada');
        $('#id_finalizar').addClass('tab_desativada');

        $("#tabs").tabs('select', 0);

        ativar_tabs();
    });

    ativar_tabs();

    obter_filiais();

    obter_tipo_pedido();

    if (obter_valor_sessao('filial')) {
    	
    	
    	
        obter_clientes(obter_valor_sessao('filial'));
        obter_prospects(obter_valor_sessao('filial'));
    }
    
    /**
     * 
     * Classe:		select_gravar_sessao
     *
     * Descrição:	Classe utilizada para "select", com a finalizade de gravar na sessão o valor do campo quando o usuário usar o "change"
     *
     */
    $('.select_gravar_sessao').live('change', function() {
    	
    	console.log('salvar_sessao('+$(this).attr('name')+', '+$(this).val()+');');
        salvar_sessao($(this).attr('name'), $(this).val());
        if ($(this).attr('name') == 'filial') {
            obter_clientes($(this).val());

            obter_prospects($(this).val());
        }
    });
    
    obter_tabela_precos();
    obter_condicoes_pagamento();
    obter_formas_pagamento();
    remover_disabled();

    /**
     * 
     * ID:			trocar_cliente
     *
     * Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
     *
     */
    $('#trocar_cliente').live('click', function() {

        salvar_sessao('descricao_cliente', null);
        salvar_sessao('codigo_cliente', null);
        salvar_sessao('loja_cliente', null);

        $('input[name="cliente"]').val("");
        $('input[name="cliente"]').removeAttr('readonly', '');
        $('input[name="codigo_cliente"]').val("");
        $('input[name="loja_cliente"]').val("");
        $('input[name="numero_parcelas"]').val("");
        
        rotina_cliente();

        $(this).hide();
        $('#info_cli').hide();
    });

    /**
     * 
     * ID:			trocar_prospect
     *
     * Descrição:	Utilizado para apagar todos os dados do prospect na sessão, e forçar que o representante selecione outro prospect
     *
     */
    $('#trocar_prospect').live('click', function() {

        salvar_sessao('descricao_prospect', null);
        salvar_sessao('codigo_prospect', null);
        salvar_sessao('loja_prospect', null);

        $('input[name="prospect"]').val("");
        $('input[name="prospect"]').removeAttr('readonly', '');
        $('input[name="codigo_prospect"]').val("");
        $('input[name="loja_prospect"]').val("");
        $('input[name="numero_parcelas"]').val("");
        
        //-----------------------------------------------

        $('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
        $('select[name="tabela_precos"]').change();
        $('select[name="tabela_precos"]').attr('disabled', 'disabled');

        $('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
        $('select[name="condicao_pagamento"]').change();
        $('select[name="condicao_pagamento"]').attr('disabled', 'disabled');

        $('select[name="forma_pagamento"] option[value=""]').attr('selected', 'selected');
        $('select[name="forma_pagamento"]').change();
        $('select[name="forma_pagamento"]').attr('disabled', 'disabled');

        //-----------------------------------------------

        $(this).hide();
        $('#info_pro').hide();

    });

    $('select[name="cliente_prospect"]').live('change', function() {

        if ($(this).val() == 'P') {
            $('#campo_cliente').hide();
            $('#campo_prospect').show();

            //Limpando Dados do Cliente e Prospect
            $('#trocar_cliente').click();
            $('#trocar_cliente_entrega').click();
        } else {
            $('#campo_prospect').hide();
            $('#campo_cliente').show();

            //Limpando Dados do Cliente e Prospect
            $('#trocar_prospect').click();
        }
    });

    /**
     * 
     * CLASSE:		avancar
     *
     * Descrição:	Utilizado para validar os campos, se estiver tudo correto, passar para a proxima ABA
     *
     */
    $('#avancar_passo_1').live('click', function(e) {
        e.preventDefault();
        
        if (sessionStorage[sessionStorage['sessao_tipo']]) {
            var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
            sessao_pedido.numero_parcelas = $('input[name=numero_parcelas]').val();
        } else {
            var sessao_pedido = [];
        }
        if (!sessao_pedido.filial) {
            mensagem('Selecione uma <strong>Filial</strong>.');
        } else if (!sessao_pedido.tipo_pedido) {
            mensagem('Selecione um <strong>Tipo de Pedido</strong>.');
        } else if (!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido') {
            mensagem('Selecione um <strong>Cliente</strong>.');
        } else if (!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null)) {
            mensagem('Selecione um <strong>Cliente</strong>.');
        } else if (!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P') {
            mensagem('Selecione um <strong>Prospect</strong>.');
        } else if (!sessao_pedido.tabela_precos) {
            mensagem('Selecione uma <strong>Tabela de Preços</strong>.');
        } else if (!sessao_pedido.forma_pagamento) {
            mensagem('Selecione uma <strong>Forma de Pagamento</strong>.');
        } else if(!$('input[name=data_entrega]').val())	{
			mensagem('Informe uma <b>Data de Entrega</b>.');
		}
		else  if (!sessao_pedido.condicao_pagamento) {
            mensagem('Selecione uma <strong>Condição de Pagamento</strong>.');
        } else {
        	validarParcelas(function(validacao) { 
        		if (validacao) {
        			salvar_dados_condicao_pagamento(sessao_pedido.condicao_pagamento);
                    salvar_dados_forma_pagamento(sessao_pedido.forma_pagamento);
                    salvar_dados_cliente(sessao_pedido.codigo_cliente, sessao_pedido.loja_cliente);
                    salvar_dados_prospect(sessao_pedido.codigo_prospect, sessao_pedido.loja_prospect);         
                    salvar_sessao('data_entrega', $('input[name=data_entrega]').val());
                    
                    
                    calcularAcrescimoCondicaoPagamento(obter_valor_sessao('data_entrega'));
                    
                    // Ativando Botao "Adicionar Produtos"
                    $('#id_adicionar_produtos').removeClass('tab_desativada');

                    ativar_tabs();

                    //Chamando as funções do arquivo adicionar produtos
                    adicionar_produtos();

                    // Acionando (Click) botao "Adicionar Produtos"
                    $("#id_adicionar_produtos").click();
                    
                    $('input[name=produto]').focus();
        		}
        	});
        }
    });

    $('.cancelar').click(function(e) {
        e.preventDefault();

        confirmar('Deseja cancelar esse pedido?',
            function() {
                $(this).dialog('close');

                //-----------------

                sessionStorage[sessionStorage['sessao_tipo']] = '';
                location.reload();

                //-----------------

                $("#confirmar_dialog").remove();
            }
        );

    });
    
    if (obter_valor_sessao('parcelas')) {
        $('input[name=numero_parcelas]').val(obter_valor_sessao('parcelas'));
    }

    if (obter_valor_sessao('condicao_pagamento')) {
    	$('select[name="condicao_pagamento"] option[value="' + obter_valor_sessao('condicao_pagamento') + '"]').attr('selected', 'selected');
    }
    
    if (obter_valor_sessao('condicao_pagamento')) {
    	$('input[name=data_entrega]').val(obter_valor_sessao('data_entrega') ? obter_valor_sessao('data_entrega') : '');
    }
    
    
    $('input[name=data_entrega]').on('change', function() {
    	
        var codigo_condicao_pagamento = $('select[name="condicao_pagamento"]').val();
        var condicao_pagamento_sessao = obter_valor_sessao('condicao_pagamento');
        var data_entrega_sessao = obter_valor_sessao('data_entrega');
        var data_entrega = $('input[name=data_entrega]').val();
       
        obter_numero_parcelas(data_entrega, data_entrega_sessao, codigo_condicao_pagamento, condicao_pagamento_sessao);
       
        
        obter_prazo_medio(codigo_condicao_pagamento, data_entrega);
        
        salvar_sessao('condicao_pagamento', codigo_condicao_pagamento);
        salvar_sessao('data_entrega', data_entrega);
    });
    
    
    $('select[name="condicao_pagamento"]').on('change', function() {
        var codigo_condicao_pagamento = $('select[name="condicao_pagamento"]').val();
        var condicao_pagamento_sessao = obter_valor_sessao('condicao_pagamento');
        var data_entrega_sessao = obter_valor_sessao('data_entrega');
        var data_entrega = $('input[name=data_entrega]').val();
       
                
        obter_numero_parcelas(data_entrega, data_entrega_sessao, codigo_condicao_pagamento, condicao_pagamento_sessao);
        obter_prazo_medio(codigo_condicao_pagamento, data_entrega);
        salvar_sessao('condicao_pagamento', codigo_condicao_pagamento);
        salvar_sessao('data_entrega', data_entrega);
    });
    
    
    
    
         
    $("#btn_vencimento").on('click', function() {
        var data_entrega = $('input[name=data_entrega]').val();
    	console.log('datas: ');
        console.log('Data Entrega: '+data_normal2objDate(data_entrega).getTime());
        console.log('Data Atual: '+data_normal2objDate(date('d/m/Y')).getTime());
        
        if(isValidDate(data_entrega)){
        	if(data_normal2objDate(data_entrega).getTime() >=  data_normal2objDate(date('d/m/Y')).getTime() ){
        		var numero_parcelas = $('input[name=numero_parcelas]').val();
		        if(numero_parcelas > 0) {
		        	salvar_sessao('numero_parcelas', numero_parcelas);
		        	location.href = "pedido_parcelas.html#" + orcamento;
		        }
        	}else{        		
        		mensagem('Data de Entrega não pode ser inferior a data atual.');
        	}
        
        }else{
        	mensagem('Data de Entrega Inválida.');        	
        }
    });    

});

/**
 * Metódo:		ativar_tabs
 * 
 * Descrição:	Função Utilizada ativar a biblioteca, e desativar tabs com a classe tab_desativada
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function ativar_tabs() {
    // Desativando tabs com classe tab_desativada
    var tabs_desativar = [];

    $("#tabs ul li a").each(function(e, i) {
        var classname = i.className;

        if (classname === 'tab_desativada') {
            tabs_desativar.push(e);
        }
    });

    $("#tabs").tabs({
        disabled: tabs_desativar
    });
}

/**
 * Metódo:		buscar_clientes
 * 
 * Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @param		array 			var clientes		- Todos os clientes do representante
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function buscar_clientes(clientes) {
    
    $('input[name=cliente]').autocomplete({
        minLength: 3,
        source: clientes,
        position: {
            my: "left bottom",
            at: "left top",
            collision: "none"
        },
        select: function(event, ui) {
        	
            $('input[name=cliente]').val(ui.item.label);
            $('input[name=codigo_cliente]').val(ui.item.codigo);
            $('input[name=loja_cliente]').val(ui.item.loja);

            salvar_sessao('descricao_cliente', ui.item.label);
            salvar_sessao('codigo_cliente', ui.item.codigo);
            salvar_sessao('loja_cliente', ui.item.loja);
            salvar_sessao('desconto_cliente', ui.item.desconto);

            // Salvando Cliente de Entrega
            salvar_sessao('descricao_cliente_entrega', ui.item.label);
            salvar_sessao('codigo_cliente_entrega', ui.item.codigo);            
            salvar_sessao('loja_cliente_entrega', ui.item.loja);
            salvar_sessao('codigo_transportadora', ui.item.condicao_pagamento);
            
            
            if(ui.item.total_titulos_vencidos > 0 ){
            	
            	mensagem('Cliente com títulos em atraso. <br/>  Pedido sujeito a aprovação.');	
            }
            salvar_sessao('total_titulos_vencidos', ui.item.total_titulos_vencidos);
            
            
            exibir_informacoes_cliente(ui.item.codigo, ui.item.loja);           
            salvar_sessao('tabela_preco_sugerida', ui.item.tabela_preco);
            rotina_cliente(ui.item.tabela_preco);
            obter_tabela_precos();
            
            // Bloquear campo quando selecionar cliente
            $('input[name=cliente]').attr('readonly', 'readonly');
            $('input[name=tabela_precos]').focus();
            $('#trocar_cliente').show();

            return false;
        }
    });
}

/**
 * Metódo:		buscar_prospects
 * 
 * Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @param		array 			var clientes		- Todos os clientes do representante
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function buscar_prospects(prospects) {

    $('input[name=prospect]').autocomplete({
        minLength: 3,
        source: prospects,
        position: {
            my: "left bottom",
            at: "left top",
            collision: "none"
        },
        select: function(event, ui) {

            $('input[name=prospect]').val(ui.item.label);
            $('input[name=codigo_prospect]').val(ui.item.codigo);
            $('input[name=loja_prospect]').val(ui.item.loja);

            salvar_sessao('descricao_prospect', ui.item.label);
            salvar_sessao('codigo_prospect', ui.item.codigo);
            salvar_sessao('loja_prospect', ui.item.loja);
            salvar_sessao('desconto_prospect', ui.item.desconto);

            exibir_informacoes_prospect(ui.item.codigo, ui.item.loja);

            $('select[name=tabela_precos]').removeAttr('disabled', '');
            $('select[name=forma_pagamento]').removeAttr('disabled', '');

            // Bloquear campo quando selecionar prospect
            $('input[name=prospect]').attr('readonly', 'readonly');
            $('select[name=tabela_precos]').focus();
            $('#trocar_prospect').show();

            return false;
        }
    });
}

/**
 * Metódo:		exibir_informacoes_cliente
 * 
 * Descrição:	Função Utilizada para exibir informações do cliente
 * 
 * Data:			11/10/2013
 * Modificação:	11/10/2013
 * 
 * @access		public
 * @param		String 			var codigo		- Codigo do Cliente
 * @param		String 			var loja		- Loja do cliente
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function exibir_informacoes_cliente(codigo, loja) {
    if (codigo && loja) {
        if (info.empresa) {
            var where = " AND empresa = '" + info.empresa + "'";
        }

        db.transaction(function(x) {
            doQuery(x,
                'SELECT * FROM clientes WHERE codigo = ? AND loja = ? ' + where, [codigo, loja],
                function(x, dados) {

                    if (dados.rows.length) {
                        var dado = dados.rows.item(0);
                        
                        var html = '';

                        $('.info_nome').html(dado.nome);
                        $('.info_cpf').html(dado.cpf);

                        obter_ultimo_pedido_cliente(codigo, loja);

                        $('.info_limite_credito').html(number_format(dado.limite_credito, 3, ',', '.'));
                        $('.info_titulos_aberto').html(number_format(dado.total_titulos_aberto, 3, ',', '.'));
                        $('.info_titulos_vencidos').html(number_format(dado.total_titulos_vencidos, 3, ',', '.'));
                        $('.info_credito_disponivel').html(number_format(dado.limite_credito - dado.total_titulos_aberto, 3, ',', '.'));

                        $('.info_endereco').html(dado.endereco);
                        $('.info_bairro').html(dado.bairro);
                        $('.info_cidade').html(dado.cidade);
                        $('.info_estado').html(dado.estado);

                        $('.info_cep').html(dado.cep);
                        
                        if($('select[name="condicao_pagamento"]').val() == '') {
                        	$('select[name="condicao_pagamento"] option[value="' + dado.condicao_pagamento + '"]').attr('selected', 'selected');
                        	$('select[name="condicao_pagamento"]').change();
                        }
                        
                        /*Tratamento telefone*/
                        var telefone = '';
                        if (dado.ddd) {
                            telefone = '(' + dado.ddd + ') ' + dado.telefone;
                        } else {
                            if (dado.telefone) {
                                telefone = dado.telefone;
                            }
                        }
                        /*Tratamento telefone*/
                        $('.info_telefone').html(telefone);

                        $('#info_cli').show();

                    }
                }
            );
        });
    }
}

/**
 * Metódo:		exibir_informacoes_prospect
 * 
 * Descrição:	Função Utilizada para exibir informações do prospect
 * 
 * Data:			13/11/2013
 * Modificação:	13/11/2013
 * 
 * @access		public
 * @param		String 			var codigo		- Codigo do Prospect
 * @param		String 			var loja		- Loja do Prospect
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function exibir_informacoes_prospect(codigo, loja) {
    if (codigo && loja) {
        if (info.empresa) {
            var where = " AND empresa = '" + info.empresa + "'";
        }

        db.transaction(function(x) {
            doQuery(x,
                'SELECT * FROM prospects WHERE codigo = ? AND codigo_loja = ? ' + where, [codigo, loja],
                function(x, dados) {

                    if (dados.rows.length) {
                        var dado = dados.rows.item(0);

                        var html = '';

                        $('.info_nome').html(dado.nome);
                        $('.info_cpf').html(dado.cgc);

                        $('.info_endereco').html(dado.endereco);
                        $('.info_bairro').html(dado.bairro);
                        $('.info_cidade').html(dado.nome_municipio);
                        $('.info_estado').html(dado.estado);

                        $('.info_cep').html(dado.cep);

                        if($('select[name="condicao_pagamento"]').val() == '') {
                        	$('select[name="condicao_pagamento"] option[value="' + dado.condicao_pagamento + '"]').attr('selected', 'selected');
                        	$('select[name="condicao_pagamento"]').change();
                        }
                        
                        /*Tratamento telefone*/
                        var telefone = '';
                        if (dado.ddd) {
                            telefone = '(' + dado.ddd + ') ' + dado.telefone;
                        } else {
                            if (dado.telefone) {
                                telefone = dado.telefone;
                            }
                        }
                        /*Tratamento telefone*/
                        $('.info_telefone').html(telefone);

                        $('#info_pro').show();
                    }
                }
            );
        });
    }
}

function gerar_parcelas_pedido_edicao(id_pedido) {
 var where = " WHERE pedido_parcela_id_pedido = '" + id_pedido + "'";
 var parcelas = [];
 var parcela = new Object;
    db.transaction(function(x) {
        doQuery(x, 'SELECT * FROM pedido_parcela' + where, [], function(x, dados) {
        	var total_parcelas = dados.rows.length;
        	if(total_parcelas) {
        		for(var i = 0; i < total_parcelas; i++) {
        			var dado = dados.rows.item(i);
        			parcela.numero = parseInt(dado.pedido_parcela_numero_parcela);
        	        parcela.data = dado.pedido_parcela_data_vencimento;
        	        parcela.valor = dado.pedido_parcela_valor_parcela;
        	        parcelas.push(parcela);
        	        parcela = {};
        		}
        		salvar_sessao('parcelas', parcelas);
        		salvar_sessao('parcelas_edicao', true);
        	}
        });
    });
}

/**
 * Metódo:		obter_clientes
 * 
 * Descrição:	Função Utilizada para retornar Todos os Clientes
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_clientes(filial) {
    if (info.empresa) {
        var wheres = " WHERE empresa = '" + info.empresa + "'";
    }
    if (filial) {
        wheres += " AND (filial = '" + filial + "' OR filial = '')";
    }

   

    db.transaction(function(x) {
        doQuery(x, 'SELECT * FROM clientes ' + wheres, [], function(x, dados) {
            if (dados.rows.length) {
                var clientes = [];

                for (i = 0; i < dados.rows.length; i++) {
                    var dado = dados.rows.item(i);
                    clientes.push({
                        label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf,
                        codigo: dado.codigo,
                        loja: dado.loja,
                        tabela_preco: dado.tabela_preco,
                        desconto: dado.desconto,
                        condicao_pagamento: dado.condicao_pagamento,
                        total_titulos_vencidos: dado.total_titulos_vencidos
                        
                    });
                }

                buscar_clientes(clientes);
                
                // Selecionar campo se existir na sessão
                if (obter_valor_sessao('codigo_cliente')) {
                    $('input[name=cliente]').val(obter_valor_sessao('descricao_cliente'));
                    $('input[name=codigo_cliente]').val(obter_valor_sessao('codigo_cliente'));
                    $('input[name=loja_cliente]').val(obter_valor_sessao('loja_cliente'));

                    exibir_informacoes_cliente(obter_valor_sessao('codigo_cliente'), obter_valor_sessao('loja_cliente'));

                    // Bloquear campo quando selecionar cliente
                    $('input[name=cliente]').attr('readonly', 'readonly');
                    $('#trocar_cliente').show();
                }
            }
        });
    });
}

/**
 * Metódo:		obter_condicoes_pagamento
 * 
 * Descrição:	Função Utilizada para retornar Condições de pagamento
 * 
 * Data:			09/10/2013
 * Modificação:	09/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 **/
function obter_condicoes_pagamento() {
    db.transaction(function(x) {
        doQuery(x,
            'SELECT * FROM condicoes_pagamento ', [],
            function(x, dados) {

                var total = dados.rows.length;
                
                $('select[name=condicao_pagamento]').append('<option value="">Selecione...</option>');

                for (i = 0; i < total; i++) {
                    var dado = dados.rows.item(i);
                    $('select[name=condicao_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
                }
                
                // Selecionar campo se existir na sessão
                if (obter_valor_sessao('condicao_pagamento')) {
                    $('select[name="condicao_pagamento"] option[value="' + obter_valor_sessao('condicao_pagamento') + '"]').attr('selected', 'selected');
                    $('select[name="condicao_pagamento"]').change();
                }
            }
        );
    });
}

/**
 * Metódo:		obter_descricao_pedido
 * 
 * Descrição:	Função Utilizada para obter a descrição do pedido
 * 
 * Data:			13/11/2013
 * Modificação:	13/11/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_descricao_pedido(tipo) {
    if (sessionStorage['sessao_tipo'] == 'sessao_orcamento') {
        var descricao_tipo_pedido = 'Orçamento';
    } else {
        var descricao_tipo_pedido = 'Pedido';
    }

    if (tipo == 'upper') {
        return descricao_tipo_pedido.toUpperCase();
    } else if (tipo == 'lower') {
        return descricao_tipo_pedido.toLowerCase();
    } else {
        return descricao_tipo_pedido;
    }
}

/**
 * Metódo:		obter_filiais
 * 
 * Descrição:	Função Utilizada para retornar as filiais
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_filiais() {
    db.transaction(function(x) {
        doQuery(x,
            'SELECT * FROM filiais', [],
            function(x, dados) {

                var total = dados.rows.length;

                $('select[name=filial]').append('<option value="">Selecione...</option>');

                //Machs possui apenas uma filial.
                //for (i = 0; i < total; i++) {
                    var dado = dados.rows.item(0);
                    $('select[name=filial]').append('<option value="' + dado.codigo + '" selected>' + dado.codigo + ' - ' + dado.razao_social + '</option>');
                //}

                // Selecionar campo se existir na sessão
                if (obter_valor_sessao('filial')) {
                    $('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
                }

            }
        );
    });
}

/**
 * Metódo:		obter_formas_pagamento
 * 
 * Descrição:	Função Utilizada para retornar Formas de Pagamento
 * 
 * Data:			25/05/2015
 * Modificação:	25/05/2015
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_formas_pagamento() {
    db.transaction(function(x) {
        doQuery(x, 'SELECT * FROM formas_pagamento', [], function(x, dados) {

            var total = dados.rows.length;

            $('select[name=forma_pagamento]').append('<option value="">Selecione...</option>');

            for (i = 0; i < total; i++) {
                var dado = dados.rows.item(i);
                $('select[name=forma_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
            }

            // Selecionar campo se existir na sessão
            if (obter_valor_sessao('forma_pagamento')) {
                $('select[name="forma_pagamento"] option[value="' + obter_valor_sessao('forma_pagamento') + '"]').attr('selected', 'selected');
                $('select[name="forma_pagamento"]').change();
            }
        });
    });
}

/**
 * Metódo:		obter_numero_parcelas
 * 
 * Descrição:	Função Utilizada para retornar o número de parcelas de uma determinada
 * 				condição de pagamento.
 * 
 * Data:			25/05/2015
 * Modificação:	25/05/2015
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_numero_parcelas(data_entrega, data_entrega_sessao,  codigo_condicao_pagamento, condicao_pagamento_sessao) {
    var where = " WHERE codigo = '" + codigo_condicao_pagamento + "'";
    db.transaction(function(x) {
        doQuery(x, 'SELECT quantidade_parcelas FROM condicoes_pagamento' + where, [], function(x, dados) {
        	var total = dados.rows.length;
        	if(total > 0) {
        		var numero_parcelas = dados.rows.item(0).quantidade_parcelas;
                if (numero_parcelas > 0) {
                	$('input[name="numero_parcelas"]').val(numero_parcelas);
                    if (obter_valor_sessao('editar') && (!obter_valor_sessao('parcelas_edicao'))) {
                    	gerar_parcelas_pedido_edicao(obter_valor_sessao('id_pedido'));
                    }
                    
                    
                    
                   console.log(data_entrega + ' - ' + data_entrega_sessao ); 
                    if ((!obter_valor_sessao('parcelas')) || (condicao_pagamento_sessao != codigo_condicao_pagamento) || obter_valor_sessao('numero_parcelas') != numero_parcelas || data_entrega  != data_entrega_sessao ) {
                    	gerarParcelas(data_entrega, numero_parcelas);
        	        }
                }
        	}
        });
    });
}

/**
 * Metódo:		obter_prazo_medio
 * 
 * Descrição:	Função utilizada para retornar o prazo médio da condição de pagamento.
 * 
 * Data:	    25/05/2015
 * Modificação:	25/05/2015
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_prazo_medio(codigo_condicao_pagamento, data_entrega) {
    var where = " WHERE codigo = '" + codigo_condicao_pagamento + "'";
    db.transaction(function(x) {
        doQuery(x, 'SELECT prazo_medio FROM condicoes_pagamento' + where, [], function(x, dados) {
        	var total = dados.rows.length;
        	if(total > 0) {
	            salvar_sessao('prazo_medio', dados.rows.item(0).prazo_medio);
	            calcularAcrescimoCondicaoPagamento(data_entrega);
        	}
        });
    });
}

/**
 * Metódo:		obter_prospects
 * 
 * Descrição:	Função Utilizada para retornar Todos os Prospects
 * 
 * Data:			13/11/2013
 * Modificação:	13/11/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_prospects(filial) {
    var wheres = "WHERE status = 'A' OR status IS NULL";

    if (info.empresa) {
        wheres += " AND empresa = '" + info.empresa + "' ";
    }

    if (filial) {
        wheres += " AND (filial = '" + filial + "' OR filial = '')";
    }

   

    db.transaction(function(x) {
        doQuery(x, 'SELECT * FROM prospects ' + wheres, [], function(x, dados) {
            if (dados.rows.length) {
                var prospects = [];

                for (i = 0; i < dados.rows.length; i++) {
                    var dado = dados.rows.item(i);

                    prospects.push({
                        label: dado.codigo + '/' + dado.codigo_loja + ' - ' + dado.nome + ' - ' + dado.cgc,
                        codigo: dado.codigo,
                        loja: dado.codigo_loja
                    });
                }

                buscar_prospects(prospects);

                // Selecionar campo se existir na sessão
                if (obter_valor_sessao('codigo_prospect')) {
                    $('input[name=prospect]').val(obter_valor_sessao('descricao_prospect'));
                    $('input[name=codigo_prospect]').val(obter_valor_sessao('codigo_prospect'));
                    $('input[name=loja_prospect]').val(obter_valor_sessao('loja_prospect'));

                    exibir_informacoes_prospect(obter_valor_sessao('codigo_prospect'), obter_valor_sessao('loja_prospect'));

                    // Bloquear campo quando selecionar cliente
                    $('input[name=prospect]').attr('readonly', 'readonly');
                    $('#trocar_prospect').show();

                    //----------------
                    // Selecionar a opção prospect se existir na sessao
                    $('select[name="cliente_prospect"] option[value="P"]').attr('selected', 'selected');
                    $('#campo_cliente').hide();
                    $('#campo_prospect').show();

                    if (obter_valor_sessao('tabela_precos')) {
                        $('select[name="tabela_precos"]').removeAttr('disabled');
                    }
                }
            }
        });
    });

    //--------------
    // Selecionar campo se existir na sessão
    if (obter_valor_sessao('codigo_prospect')) {
        $('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
    }
}

/**
 * Metódo:		obter_tabela_precos
 * 
 * Descrição:	Função Utilizada para retornar Tabelas Preços
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_tabela_precos() {
	var tabela_preco_pedido = obter_valor_sessao('tabela_precos');
	
	
	$('select[name=tabela_precos]').html('<option value="">Selecione...</option>');
	
	var tabela_preco_sugerida_cliente  = obter_valor_sessao('tabela_preco_sugerida');
	
	
    var where = " WHERE " +
    "( 		representante_tabela_preco.tabela_preco_codigo = tabelas_preco.codigo  	AND representante_tabela_preco.tabela_preco_empresa = tabelas_preco.empresa) " + 
	"OR " +
    "( tabelas_preco.codigo = ?  AND clientes.empresa = tabelas_preco.empresa	) " +
    		" AND tabelas_preco.codigo > 0 ";

    if (info.empresa) {
        where += " AND tabelas_preco.empresa = '" + info.empresa + "' ";
    }

    //DATA INICIO DE VIGENCIA DA TABELA DE PREÇO
    where += " AND (vigencia_inicio <= '" + date('Ymd') + "' OR vigencia_inicio = '0' ) ";

    //DATA FINAL DE VIGENCIA DA TABELA DE PREÇO
    where += " AND (vigencia_final >= '" + date('Ymd') + "' OR vigencia_final = '0')";

    db.transaction(function(x) {
        doQuery(x,
            'SELECT DISTINCT tabelas_preco.* FROM tabelas_preco, representante_tabela_preco, clientes  ' + where, [tabela_preco_sugerida_cliente],
            function(x, dados) {
                var total = dados.rows.length;
                
                tabelas_precos_sistema = [];
                for (i = 0; i < total; i++) {
                    var dado = dados.rows.item(i);
                    tabelas_precos_sistema.push(dado);
                    $('select[name=tabela_precos]').append('<option value="' + dado.codigo + '" data-condicao_pagamento="' + dado.condicao_pagamento + '">' +  dado.codigo +  ' - ' + dado.descricao + '</option>');
                }
                if(tabela_preco_pedido){
                	rotina_cliente(tabela_preco_pedido);
                }else{
                	rotina_cliente(tabela_preco_sugerida_cliente);
                }
                
                
                rotina_tabelas_preco();

            }
        );
    });
}

/**
 * Metódo:		obter_tipo_pedido
 * 
 * Descrição:	Função Utilizada para retornar Tipos de pedido
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_tipo_pedido() {

    $('select[name=tipo_pedido]').append('<option value="">Selecione...</option>');

    if (sessionStorage['sessao_tipo'] == 'sessao_orcamento') {
        $('select[name=tipo_pedido]').append('<option value="*">Orçamento</option>');
    } else {
        $('select[name=tipo_pedido]').append('<option value="N" ' + (obter_valor_sessao('tipo_pedido') == 'N' ? 'selected' : '') + '>Venda Normal</option>');
        $('select[name=tipo_pedido]').append('<option value="B" ' + (obter_valor_sessao('tipo_pedido') == 'O' ? 'selected' : '') + '>Bonificação</option>');
    }

    // Selecionar campo se existir na sessão
    if (obter_valor_sessao('tipo_pedido')) {
        $('select[name="tipo_pedido"] option[value="' + obter_valor_sessao('tipo_pedido') + '"]').attr('selected', 'selected');
    }
}

/**
 * Metódo:		obter_ultimo_pedido_cliente
 * 
 * Descrição:	Função Utilizada para pegar o ultimo pedido do cliente e exibir os valores nas informações do cliente
 * 
 * Data:			10/10/2013
 * Modificação:	10/10/2013
 * 
 * @access		public
 * @param		String 			var codigo		- Codigo do Cliente
 * @param		String 			var loja		- Loja do Cliente
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_ultimo_pedido_cliente(codigo, loja) {
    db.transaction(function(x) {
        doQuery(x, 'SELECT SUM(ip_valor_total_item) AS valor_pedido, SUM(ip_total_desconto_item) AS valor_desconto, * FROM pedidos_processados WHERE cliente_codigo = ? AND cliente_loja = ? GROUP BY pedido_codigo ORDER BY pedido_data_emissao DESC, pedido_codigo DESC', [codigo, loja], function(x, dados) {
            if (dados.rows.length) {
                $('#conteudo_ultimo_pedido ul').show();
                $('#conteudo_ultimo_pedido div').hide();

                var dado = dados.rows.item(0);

                $('.info_pedido_data_emissao').html(protheus_data2data_normal(dado.pedido_data_emissao));
                $('.info_pedido_codigo').html(dado.pedido_codigo);
                $('.info_pedido_codigo').attr('href', 'pedidos_espelho.html#' + dado.pedido_codigo + '|' + dado.pedido_filial);
                $('.info_condicao_pagamento').html(dado.condicoes_pagamento_codigo + ' - ' + dado.condicoes_pagamento_descricao);
                $('.info_forma_pagamento').html(dado.forma_pagamento_codigo + ' - ' + dado.forma_pagamento_descricao);
                $('.info_valor_pedido').html('R$ ' + number_format(dado.valor_pedido, 3, ',', '.'));
                $('.info_valor_desconto').html('R$ ' + number_format(dado.valor_desconto, 3, ',', '.'));
            } else {
                $('#conteudo_ultimo_pedido ul').hide();
                $('#conteudo_ultimo_pedido div').show();
            }
        });
    });
}

/**
 * Metódo:		obter_valor_sessao
 * 
 * Descrição:	Função Utilizada para retornar o valor de campo da sessão
 * 
 * Data:			11/10/2013
 * Modificação:	11/10/2013
 * 
 * @access		public
 * @param		String 			var campo		- Nome do campo que será utilizado para retornar o valor na sessao
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_valor_sessao(campo) {
    if (sessionStorage[sessionStorage['sessao_tipo']]) {
        var sessao = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
    }
    
    
  //  console.log(JSON.stringify(sessao));
    if (sessao) {
        if (sessao[campo]) {
            return sessao[campo];
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//---------------------------------------------------------------------------
//----------  ----------  Rotinas (Regras) da Incusão do Pedido ----------  ----------
//---------------------------------------------------------------------------

/**
 * Metódo:		remover_disabled
 * 
 * Descrição:	Função Utilizada para habilitar os botoes que estao bloqueados
 * 
 * Data:			09/10/2013
 * Modificação:	09/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function remover_disabled() {
    if (sessionStorage[sessionStorage['sessao_tipo']]) {
        var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);

        // Se existir Filial na sessao, habilitar TIPO DE PEDIDO
        if (sessao_pedido.filial) {
            //$('select[name=tipo_pedido]').removeAttr('disabled');
        	$('input[name=cliente]').removeAttr('disabled');
        }
        
        // Se existir Tipo de Pedido na sessao, habilitar CLIENTE
        if (sessao_pedido.tipo_pedido && !sessao_pedido.codigo_cliente) {
            $('input[name=cliente]').removeAttr('disabled');
        }

        // Se existir Tipo de Pedido na sessao, habilitar Propect
        if (sessao_pedido.tipo_pedido && !sessao_pedido.codigo_prospect) {
            $('input[name=prospect]').removeAttr('disabled');
        }

        // Se existir Cliente na sessao, habilitar TABELA de PREÇOS
        if (sessao_pedido.codigo_cliente) {
            $('select[name=tabela_precos]').removeAttr('disabled');
            $('select[name=condicao_pagamento]').removeAttr('disabled');
            $('select[name=forma_pagamento]').removeAttr('disabled');
        }

        //-------------------------------------------------------------------------------------------------------------
        //------------ Desativar botao "Adicionar Produtos" se uma opção nao for selecionada --------------------------
        //-------------------------------------------------------------------------------------------------------------

        if (!sessao_pedido.filial) {
            $('#id_adicionar_produtos').addClass('tab_desativada');
        }

        if (!sessao_pedido.tipo_pedido) {
            $('#id_adicionar_produtos').addClass('tab_desativada');
        }

        //------------------------------------------
        if (!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido') {
            $('#id_adicionar_produtos').addClass('tab_desativada');
        } else if (!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null)) {
            $('#id_adicionar_produtos').addClass('tab_desativada');
        } else if (!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P') {
            $('#id_adicionar_produtos').addClass('tab_desativada');
        }
        //------------------------------------------

        if (!sessao_pedido.tabela_precos) {
            $('#id_adicionar_produtos').addClass('tab_desativada');
        }

        if (!sessao_pedido.condicao_pagamento) {
            $('#id_adicionar_produtos').addClass('tab_desativada');
        }

        ativar_tabs();
    }
}

/**
 * Metódo:		rotina_cliente
 * 
 * Descrição:	Função Utilizada para selecionar a tabela de preços do cliente (Se existir, se não seleciona a opção vazia "Selecione..." e bloqueia o campo e condicao_pagamento)
 * 
 * Data:			11/10/2013
 * Modificação:	11/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function rotina_cliente(tabela) {
    if (tabela) {
        $('select[name="tabela_precos"] option[value="' + tabela + '"]').attr('selected', 'selected');
        $('select[name="tabela_precos"]').change();

    } else {
        $('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
        $('select[name="tabela_precos"]').change();
        $('select[name="tabela_precos"]').attr('disabled', 'disabled');

        $('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
        $('select[name="condicao_pagamento"]').change();
        $('select[name="condicao_pagamento"]').attr('disabled', 'disabled');

        $('select[name="forma_pagamento"] option[value=""]').attr('selected', 'selected');
        $('select[name="forma_pagamento"]').change();
        $('select[name="forma_pagamento"]').attr('disabled', 'disabled');
    }
    
    
    console.log('concluida rotina do cliente');
}

/**
 * Metódo:		rotina_tabelas_preco
 * 
 * Descrição:	Função Utilizada buscar a condição de pagamento da tabela de preços e depois selecionar a condição de pagamento e bloquea-la (Se não existir condição de pagamento na tabela de preços, o campo de condição de pagamento fica livre para seleção)
 * 
 * Data:			11/10/2013
 * Modificação:	11/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function rotina_tabelas_preco() {
	console.log('Tabela de preços: '+obter_valor_sessao('tabela_precos'));
	
	
    $('select[name=tabela_precos]').change(function() {

        // Obter condição de pagamento pela tabela de preços
        var codigo_tabela_precos = $(this).val();
        var codigo_condicao_pagamento = trim($(this).children(":selected").data('condicao_pagamento'));

        if (!!!codigo_condicao_pagamento) {
            if (obter_valor_sessao('condicao_pagamento')) {
                $('select[name="condicao_pagamento"] option[value="' + obter_valor_sessao('condicao_pagamento') + '"]').attr('selected', 'selected');
            }
        }
        
        for(var i in tabelas_precos_sistema) {
        	if (tabelas_precos_sistema[i].codigo == codigo_tabela_precos) {
        		if (tabelas_precos_sistema[i].irregular == 'S') {
        			mensagem('Tabela de Preços Irregular. <br/>  Pedido sujeito a aprovação.');
        		}
        	}
        }
        

        // Se existir condição de pagamento selecionar e bloquear campo
        if (!!!codigo_condicao_pagamento) {
        	
            $('select[name="condicao_pagamento"] option[value="' + codigo_condicao_pagamento + '"]').attr('selected', 'selected');
            $('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
                        
            salvar_sessao('condicao_pagamento', codigo_condicao_pagamento);

            $('#aviso_condicao_pagamento').html('A condição de pagamento ' + codigo_condicao_pagamento + ' é fixa para a tabela de preços ' + codigo_tabela_precos + '.');
        } else {
            $('select[name="condicao_pagamento"]').removeAttr('disabled');
            $('#aviso_condicao_pagamento').html('');
        }

        // chamando regra de desconto para salvar na sessao
        //regra_desconto();

    });

    // Selecionar campo se existir na sessão
    if (obter_valor_sessao('tabela_precos')) {
        $('select[name="tabela_precos"] option[value="' + obter_valor_sessao('tabela_precos') + '"]').attr('selected', 'selected');
        $('select[name="tabela_precos"]').change();
    }
    
    console.log('concluida rotina de tabela de preços');

}

/**
* Metódo:		regra_desconto
* 
* Descrição:	Função Utilizada para salvar o valor da regra de desconto na sessao ( A regra padrão e pela tabela de preços)
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 

function regra_desconto()
{
	db.transaction(function(x) {	
		doQuery(x, 
			'SELECT percentual_desconto FROM regra_desconto WHERE tabela_preco = ?', [obter_valor_sessao('tabela_precos')], function(x, dados) {

				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);

					salvar_sessao('regra_desconto', dado.percentual_desconto);
				}
				else
				{
					salvar_sessao('regra_desconto', 0);
				}
			}
		);
	});
}
*/

/**
 * Metódo:		salvar_dados_cliente
 * 
 * Descrição:	Função Utilizada para salvar dados do cliente
 * 
 * Data:			07/11/2013
 * Modificação:	07/11/2013
 * 
 * @access		public
 * @param		String 			var codigo		- Codigo do Cliente
 * @param		String 			var loja		- Loja do cliente
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_dados_cliente(codigo, loja) {
    if (codigo && loja) {
        db.transaction(function(x) {
            doQuery(x,
                'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja],
                function(x, dados) {

                    if (dados.rows.length) {
                        var dado = dados.rows.item(0);
                        salvar_sessao('dados_cliente', dado);
                    }
                }
            );
        });
    }
}

/**
 * Metódo:		salvar_dados_condicao_pagamento
 * 
 * Descrição:	Função Utilizada para salvar dados da condição de pagamento
 * 
 * Data:			07/11/2013
 * Modificação:	07/11/2013
 * 
 * @access		public
 * @param		String 			var codigo		- Codigo condição de pagamento
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_dados_condicao_pagamento(codigo) {
    if (codigo) {
        db.transaction(function(x) {
            doQuery(x,
                'SELECT * FROM condicoes_pagamento WHERE codigo = ?', [codigo],
                function(x, dados) {

                    if (dados.rows.length) {
                        var dado = dados.rows.item(0);

                        salvar_sessao('dados_condicao_pagamento', dado);
                    }
                }
            );
        });
    }
}

/**
 * Metódo:		salvar_dados_forma_pagamento
 * 
 * Descrição:	Função Utilizada para salvar dados da forma de pagamento
 * 
 * Data:			07/11/2013
 * Modificação:	07/11/2013
 * 
 * @access		public
 * @param		String 			var codigo		- Codigo forma de pagamento
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_dados_forma_pagamento(codigo) {
    if (codigo) {
        db.transaction(function(x) {
            doQuery(x,
                'SELECT * FROM formas_pagamento WHERE codigo = ?', [codigo],
                function(x, dados) {
                    if (dados.rows.length) {
                        var dado = dados.rows.item(0);
                        salvar_sessao('dados_forma_pagamento', dado);
                    }
                }
            );
        });
    }
}

/**
 * Metódo:		salvar_dados_prospect
 * 
 * Descrição:	Função Utilizada para salvar dados do prospect
 * 
 * Data:			16/11/2013
 * Modificação:	16/11/2013
 * 
 * @access		public
 * @param		String 			var codigo		- Codigo do Prospect
 * @param		String 			var loja		- Loja do Prospect
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_dados_prospect(codigo, loja) {
    if (codigo && loja) {
        db.transaction(function(x) {
            doQuery(x,
                'SELECT * FROM prospects WHERE codigo = ? AND codigo_loja = ?', [codigo, loja],
                function(x, dados) {
                    if (dados.rows.length) {
                        var dado = dados.rows.item(0);
                        salvar_sessao('dados_prospect', dado);
                    }
                }
            );
        });
    }
}

/**
 * Metódo:		salvar_sessao
 * 
 * Descrição:	Função Utilizada para salvar dados do pedido na sessão
 * 
 * Data:			08/10/2013
 * Modificação:	08/10/2013
 * 
 * @access		public
 * @param		String 			var nome_campo		- Nome do campo que será utilizado na sessao
 * @param		String 			var valor			- Valor do campo que será utilizado na sessao
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_sessao(nome_campo, valor) {
    var sessao_pedido = [];

    // Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
    if (sessionStorage[sessionStorage['sessao_tipo']]) {
        sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
    }

    // Adicionar novo valor no array
    sessao_pedido[nome_campo] = valor;

    // Gravar dados do Array na sessão
    sessionStorage[sessionStorage['sessao_tipo']] = serialize(sessao_pedido);

    remover_disabled();
}