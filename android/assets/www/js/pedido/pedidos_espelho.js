$(document).ready(function() {
	
	var codigo_e_empresa 	= parse_url(location.href).fragment;
	array_codigo_e_empresa 	= codigo_e_empresa.split('|');
	var codigo_do_pedido 	= array_codigo_e_empresa[0];
	var codigo_da_empresa 	= array_codigo_e_empresa[1];
	var tipo_pedido 		= array_codigo_e_empresa[2];
	
	//Definindo se é um Orçamento ou Pedido
	if(tipo_pedido == 'O')
	{
		var tabela = 'orcamentos';
		var descricao = 'orçamento';
		$('.descricao_pedido').html('Orçamento');
	}
	else
	{
		var tabela = 'pedidos_pendentes';
		var descricao = 'pedido';
	}
	
	//Setar os botões
	//$('.caixa ul li a').button();
	
	$('#enviar_pedido').click(function(e) {
			e.preventDefault();
	
			confirmar('Deseja enviar este ' + descricao + '?', 
			function () {
				$(this).dialog('close');
		
				location.href = "sincronizar.html?codigo_pedido=" + codigo_do_pedido;
				
				$("#confirmar_dialog").remove();
			});
	});
	
	// Função utilizada para verificar se o cliente pertence ao representante
	function verificar_cliente_representante(cpfOuCnpj, callback)
	{
		// Obter Pedido Pendentes
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM clientes WHERE cpf = ?', [cpfOuCnpj], 
				function(x, dados) {
					if (dados.rows.length)
					{						
						callback();
					}
					else
					{						
						mensagem('O Pedido não pode ser copiado porque o cliente (' + formatar_cpfCNPJ(cpfOuCnpj) + ') não pertence ao representante.');
					}
				}
			)
		});
	}

	// Transformar orçemaneto em pedido 
	$('#transformar_em_pedido').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja transformar este orçamento em pedido?', 
				function () {
					$(this).dialog('close');
					
					//------------------
					//---------------------------------
					
					
					copiar_pedido(codigo_do_pedido, codigo_da_empresa, false, tabela, true);
					
					//---------------------------------
					//------------------
					
					$("#confirmar_dialog").remove();
					
				}
			);
	});
	
	// Copiar pedido
	$('#copiar').click(function(e) {
		e.preventDefault();
		
		var copiar_pedido_orcamento = function()
		{
			confirmar('Deseja copiar este ' + descricao + '?', 
				function () {
					$(this).dialog('close');
					
					//------------------
					//---------------------------------
			
					if(!tipo_pedido)
					{
						
						
						copiar_pedido_processado(codigo_do_pedido, codigo_da_empresa);
					}
					else
					{
						
						copiar_pedido(codigo_do_pedido, codigo_da_empresa, false, tabela);
					}
					
					//---------------------------------
					//------------------
					
					$("#confirmar_dialog").remove();
					
				}
			);
		}
		
		if(tipo_pedido == 'O')
		{
			copiar_pedido_orcamento();
		}else{
			verificar_cliente_representante($('#cpfOuCnpj').val(), copiar_pedido_orcamento);
		}
		
	});
	
	// Editar pedido
	$('#editar').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja editar este ' + descricao + '?', 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
		
				copiar_pedido(codigo_do_pedido, codigo_da_empresa, true, tabela);
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
				
			}
		);
		
	});
	
	// Editar pedido
	$('#excluir').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja excluir este ' + descricao + '?', 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
				db.transaction(function(x) {
					x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + codigo_do_pedido + '"', [], 
					function(){

						if(tipo_pedido == 'O')
						{
							// Apagando pedido da sessao
							location.href="orcamentos_aguardando.html";
						}
						else
						{
							// Apagando pedido da sessao
							location.href="pedidos_aguardando.html";
						}

					});
					x.executeSql('DELETE FROM pedido_parcela WHERE pedido_parcela_id_pedido = "' + codigo_do_pedido + '"', []);
				});
											
				$("#confirmar_dialog").remove();
				
			}
		);
		
	});

	
	// Se for "P" (Pedidos Pendentes) entra no IF, se não entra no IF dos Pedidos Processados
	if(tipo_pedido == 'P' || tipo_pedido == 'O')
	{
		
		
		// Obter Pedido Pendentes
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ? ORDER BY pedido_numero_item', [codigo_do_pedido, codigo_da_empresa], 
				function(x, dados) {
					if (dados.rows.length)
					{
						var pedido = dados.rows.item(0);
						
						pedido.pedido_codigo ? obterParcelas(pedido.pedido_codigo) : obterParcelas(pedido.pedido_id_pedido);
												
						if(!pedido.exportado)
						{
							$('#editar').show();
							$('#excluir').show();
							
							if(tipo_pedido == 'P')
							{
								$('#enviar_pedido').show();
							}
							
						}
						else
						{
							if(tipo_pedido == 'O')
							{
								if(!isEmpty(pedido.cliente_codigo))
								{
									$('#transformar_em_pedido').show();
								}
							}
						}
						
						
						if(pedido.pedido_status == 'B'){
							var mensagem_pedido_bloqueado = '<div class="bs-callout bs-callout-warning" id="callout-helper-pull-navbar">';
							mensagem_pedido_bloqueado += '    <h4>Motivo Bloqueio</h4>';
							mensagem_pedido_bloqueado += '    	<p>'+pedido.pedido_motivo_bloqueio+'</p>';
							mensagem_pedido_bloqueado += '</div>';	
							
							$('#motivo_bloqueio').html(mensagem_pedido_bloqueado);
							$('#motivo_bloqueio').show();
							
						}
						
						if(pedido.pedido_status == 'R'){
							var mensagem_pedido_reprovacao = '<div class="bs-callout bs-callout-danger" id="callout-helper-pull-navbar">';
							mensagem_pedido_reprovacao += '    <h4>Motivo Reprovação</h4>';
							mensagem_pedido_reprovacao += '    	<p>'+pedido.pedido_motivo_reprovacao+'</p>';
							mensagem_pedido_reprovacao += '</div>';	
							
							$('#motivo_reprovacao').html(mensagem_pedido_reprovacao);
							$('#motivo_reprovacao').show();
							
						}
						
						
						// informações gerais
						$('.codigo_e_representante').text(info.cod_rep + ' / ' + info.nome_representante);
						$('.numero_pedido').text(pedido.pedido_id_pedido);
						$('.filial').text(pedido.pedido_filial);
						$('.data_emissao').text(protheus_data2data_normal(pedido.pedido_data_emissao));
						
						
						x.executeSql(
								'SELECT * FROM tabelas_preco WHERE codigo = ? ', [pedido.pedido_tabela_precos], 
								function(x, dados_tabela_precos) {
									if (dados_tabela_precos.rows.length)
									{
										var tabela_preco = dados_tabela_precos.rows.item(0);
										
										$('.tabela_preco').text(tabela_preco.codigo+' - '+ tabela_preco.descricao);
									}else{
										$('.tabela_preco').text(pedido.pedido_tabela_precos);
									}
								});
								
								
						$('.condicao_pagamento').text(pedido.condicoes_pagamento_codigo + ' - '  + pedido.condicoes_pagamento_descricao);
						
                        if(pedido.forma_pagamento_codigo && pedido.forma_pagamento_descricao) {
                        	$('.forma_pagamento').text(pedido.forma_pagamento_codigo + ' - ' + pedido.forma_pagamento_descricao);
                        } else {
                        	$('.forma_pagamento').text('N/A');
                        }
						
						// Descontos
						$('.pedido_desconto1').text(pedido.pedido_desconto1 ? number_format(pedido.pedido_desconto1, 3, ',', '.') : 'N/A');
						$('.pedido_desconto4').text(pedido.pedido_desconto4 ? number_format(pedido.pedido_desconto4, 3, ',', '.') : 'N/A');
						
						if(!isEmpty(pedido.cliente_codigo))
						{
						
							// Informações do Cliente
							$('.nome_cliente').text(pedido.cliente_nome);
							$('.codigo_loja_cliente').text(pedido.cliente_codigo + ' / ' + pedido.cliente_loja);
							$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.cliente_cpf));
							$('#cpfOuCnpj').val(pedido.cliente_cpf);
							$('.contato').text(pedido.cliente_pessoa_contato ? pedido.cliente_pessoa_contato : 'N/A');
							
							/*Tratamento telefone*/
							var telefone = '';
							if(pedido.cliente_ddd)
							{
								telefone = pedido.cliente_ddd + pedido.cliente_telefone;
							}
							else
							{
								if(pedido.cliente_telefone)
								{
									telefone = pedido.cliente_telefone;
								}						
							}
							/*Tratamento telefone*/
							$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
							
							
							$('.email').text(pedido.cliente_email ? pedido.cliente_email : 'N/A');
							$('.cidade').text(pedido.cliente_cidade ? pedido.cliente_cidade : 'N/A');
							$('.estado').text(pedido.cliente_estado ? pedido.cliente_estado : 'N/A');
							$('.endereco').text(pedido.cliente_endereco ? pedido.cliente_endereco : 'N/A');
						}
						else
						{
							// Informações do Prospect
							
							$('.descricao_cliente').html('Prospect');
							
							$('.nome_cliente').text(pedido.prospect_nome);
							$('.codigo_loja_cliente').text(pedido.prospect_codigo + ' / ' + pedido.prospect_codigo_loja);
							$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.prospect_cgc));
							$('#cpfOuCnpj').val(pedido.prospect_cgc);
							$('.contato').text(pedido.prospect_contato ? pedido.prospect_contato : 'N/A');
							
							/*Tratamento telefone*/
							var telefone = '';
							if(pedido.cliente_ddd)
							{
								telefone = pedido.prospect_ddd + pedido.prospect_telefone;
							}
							else
							{
								if(pedido.prospect_telefone)
								{
									telefone = pedido.prospect_telefone;
								}						
							}
							$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
							/*Tratamento telefone*/							
							
							$('.email').text(pedido.prospect_email ? pedido.prospect_email : 'N/A');
							
							$('.estado').text(pedido.prospect_estado ? pedido.prospect_estado : 'N/A');
							$('.endereco').text(pedido.prospect_endereco ? pedido.prospect_endereco : 'N/A');
							
							
							// -- Obter Municipio
							x.executeSql(
							'SELECT * FROM municipios WHERE codigo = ? AND uf = ?', [pedido.prospect_codigo_municipio, pedido.prospect_estado], 
							function(x, dados) {
								if (dados.rows.length)
								{
									var municipio = dados.rows.item(0);
									$('.cidade').text(municipio.nome);
								}
								else
								{
									$('.cidade').text('N/A');
								}
							});
						}
					
						// Informações do Pedido
						$('.pedido_cliente').text(pedido.pedido_pedido_cliente ? pedido.pedido_pedido_cliente : 'N/A');
						$('.data_entrega').text(pedido.pedido_data_entrega ? protheus_data2data_normal(pedido.pedido_data_entrega) : 'N/A');
						
						// -- obter evento
						x.executeSql(
						'SELECT * FROM eventos WHERE id = ?', [pedido.pedido_id_feira], 
						function(x, dados) {
							if (dados.rows.length)
							{
								var evento = dados.rows.item(0);
								$('.eventos').text(evento.id + ' - ' + evento.nome);
							}
						});
						
						$('.tipo_frete').text(obter_tipo_frete(pedido.pedido_tipo_frete));
						$('.transportadoras').text(pedido.transportadora_nome ? pedido.transportadora_nome : 'N/A');
						
						// Cliente de Entrega
						if(pedido.pedido_cliente_entrega && pedido.pedido_loja_entrega)
						{
							x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [pedido.pedido_cliente_entrega, pedido.pedido_loja_entrega], function(x, dados) {
								if(dados.rows.length)
								{
									var cliente_entrega = dados.rows.item(0);
									
									$('.cliente_entrega').empty();
									$('.cliente_entrega').append('<b>Cliente:</b> ' + cliente_entrega.nome + '<br />');
									$('.cliente_entrega').append('<b>Endereço:</b> ' + cliente_entrega.endereco + '<br />');
									$('.cliente_entrega').append('<b>Bairro:</b> ' + cliente_entrega.bairro + '<br />');
									$('.cliente_entrega').append('<b>CEP:</b> ' + formatar_cep(cliente_entrega.cep) + '<br />');
									$('.cliente_entrega').append('<b>Cidade:</b> ' + cliente_entrega.cidade + '<br />');
									$('.cliente_entrega').append('<b>Estado:</b> ' + cliente_entrega.estado + '<br />');
								}
								else
								{
									$('.cliente_entrega').text('Cliente de Entrega não foi encontrado.');
								}
							});
						}
						
						$('.mensagem_nota_fiscal').text(pedido.pedido_mensagem_nota ? wordwrap(pedido.pedido_mensagem_nota, 35, " ", 1 ) : 'N/A');
						$('.observacao_comercial').text(pedido.pedido_observacao_comercial ? wordwrap(pedido.pedido_observacao_comercial, 35, " ", 1 ) : 'N/A');
						
						var acrescimo_condicao_pagamento = pedido.pedido_acrescimo_condicao_pagamento ? pedido.pedido_acrescimo_condicao_pagamento : parseFloat(0);
							
						//--------
						var total_itens = dados.rows.length;
						
						if(total_itens > 0)
						{
							// Declarando variaveis totais
							var total_preco_unitario		= 0;
							var total_modificador 			= 0;
							var total_desconto_reais		= 0;
							var total_preco_venda 			= 0;
							var total_quantidade 			= 0;
							var total			 			= 0;
							var total_ipi					= 0;
							var total_icms 					= 0;
							var total_st 					= 0;
							var total_com_ipi		 		= 0;
							var total_geral		 			= 0;
							
							for(i = 0; i < total_itens; i ++)
							{
								var item_pedido = dados.rows.item(i);
								
								var preco_unitario = item_pedido.pedido_preco_unitario;
								if(pedido.pedido_desconto1 > 0)
								{
									preco_unitario = aplicar_descontos_cabecalho(item_pedido.pedido_preco_unitario, pedido.pedido_desconto1, '0');
								}
								
								var itens_pedido 		= [];
								var valor_ipi 			= item_pedido.ip_valor_total_item * (item_pedido.produto_ipi / 100);
								var valor_com_desconto 	= item_pedido.ip_valor_total_item - item_pedido.ip_total_desconto_item;
								
								var percentual_modificador = parseFloat(item_pedido.pedido_percentual_modificador);
								var preco_venda_final = parseFloat(item_pedido.pedido_preco_venda);

								var html = '';
									html += '<td align="center">' +  item_pedido.pedido_numero_item  + '</td>';
									html += '<td align="center">' + item_pedido.pedido_codigo_produto + '</td>';
									html += '<td>' + item_pedido.produto_descricao + '</td>';
									html += '<td align="right">' + number_format(preco_unitario, 3, ',', '.') + '</td>';
									html += '<td align="right">';
									
									if(item_pedido.pedido_modificador == 'A') {					
										html += '+';										
									} else if(item_pedido.pedido_modificador == 'D') {
										html += '-';					
									} 
									
									//preco_venda_final += parseFloat((acrescimo_condicao_pagamento * item_pedido.pedido_preco_venda) / 100); 
									
									
									
									
									
									html += number_format(percentual_modificador, 3, ',', '.') + ' %<br />(R$ ';
									
									calcularValorModificador(preco_unitario, percentual_modificador, item_pedido.pedido_modificador, function(valorModificador) {
										html += number_format(valorModificador, 3, ',', '.');
										total_desconto_reais	+=	parseFloat(valorModificador);
									});
									
									html += ')</td>';									
									html += '<td align="right">' + number_format(preco_venda_final, 3, ',', '.') + '</td>';
									html += '<td align="right">' + item_pedido.pedido_quantidade + '</td>';
									
									//Total sem impostos
									html += '<td align="right">' + number_format(preco_venda_final * item_pedido.pedido_quantidade, 3, ',', '.')  + '</td>';
									
									//Impostos
									html += '<td align="right">' + (item_pedido.pedido_ipi ? number_format(item_pedido.pedido_ipi, 3, ',', '.') : '0,000') + '</td>';
									html += '<td align="right">' + (item_pedido.pedido_icms ? number_format(item_pedido.pedido_icms, 3, ',', '.') : '0,000') + '</td>';
									html += '<td align="right">' + (item_pedido.pedido_st ? number_format(item_pedido.pedido_st, 3, ',', '.') : '0,000') + '</td>';			
									
									//Total com Impostos
									var total_item_impostos = (parseFloat(preco_venda_final) * parseFloat(item_pedido.pedido_quantidade)) + (parseFloat(item_pedido.pedido_ipi) + parseFloat(item_pedido.pedido_st));
									html += '<td align="right">' + number_format(total_item_impostos, 3, ',', '.') +'</td>';	
									
								$('.itens_pendentes').parent('table').show();
								$('.itens_pendentes').append('<tr>' + html + '</tr>');
								
								// Somando Totais
								total_preco_unitario		+= parseFloat(preco_unitario) * parseFloat(item_pedido.pedido_quantidade);
								total_preco_venda 			+= parseFloat(preco_venda_final);
								total_quantidade			+= parseFloat(item_pedido.pedido_quantidade);
								total_ipi					+= parseFloat(item_pedido.pedido_ipi);
								total_icms					+= parseFloat(item_pedido.pedido_icms);
								total_st					+= parseFloat(item_pedido.pedido_st);
								total						+= parseFloat(preco_venda_final * item_pedido.pedido_quantidade);
								total_geral					+= parseFloat(preco_venda_final * item_pedido.pedido_quantidade) + (total_ipi + total_st);
								total_modificador 			+= percentual_modificador;
							}
							
							total_modificador =  (total * 100 / total_preco_unitario) -100 ;
							
							//Exibindo Totais
							var totais_html = '';
							totais_html += '<td colspan="3">TOTAIS DE PRODUTOS</td>';
							totais_html += '<td align="right">' + number_format(total_preco_unitario, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_modificador, 3, ',', '.') + ' %<br />R$ ' + number_format(total_desconto_reais, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + total_quantidade + '</td>';
							totais_html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_ipi, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_icms, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_st, 3, ',', '.') + '</td>';							
							totais_html += '<td align="right">' + number_format(total_geral, 3, ',', '.') + '</td>';

							$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
							
							/*
							if(pedido.pedido_acrescimo_condicao_pagamento > 0) {
								var acrescimo_condicao_pagamento = pedido.pedido_acrescimo_condicao_pagamento;
								acrescimo_condicao_pagamento = (acrescimo_condicao_pagamento * total_geral) / 100;
								
								var totais_html = '';
								totais_html += '<td colspan="11">Acréscimo Condição de Pagamento (';
								totais_html += number_format(pedido.pedido_acrescimo_condicao_pagamento, 3, ',', '.');
								totais_html += '%)</td>';
								totais_html += '<td align="right">' + number_format(acrescimo_condicao_pagamento, 3, ',', '.')  + '</td>';
								
								$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
								
								total_geral += acrescimo_condicao_pagamento;
								totais_html = '';
								totais_html += '<td colspan="11">TOTAIS DO ' + (tipo_pedido != 'O' ? 'PEDIDO' : 'ORÇAMENTO') + '</td>';
								totais_html += '<td align="right">' + number_format(total_geral, 3, ',', '.') + '</td>';
								
								$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
							}
							*/
						}
						
						alterarCabecalhoListagem('#itens_pendentes_tabela');
						alterarCabecalhoTabelaConteudo();
					}
				}
			);
		});
	}
	else
	{
	
		// Obter Pedido Processado
		db.transaction(function(x) {
			
			x.executeSql(
				'SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ? ORDER BY ip_codigo_item DESC', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
					if (dados.rows.length)
					{
						
						$('#atencao_impostos').hide();
						
						var pedido = dados.rows.item(0);
						
						pedido.pedido_codigo ? obterParcelas(pedido.pedido_codigo) : obterParcelas(pedido.pedido_id_pedido);
						
						// informações gerais
						$('.codigo_e_representante').text(info.cod_rep + ' / ' + info.nome_representante);
						$('.numero_pedido').text(pedido.pedido_codigo);
						$('.filial').text(pedido.pedido_filial);
						$('.data_emissao').text(protheus_data2data_normal(pedido.pedido_data_emissao));
                        
						x.executeSql(
								'SELECT * FROM tabelas_preco WHERE codigo = ? ', [pedido.ip_codigo_tabela_preco], 
								function(x, dados_tabela_precos) {
									if (dados_tabela_precos.rows.length)
									{
										var tabela_preco = dados_tabela_precos.rows.item(0);
										
										$('.tabela_preco').text(tabela_preco.codigo+' - '+ tabela_preco.descricao);
									}else{										
										if(!!pedido.ip_codigo_tabela_preco){
											$('.tabela_preco').text(pedido.ip_codigo_tabela_preco);
										}
									}
										
								});
						
						$('.condicao_pagamento').text(pedido.condicoes_pagamento_codigo + ' - ' + pedido.condicoes_pagamento_descricao);
                        
                        if(pedido.forma_pagamento_codigo && pedido.forma_pagamento_descricao) {
                        	$('.forma_pagamento').text(pedido.forma_pagamento_codigo + ' - ' + pedido.forma_pagamento_descricao);
                        } else {
                        	$('.forma_pagamento').text('N/A');
                        }
                        	
						// Informações do Cliente
						$('.nome_cliente').text(pedido.cliente_nome);
						$('.codigo_loja_cliente').text(pedido.cliente_codigo + ' / ' + pedido.cliente_loja);
						$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.cliente_cpf));
						$('#cpfOuCnpj').val(pedido.cliente_cpf);
						$('.contato').text(pedido.cliente_pessoa_contato ? pedido.cliente_pessoa_contato : 'N/A');
						
						// Descontos 
						$('.pedido_desconto1').text(pedido.pedido_desconto1 ? number_format(pedido.pedido_desconto1, 3, ',', '.') : 'N/A');
						$('.pedido_desconto4').text(pedido.pedido_desconto4 ? number_format(pedido.pedido_desconto4, 3, ',', '.') : 'N/A');
						
						/*Tratamento telefone*/
						var telefone = '';
						if(pedido.cliente_ddd)
						{
							telefone = pedido.cliente_ddd + pedido.cliente_telefone;
						}
						else
						{
							if(pedido.cliente_telefone)
							{
								telefone = pedido.cliente_telefone;
							}						
						}						
						$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
						/*Tratamento telefone*/		
						
						$('.email').text(pedido.cliente_email ? pedido.cliente_email : 'N/A');
						$('.cidade').text(pedido.cliente_cidade ? pedido.cliente_cidade : 'N/A');
						$('.estado').text(pedido.cliente_estado ? pedido.cliente_estado : 'N/A');
						$('.endereco').text(pedido.cliente_endereco ? pedido.cliente_endereco : 'N/A');
					
						// Informações do Pedido
						$('.pedido_cliente').text(pedido.ip_pedido_cliente ? pedido.ip_pedido_cliente : 'N/A');
						$('.data_entrega').text(pedido.pedido_data_entrega ? protheus_data2data_normal(pedido.pedido_data_entrega) : 'N/A');
						$('.eventos').text('N/A');
						$('.tipo_frete').text(obter_tipo_frete(pedido.pedido_tipo_frete));
						$('.transportadoras').text(pedido.transportadora_nome ? pedido.transportadora_nome : 'N/A');
						
						// --
						// Cliente de Entrega
						if(pedido.pedido_cliente_entrega && pedido.pedido_loja_entrega)
						{
							x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [pedido.pedido_cliente_entrega, pedido.pedido_loja_entrega], function(x, dados) {
								if(dados.rows.length)
								{
									var cliente_entrega = dados.rows.item(0);
									
									$('.cliente_entrega').empty();
									$('.cliente_entrega').append('<b>Cliente:</b> ' + cliente_entrega.nome + '<br />');
									$('.cliente_entrega').append('<b>Endereço:</b> ' + cliente_entrega.endereco + '<br />');
									$('.cliente_entrega').append('<b>Bairro:</b> ' + cliente_entrega.bairro + '<br />');
									$('.cliente_entrega').append('<b>CEP:</b> ' + formatar_cep(cliente_entrega.cep) + '<br />');
									$('.cliente_entrega').append('<b>Cidade:</b> ' + cliente_entrega.cidade + '<br />');
									$('.cliente_entrega').append('<b>Estado:</b> ' + cliente_entrega.estado + '<br />');
								}
								else
								{
									$('.cliente_entrega').text('Cliente de Entrega não foi encontrado.');
								}
							});
						}
						//--
						
						$('.mensagem_nota_fiscal').text(pedido.pedido_mensagem ? wordwrap(pedido.pedido_mensagem, 35, " ", 1 ) : 'N/A');
						
						var total_itens = dados.rows.length;
						
						if(total_itens > 0)
						{
							// Declarando variaveis totais
							var total_preco_venda 			= 0;
							var total_quantidade 			= 0;
							var total_quantidade_faturada 	= 0;
							var total			 			= 0;
							var total_faturado		 		= 0;
							var total_ipi   				= 0;
							var total_com_ipi		 		= 0;
							var total_desconto		 		= 0;
							var total_preco_com_desconto	= 0;
							var total_geral		 			= 0;
							
							var numero_item = 1;
							
							for(i = 0; i < total_itens; i ++)
							{
								var item_pedido = dados.rows.item(i);
								
								var preco_unitario = item_pedido.ip_preco_unitario;
								if(pedido.pedido_desconto1 > 0)
								{
									preco_unitario = aplicar_descontos_cabecalho(item_pedido.ip_preco_unitario, pedido.pedido_desconto1, '0');
								}
								
								var itens_pedido = [];
								var valor_ipi = parseFloat((item_pedido.inf_ipi ? item_pedido.inf_ipi : 0));
								var valor_com_desconto = item_pedido.ip_valor_total_item - item_pedido.ip_total_desconto_item;
								
								var html = '';
								html += '<td align="center">' + (numero_item++)  + '</td>';
								html += '<td align="center">' + item_pedido.ip_codigo_produto + '</td>';
								html += '<td>' + item_pedido.ip_descricao_produto + '</td>';
								html += '<td>' + (item_pedido.ip_pedido_cliente ? item_pedido.ip_pedido_cliente : 'N/A') + '</td>';
								html += '<td align="right">' + number_format(preco_unitario, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto, 3, ',', '.') + '</td>';
								html += '<td align="center">' + number_format(preco_unitario - (item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto), 3, ',', '.') + '</td>';
								html += '<td align="center">' + item_pedido.ip_quantidade_vendida_produto + '</td>';
								html += '<td align="right">' + number_format(item_pedido.ip_valor_total_item, 3, ',', '.') + '</td>';
								
								html += '<td align="center">' + item_pedido.ip_quantidade_faturada_produto + '</td>';
								
								html += '<td align="right">' + number_format(item_pedido.ip_preco_produto * item_pedido.ip_quantidade_faturada_produto, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(parseFloat(valor_ipi), 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(valor_ipi + parseFloat(item_pedido.ip_valor_total_item), 3, ',', '.') + '</td>';

								
								$('.itens_processados').parent('table').show();
								$('.itens_processados').append('<tr>' + html + '</tr>');
								
								// Somando Totais
								total_preco_venda 			+= parseFloat(preco_unitario);
								total_preco_com_desconto 	+= parseFloat(preco_unitario - (item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto));
								total_quantidade 			+= parseFloat(item_pedido.ip_quantidade_vendida_produto);
								total_quantidade_faturada	+= parseFloat(item_pedido.ip_quantidade_faturada_produto);
								total						+= parseFloat(item_pedido.ip_valor_total_item);
								total_faturado				+= parseFloat(item_pedido.ip_preco_produto * item_pedido.ip_quantidade_faturada_produto);
								total_ipi					+= parseFloat(valor_ipi);
								total_com_ipi				+= valor_ipi + parseFloat(item_pedido.ip_valor_total_item);
								total_desconto				+= parseFloat(item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto);
								total_geral					+= (valor_com_desconto * (item_pedido.produto_ipi / 100)) + parseFloat(valor_com_desconto);
							}
							
							//Exibindo Totais
							var totais_html = '';
							totais_html += '<td colspan="4">TOTAIS DO PEDIDO</td>';
							totais_html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_desconto, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_preco_com_desconto, 3, ',', '.') + '</td>';
							totais_html += '<td align="center">' + total_quantidade + '</td>';
							totais_html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
							totais_html += '<td align="center">' + total_quantidade_faturada + '</td>';
							
							totais_html += '<td align="right">' + number_format(total_faturado, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_ipi, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_com_ipi, 3, ',', '.') + '</td>';
							
							$('.itens_processados').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
							
							/*
							if(pedido.pedido_acrescimo_condicao_pagamento > 0) {
								var acrescimo_condicao_pagamento = pedido.pedido_acrescimo_condicao_pagamento;
								acrescimo_condicao_pagamento = (acrescimo_condicao_pagamento * total_com_ipi) / 100;
								
								var totais_html = '';
								totais_html += '<td colspan="11">Acréscimo Condição de Pagamento (';
								totais_html += number_format(pedido.pedido_acrescimo_condicao_pagamento, 3, ',', '.');
								totais_html += '%)</td>';
								totais_html += '<td align="right">' + number_format(acrescimo_condicao_pagamento, 3, ',', '.')  + '</td>';
								
								$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
								
								total_com_ipi += acrescimo_condicao_pagamento;
								totais_html = '';
								totais_html += '<td colspan="11">TOTAIS DO ' + (tipo_pedido != 'O' ? 'PEDIDO' : 'ORÇAMENTO') + '</td>';
								totais_html += '<td align="right">' + number_format(total_com_ipi, 3, ',', '.') + '</td>';
								
								$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
							}
							*/
						}
						
						alterarCabecalhoListagem('#itens_processados_tabela');
						alterarCabecalhoTabelaConteudo();
						
	
						
						//
						/*
						if(pedido.orc == '1')
						{
							if(pedido.razao_social_do_cliente.length > 0)
							{
								$('#transformar_pedido').show();
							}
						
							$('#transformar_pedido').click(function (e) {
								e.preventDefault();
								
								apprise('Você realmente deseja transformar esse orçamento em pedido?', {
									'verify': true,
									'textYes': 'Sim',
									'textNo': 'Não'
								}, function (dado) {
									if (dado)
									{
										db.transaction(function(x) {
											x.executeSql("UPDATE pedidos SET orc = '0' WHERE codigo = ?", [codigo_do_pedido]);
											alert("UPDATE pedidos SET orc = '0' WHERE codigo = ?" + codigo_do_pedido);
											window.location = 'pedidos.html';
										});
									}
								});
							});
						}
						*/
						
						/*
						$('#excluir').click(function () {
							apprise('Você realmente deseja excluir o ' + (pedido.orc ? 'orçamento' : 'pedido') + '?', {
								'verify': true,
								'textYes': 'Sim',
								'textNo': 'Não'
							}, function (dado) {
								if (dado)
								{
									db.transaction(function(x) {
										x.executeSql('DELETE FROM pedidos WHERE codigo = ?', [codigo_do_pedido]);
										
										window.location = (pedido.orc ? 'orcamentos.html' : 'pedidos_analisados.html');
									});
								}
							});
							
							return false;
						});
						*/
						
						//
						/*
						if (pedido.orc)
						{
							$('#editar').text('Editar Orçamento');
							
							$('#excluir').text('Excluir Orçamento');
							
							$('h2').text('Espelho do Orçamento');
						}
						*/
						/*
						if (!pedido.exportado)
						{
							$('#editar').attr('href', (pedido.orc ? 'orcamentos_adicionar' : 'pedidos_adicionar') + '.html#' + pedido.codigo).parents('p').show();
						}
						*/
						
						// empresa/representante
						/*
						x.executeSql(
							'SELECT * FROM informacoes_do_representante', [], function(x, dados) {
								if (dados.rows.length)
								{
									var representante = dados.rows.item(0);
									
									$('#empresa').append('<p style="margin-top: 0;">' + representante.razao_social_da_empresa + ' - CNPJ: ' + representante.cnpj_da_empresa + '<br>' + representante.endereco_da_empresa + ', ' + representante.numero_da_empresa + ' - ' + representante.bairro_da_empresa + ' - CEP: ' + representante.cep_da_empresa + ' - ' + representante.municipio_da_empresa + ' - ' + representante.uf_da_empresa + '</p><p>Telefone: ' + representante.telefone_1_da_empresa + ' - E-mail: ' + representante.email_da_empresa + '</p>');

									$('.rs_rep').text(representante.razao_social);
									$('.cod_rep').text(representante.codigo);
									$('.cnpj_rep').text(representante.cnpj);
									$('.tel_rep').text(representante.telefone);
									$('.email_rep').text(representante.email);
									$('.end_rep').text(representante.endereco);
									$('.bairro_rep').text(representante.bairro);
									$('.cep_rep').text(representante.cep);
									$('.mun_rep').text(representante.municipio);
									$('.mun_est_rep').text(representante.municipio + (representante.uf ? '/' + representante.uf : ''));
								}
							}
						);
						*/
						
						// informações gerais
						
						$('.oc').text(pedido.ordem_de_compra);
						$('.numero').text(pedido.exportado ? pedido.codigo : 'N/D');
						$('.tp_frete').text(strtoupper(pedido.tipo_de_frete));
						$('.emissao').text(date('d/m/Y', pedido.timestamp));
						$('.forma_pgto').text(pedido.descricao_da_forma_de_pagamento);
						$('.observacao').text(pedido.observacao ? pedido.observacao : 'N/D');
						
						var dt_ent = pedido.data_de_entrega;
						
						if (dt_ent)
						{
							var dia = substr(dt_ent, 6, 2);
							var mes = substr(dt_ent, 4, 2);
							var ano = substr(dt_ent, 0, 4);
							
							$('.dt_ent').text(dia + '/' + mes + '/' + ano);
						}
						else
						{
							$('.dt_ent').text('N/D');
						}
						
						// prospect/cliente
						
						if (pedido.id_pro)
						{
							$('#cli').hide();
							$('#pro').show();
							
							x.executeSql(
								'SELECT * FROM prospects WHERE id = ?', [pedido.id_pro], function(x, dados) {
									if (dados.rows.length)
									{
										var prospect = dados.rows.item(0);
		
										$('.rs_cli').text(prospect.nome);
										$('.cnpj_cli').text(prospect.cgc);
										$('.tel_cli').text(prospect.telefone);
										$('.email_cli').text(prospect.email);
										$('.end_cli').text(prospect.endereco);
										$('.bairro_cep_cli').text(prospect.bairro + (prospect.cep ? '/' + prospect.cep : ''));
										$('.contato_cli').text(prospect.contato);
										$('.mun_est_cli').text(prospect.municipio + (prospect.estado ? '/' + prospect.estado : ''));
									}
								}
							);
						}
						else
						{
							$('#cli').show();
							$('#pro').hide();
							
							x.executeSql(
								'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [pedido.codigo_do_cliente, pedido.loja_do_cliente], function(x, dados) {
									if (dados.rows.length)
									{
										var cliente = dados.rows.item(0);
		
										$('.rs_cli').text(cliente.razao_social);
										$('.cnpj_cli').text(cliente.cnpj);
										$('.tel_cli').text(cliente.telefone);
										$('.cod_cli').text(cliente.codigo);
										$('.lj_cli').text(cliente.loja);
										$('.email_cli').text(cliente.email);
										$('.end_cli').text(cliente.endereco);
										$('.bairro_cep_cli').text(cliente.bairro + (cliente.cep ? '/' + cliente.cep : ''));
										$('.contato_cli').text(cliente.pessoa_de_contato);
										$('.mun_est_cli').text(cliente.municipio + (cliente.estado ? '/' + cliente.estado : ''));
									}
								}
							);
						}
						
						// produtos
						
						x.executeSql(
							'SELECT * FROM pedidos WHERE codigo = ? ORDER BY timestamp ASC', [pedido.codigo], function(x, dados) {
								if (dados.rows.length)
								{
									var qtd_ven = 0;
									var qtd_fat = 0;
									
									for (i = dados.rows.length - 1; i >= 0; i--)
									{
										var dado = dados.rows.item(i);
										
										qtd_ven += dado.quantidade;
										qtd_fat += dado.quantidade_faturada;
										
										$('.itens').append('<tr><td style="text-align: center;">' + str_pad(i + 1, 2, 0, 'STR_PAD_LEFT') + '</td><td>' + dado.codigo_do_produto + '</td><td>' + dado.descricao_do_produto + '</td><td class="nao_exibir_impressao">' + dado.um_do_produto + '</td><td class="nao_exibir_impressao">' + (dado.descricao_da_tabela_de_preco ? dado.descricao_da_tabela_de_preco : dado.codigo_da_tabela_de_preco) + '</td><td style="text-align: right;">' + number_format(dado.preco, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.preco - round(dado.preco * (dado.desconto / 100), 2), 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.quantidade, 0, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.quantidade_faturada, 0, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.total_sem_ipi, 2, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.ipi, 2, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.total_com_ipi, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.valor_desconto, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.total_final, 2, ',', '.') + '</td></tr>');
									}

									$('.quantidade_vendida').text(number_format(qtd_ven, 0, ',', '.'));
									$('.quantidade_faturada').text(number_format(qtd_fat, 0, ',', '.'));
									$('.valor_total_desconto').text(number_format(dado.desconto_total_pedido, 2, ',', '.'));
									$('.valor_total_do_pedido').text(number_format(dado.valor_total_do_pedido, 2, ',', '.'));
								}
							}
						);
					}
					else
					{
						window.location = 'index.html';
					}
				}
			);
		});
		
		//----------------
		//-- Notas Fiscais
		
		$('#notas_fiscais').show();
		
		// Obter Pedido Processado
		db.transaction(function(x) {
			
			x.executeSql(
				'SELECT DISTINCT item_codigo_nota_fiscal, nota_data_emissao, nota_valor_total_produto, nota_valor_total, nota_valor_icms, nota_valor_ipi, nota_valor_frete, transportadora_nome, transportadora_telefone, nota_serie, nota_codigo, nota_filial FROM notas_fiscais WHERE item_codigo_pedido = ?', [codigo_do_pedido], function(x, dados) {
					
					var total_itens = dados.rows.length;
					
					if (total_itens)
					{
						
						var html = '';
						
						for(i = 0; i < total_itens; i ++)
						{
							var item_nota = dados.rows.item(i);
							
							html += '<tr>';
								html += '<td>' + item_nota.item_codigo_nota_fiscal + '</td>';
								html += '<td>' + protheus_data2data_normal(item_nota.nota_data_emissao) + '</td>';
								html += '<td align="right">' + number_format(item_nota.nota_valor_total_produto, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(item_nota.nota_valor_icms, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(item_nota.nota_valor_ipi, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(item_nota.nota_valor_frete, 3, ',', '.') + '</td>';
								html += '<td>' + item_nota.transportadora_nome + '</td>';
								html += '<td>' + item_nota.transportadora_telefone + '</td>';
								html += '<td align="center"><a href="#" id="ver_titulos" data-serie="'+item_nota.nota_serie+'" data-codigo="'+item_nota.nota_codigo+'">Ver Títulos</a> | <a href="notas_fiscais_visualizar.html#'+item_nota.nota_codigo+'_'+item_nota.nota_serie+'_'+item_nota.nota_filial+'">Ver Itens</a></td>';
							html += '</tr>';
							
						}
						
						$('.itens_notas').html(html);
						
						alterarCabecalhoListagem('#notas_fiscais_tabela');
						
					}
				}
			)
			
		});
		
		//-- #ver_titulos -  esta função esta no arquivo geral.js
		//$('#ver_titulos')
		
	}
		
});

function obter_dados_cliente(pedido, codigo, loja, callback){
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], 
			function(x, dados) 
			{
				var cliente = false;
				
				
				
	        	if(dados.rows.length) {
	        		var cliente = dados.rows.item(0);
	        		callback(pedido, cliente);
	        	}
				
			});
	});
			
	
	
	
}

function obter_dados_conversao_produto(pedido, cliente, callback){
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM conversao_unidade_medida WHERE unidade_medida = ? AND segunda_unidade_medida = ?', [pedido.produto_unidade_medida, pedido.produto_segunda_unidade_medida], 
			function(x, dados) 
			{
				var dados_conversao = false;
				
				
				
	        	if(dados.rows.length) {
	        		var dados_conversao = dados.rows.item(0);
	        		
	        	}
	        	
	        	callback(pedido, cliente, dados_conversao);
				
			});
	});
	
}

function copiar_pedido(codigo_do_pedido, codigo_da_empresa, editar, tabela, converter_pedido_orcamento)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ?', [codigo_do_pedido, codigo_da_empresa], 
			function(x, dados) 
			{
				//Dados
				var dado = dados.rows.item(0);
				
				obter_dados_cliente(dado, dado.pedido_codigo_cliente, dado.pedido_loja_cliente,  function(dado, dados_cliente){
					//Cabeçalho pedido
					var pedido_copiado = [];
	
					pedido_copiado['editar'] 						= editar;
					pedido_copiado['id_pedido'] 					= codigo_do_pedido;
					
					if(dado.prospect_codigo)
					{
						pedido_copiado['cliente_prospect'] = 'P';
						pedido_copiado['codigo_prospect'] 				= dado.prospect_codigo;
						pedido_copiado['loja_prospect']					= dado.prospect_codigo_loja;
						pedido_copiado['descricao_prospect']			= dado.prospect_codigo + '/' + dado.prospect_codigo_loja + ' - ' + dado.prospect_nome + ' - ' + dado.prospect_cgc;
					}
					else
					{
						pedido_copiado['cliente_prospect'] = 'C';
						pedido_copiado['codigo_cliente'] 				= dado.pedido_codigo_cliente;
						pedido_copiado['loja_cliente']					= dado.pedido_loja_cliente;
						pedido_copiado['descricao_cliente']				= dado.pedido_codigo_cliente + '/' + dado.pedido_loja_cliente + ' - ' + dado.cliente_nome + ' - ' + dado.cliente_cpf;
						pedido_copiado['codigo_cliente_entrega']		= dado.pedido_cliente_entrega;
						pedido_copiado['loja_cliente_entrega']			= dado.pedido_loja_entrega;
						pedido_copiado['desconto_cliente']				= dado.cliente_desconto;
						pedido_copiado['tabela_preco_sugerida']			= dados_cliente.tabela_preco;
						pedido_copiado['total_titulos_vencidos']		= dados_cliente.total_titulos_vencidos;
						pedido_copiado['descricao_cliente_entrega']		= dado.pedido_codigo_cliente + '/' + dado.pedido_loja_cliente + ' - ' + dado.cliente_nome + ' - ' + dado.cliente_cpf;
					}
					
					pedido_copiado['codigo_transportadora']			= dado.pedido_codigo_transportadora;
					pedido_copiado['forma_pagamento']				= dado.forma_pagamento_codigo;
	                pedido_copiado['condicao_pagamento']            = dado.pedido_condicao_pagamento;
					pedido_copiado['data_entrega']					= (dado.pedido_data_entrega ? protheus_data2data_normal(dado.pedido_data_entrega) : '');
					pedido_copiado['evento']						= dado.pedido_id_feira;
					pedido_copiado['filial']						= dado.pedido_filial;
					pedido_copiado['mennota']						= utf8_decode(dado.pedido_mensagem_nota);
					pedido_copiado['observacao_comercial']			= utf8_decode(dado.pedido_observacao_comercial);
					pedido_copiado['pedido_cliente']				= dado.pedido_pedido_cliente;
					pedido_copiado['tabela_precos']					= dado.pedido_tabela_precos;
					pedido_copiado['tipo_frete']					= dado.pedido_tipo_frete;
					pedido_copiado['tipo_pedido']					= dado.pedido_tipo_venda;
					pedido_copiado['status']						= dado.pedido_status;
					
					pedido_copiado['produtos'] = new Array();
				
					for (var i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						pedido_copiado['produtos'][dado.pedido_codigo_produto] = new Array();
						
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['codigo'] 				= dado.pedido_codigo_produto;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['percentual_modificador'] = dado.pedido_percentual_modificador;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['modificador'] 			= dado.pedido_modificador;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['familia'] 				= dado.produto_familia;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['derivacao'] 			= dado.produto_derivacao;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['descricao'] 			= dado.produto_descricao;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['ipi'] 					= dado.produto_ipi;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['preco_unitario'] 		= dado.pedido_preco_unitario;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['preco_venda'] 			= dado.pedido_preco_venda;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['quantidade'] 			= dado.pedido_quantidade;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['unidade_medida'] 		= dado.pedido_unidade_medida;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['segunda_unidade_medida']= dado.pedido_segunda_unidade_medida;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['local'] 				= dado.pedido_local;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['TES'] 					= dado.pedido_tipo_entrada_saida;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['CF'] 					= dado.pedido_codigo_fiscal;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['ST'] 					= 0;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['IPI'] 					= 0;
						pedido_copiado['produtos'][dado.pedido_codigo_produto]['ICMS'] 					= 0;					
					}
					
					pedido_copiado['converter_pedido_orcamento']	= converter_pedido_orcamento;
					
					if(converter_pedido_orcamento)
					{
						pedido_copiado['tipo_pedido'] = 'N';
					}
					
					
					
					if(tabela == 'orcamentos' && converter_pedido_orcamento != true)
					{
						sessionStorage['sessao_orcamento'] = serialize(pedido_copiado);
						location.href="pedidos_adicionar.html#O";
					}
					else
					{	
						sessionStorage['sessao_pedido'] = serialize(pedido_copiado);
						location.href="pedidos_adicionar.html";
					}
					
				});
			}
		)
	});
}


function copiar_pedido_processado(codigo_do_pedido, codigo_da_empresa)
{

	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ?', [codigo_do_pedido, codigo_da_empresa], 
			function(x, dados) 
			{
				
				var dado = dados.rows.item(0);
				obter_dados_cliente(dado, dado.pedido_codigo_cliente, dado.pedido_loja_cliente,  function(dado, dados_cliente){
					//Cabeçalho pedido
					var pedido_copiado = [];
					
					pedido_copiado['codigo_cliente'] 				= dado.cliente_codigo;
					pedido_copiado['loja_cliente']					= dado.cliente_loja;
					pedido_copiado['codigo_cliente_entrega']		= dado.pedido_cliente_entrega;
					pedido_copiado['loja_cliente_entrega']			= dado.pedido_loja_entrega;
					pedido_copiado['codigo_transportadora']			= dado.transportadora_codigo;
					pedido_copiado['condicao_pagamento']			= dado.cliente_condicao_pagamento;
					pedido_copiado['data_entrega']					= (dado.pedido_data_entrega ? protheus_data2data_normal(dado.pedido_data_entrega) : '');
					pedido_copiado['desconto_cliente']				= dado.cliente_desconto;
					pedido_copiado['descricao_cliente']				= dado.cliente_nome;
					pedido_copiado['descricao_cliente_entrega']		= dado.cliente_codigo + '/' + dado.cliente_loja + ' - ' + dado.cliente_nome + ' - ' + dado.cliente_cpf;
					pedido_copiado['filial']						= dado.pedido_filial;
					pedido_copiado['mennota']						= utf8_decode(dado.pedido_mensagem);
					pedido_copiado['pedido_cliente']				= dado.ip_pedido_cliente;
					pedido_copiado['tabela_precos']					= dado.pedido_tabela_precos;
					pedido_copiado['tipo_frete']					= dado.cliente_tipo_frete;
					pedido_copiado['tipo_pedido']					= 'N';
	
					pedido_copiado['produtos'] = new Array();
				
					for (var i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						
						pedido_copiado['produtos'][dado.ip_codigo_produto] = new Array();
						
						pedido_copiado['produtos'][dado.ip_codigo_produto]['codigo'] 				= dado.ip_codigo_produto;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['desconto'] 				= 0;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['descricao'] 			= dado.ip_descricao_produto;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['ipi'] 					= dado.produto_ipi;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['preco_unitario'] 		= dado.ip_preco_produto;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['preco_venda'] 			= dado.ip_preco_produto;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['quantidade'] 			= dado.ip_quantidade_vendida_produto;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['unidade_medida'] 		= dado.ip_unidade_medida_produto;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['segunda_unidade_medida']= dado.produto_segunda_unidade_medida;
						pedido_copiado['produtos'][dado.ip_codigo_produto]['local'] 				= dado.ip_local;
					}
					
					sessionStorage['sessao_pedido'] = serialize(pedido_copiado);
					
					window.location = "pedidos_adicionar.html";  
					
				});
			}
		)
	});
}


/**
* Metódo:		aplicar_descontos_cabecalho
* 
* Descrição:	Função Utilizada para aplicar os descontos do cabeçalho (Desconto do Cliente, Regra de Desconto)
* 
* Data:			23/10/2013
* Modificação:	23/10/2013
* 
* @access		public
* @param		string 					preco_produto		- Preço do Produto
* @param		string 					desconto_cliente	- Desconto do cliente
* @param		string 					regra_desconto		- Desconto da regra
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function aplicar_descontos_cabecalho(preco_produto, desconto_cliente, regra_desconto)
{
	// Aplicando Desconto do "Cliente" no preço do produto
	if(desconto_cliente > 0)
	{
		var preco = preco_produto - (preco_produto * (desconto_cliente / 100));
	}
	else
	{
		var preco = preco_produto;
	}

	// Aplicando Desconto da "Regra de desconto" no preço do produto
	if(regra_desconto  > 0)
	{
		var preco = preco - (preco * (regra_desconto / 100));
	}
	
	return preco;
}

function obterParcelas(id_pedido) {
	var where = "WHERE pedido_parcela_id_pedido = '" + id_pedido + "'";
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedido_parcela ' + where, [], function(x, dados) {
				var total_itens = dados.rows.length;
				var parcelas = [];
				var parcela = new Object;
				if (total_itens) {
					html = '';
					for(var i = 0; i < total_itens; i++ ) {
						dado = dados.rows.item(i);
						html += '<tr>';
							html += '<td align="right">' + parseInt(dado.pedido_parcela_numero_parcela) + '</td>';
							html += '<td align="center">' + protheus_data2data_normal(dado.pedido_parcela_data_vencimento.replace(/-/g, '')) + '</td>';
							html += '<td align="right">' + number_format(dado.pedido_parcela_valor_parcela, 3, ',', '.') + '</td>';
						html += '</tr>';
						parcela.numero = parseInt(dado.pedido_parcela_numero_parcela);
				    	parcela.data =  dado.pedido_parcela_data_vencimento;
				        parcela.valor = dado.pedido_parcela_valor_parcela;
				        parcelas.push(parcela);
						parcela = {};
					}
					$('.tabela_parcelas').html(html);
				}
			}
		)	
	});
}

function calcularValorModificador(valorUnitario, percentualModificador, modificador, callback) {
	var valorModificador = 0;
	if(modificador != '') {
		valorModificador = valorUnitario * (percentualModificador / 100);
	} else {
		valorModificador = 0;
	}
	callback(valorModificador);
}