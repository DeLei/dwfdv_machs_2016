$(document).ready(function() {
		
    // Desativar todos os campos quando clicar em id_informacao_cliente
    $('#id_adicionar_produtos').click(function() {

        $('#id_outras_informacoes').addClass('tab_desativada');
        $('#id_finalizar').addClass('tab_desativada');

        $("#tabs").tabs('select', 1);

        ativar_tabs();
    });

    /**
     * 
     * ID:			trocar_produto
     *
     * Descrição:	Utilizado para apagar todos os valores dos campos ligados ao produto par aque o representante possa selecionar um novo produto
     *
     */
    $('#trocar_produto').live('click', function() {
       $("#informacoes_produto").hide(); 
    	limpar_dados_produtos();
    });

    /**
     * 
     * ID:			adicionar_produto
     *
     * Descrição:	Utilizado para adicionar um produto e seus valores na sessao
     *
     */
    $('#adicionar_produto').live('click', function(e) {
    	    	
        e.preventDefault();
       
        var campos = [];
        campos['codigo_produto']		 = 'input[name=codigo_produto]';
        campos['preco_venda'] 			 = 'input[name=preco_venda]';
        campos['preco_unitario'] 		 = 'input[name=preco_unitario]';
        campos['quantidade']			 = 'input[name=quantidade]';
        campos['tipo_conversao'] 		 = 'input[name=tipo_conversao]';
        campos['valor_conversao'] 		 = 'input[name=valor_conversao]';
        campos['percentual_modificador'] = 'input[name=percentual_modificador]';
        
        
        if (verificar_existe($('input[name=codigo_produto]').val())) {
            erro_itens('O produto <b>' + $('input[name=codigo_produto]').val() + ' - ' + $('input[name=descricao_produto]').val() + '</b> já foi adicionado!');
        } else if (validar_campos(null, serialize(campos))) {
        	
            var codigo_produto			 = 	$('input[name=codigo_produto]').val();
            var descricao_produto		 = 	$('input[name=descricao_produto]').val();
            var preco_tabela			 = 	converter_decimal($('input[name=preco_tabela]').val());
            var preco_unitario			 = 	converter_decimal($('input[name=preco_unitario]').val());
            var preco_venda				 = 	converter_decimal($('input[name=preco_venda]').val());
            var quantidade				 = 	$('input[name=quantidade]').val();
            var tipo_conversao			 = 	$('input[name=tipo_conversao]').val();
            var valor_conversao			 = 	$('input[name=valor_conversao]').val();
            var percentual_modificador	 = 	converter_decimal($('input[name=percentual_modificador]').val());
            var modificador				 =  $('input[name=modificador]').val();
            var ipi 					 = 	converter_decimal($('input[name=ipi]').val());
            var unidade_medida 			 = 	$('input[name=unidade_medida]').val();
            var segunda_unidade_medida 	 = 	$('input[name=segunda_unidade_medida]').val();
            var local					 = 	$('input[name=local]').val();
            var familia					 =  $('input[name=familia]').val();
            var derivacao				 =  $('input[name=derivacao]').val();

            var produtos = new Array();
            produtos[codigo_produto] = new Array();
            produtos[codigo_produto]['codigo'] = codigo_produto;
            produtos[codigo_produto]['descricao'] = descricao_produto;
            produtos[codigo_produto]['preco_tabela'] = preco_tabela;
            produtos[codigo_produto]['preco_unitario'] = preco_unitario;
            produtos[codigo_produto]['preco_venda'] = preco_venda;
            produtos[codigo_produto]['quantidade'] = quantidade;
            produtos[codigo_produto]['tipo_conversao'] = tipo_conversao;
            produtos[codigo_produto]['valor_conversao'] = valor_conversao;
            produtos[codigo_produto]['percentual_modificador'] = percentual_modificador;
            produtos[codigo_produto]['modificador'] = modificador;
            produtos[codigo_produto]['ipi'] = ipi;
            produtos[codigo_produto]['unidade_medida'] = unidade_medida;
            produtos[codigo_produto]['segunda_unidade_medida'] = segunda_unidade_medida;
            produtos[codigo_produto]['local'] = local;
            produtos[codigo_produto]['familia'] = familia;
            produtos[codigo_produto]['derivacao'] = derivacao;
            
            var produtos = serialize(produtos);	           
            salvar_produto_sessao(produtos);
            exibir_produtos();
            $('#erro_item').hide('fast');
            limpar_dados_produtos();
            $('input[name=produto]').focus();
    		$("#informacoes_produto").hide();
        }
    });

    /**
     * 
     * ID:			avancar_passo_2
     *
     * Descrição:	Utilizado para salvar os dados editados e passar para o proximo passo
     *
     */
    $('#avancar_passo_2').live('click', function(e) {
    	
        e.preventDefault();

        $('.erro_itens').remove();

        var sessao_produtos = obter_produtos_sessao();

        if (sessao_produtos) {
            var total_produtos = Object.keys(sessao_produtos).length;
        } else {
            var total_produtos = 0;
        }
       
        var sucesso = true;

        if (total_produtos > 0) {
            
        	var produtosArray = [];
        	for(var i in sessao_produtos) { 
        		produtosArray.push(sessao_produtos[i]); 
        	}
        	
        	calculosFinalizacao(produtosArray, 0, true, function(sucesso){
        		if (sucesso == true) {
        			// Ativando Botao "Adicionar Produtos"
        			$('#id_outras_informacoes').removeClass('tab_desativada');

        			ativar_tabs();

        			//Chamando as funções do outras_informacoes
        			outras_informacoes();

        			// Acionando (Click) botao "Adicionar Produtos"
        			$("#id_outras_informacoes").click();
        		}
        	});

        } else {
            var descricao_tipo_pedido = obter_descricao_pedido('lower');
            mensagem('Adicione um produto no ' + descricao_tipo_pedido + '.');
        }
    });

    /**
     * 
     * CLASSE:			excluir_item
     *
     * Descrição:	Utilizado para excluir um item na sessao
     *
     */
    $('.excluir_item').live('click', function(e) {
        e.preventDefault();

        var codigo_produto = remover_zero_esquerda($(this).attr('href'));

        var sessao_produtos = obter_produtos_sessao();

        var produto = sessao_produtos[codigo_produto];

        confirmar('Deseja excluir o item <b>' + produto.codigo + ' - ' + produto.descricao + '</b>?',
            function() {
                $(this).dialog('close');

                delete sessao_produtos[codigo_produto];

                salvar_produtos_excluidos(serialize(sessao_produtos));

                exibir_produtos();

                $("#confirmar_dialog").remove();

            }
        );

    });

    $('.salvar_sessao_edicao').live('keyup', 'focusout', function() {
        var codigo_produto = $(this).data('codigo_produto');

        var percentual_modificador = converter_decimal($('input[name=editar_percentual_modificador_' + codigo_produto + ']').val());
        var quantidade = converter_decimal($('input[name=editar_quantidade_' + codigo_produto + ']').val());
        var preco_venda = converter_decimal($('input[name=editar_preco_venda_' + codigo_produto + ']').val());

        var sessao_produtos = obter_produtos_sessao();

        sessao_produtos[codigo_produto]['percentual_modificador'] = percentual_modificador;
        sessao_produtos[codigo_produto]['quantidade'] = quantidade;
        sessao_produtos[codigo_produto]['preco_venda'] = preco_venda;

        var produtos = serialize(sessao_produtos);

        salvar_produto_sessao(produtos);
    });
});

function calculosFinalizacao(produtosArray, iteracao, sucesso, callback){
	
	var objeto = produtosArray[iteracao];

	var codigo_produto = objeto.codigo;
	var percentual_modificador = converter_decimal($('input[name=editar_percentual_modificador_' + codigo_produto + ']').val());
	var preco_venda = converter_decimal($('input[name=editar_preco_venda_' + codigo_produto + ']').val());
	var quantidade = converter_decimal($('input[name=editar_quantidade_' + codigo_produto + ']').val());
	var tipo_conversao = converter_decimal($('input[name=editar_tipo_conversao_' + codigo_produto + ']').val());
	var valor_conversao = converter_decimal($('input[name=editar_valor_conversao_' + codigo_produto + ']').val());
	
	
	
	
	// Zerando sempre o background
	$('.produto_' + codigo_produto).css('background-color', '');

	calcularDescontoFlex(objeto.familia, objeto.percentual_modificador, objeto.modificador, function(percentual_comissao) {
		
		isModifcadorAcimaLimite(objeto.familia, objeto.percentual_modificador, objeto.modificador, function(isAcimaLimite) {
			
			var produtos = new Array();
			produtos[codigo_produto] = new Array();
			produtos[codigo_produto]['codigo'] = codigo_produto;
			produtos[codigo_produto]['descricao'] = objeto.descricao;
			produtos[codigo_produto]['preco_tabela'] = objeto.preco_tabela;
			produtos[codigo_produto]['preco_unitario'] = objeto.preco_unitario;
			produtos[codigo_produto]['preco_venda'] = preco_venda;
			produtos[codigo_produto]['quantidade'] = quantidade;
			produtos[codigo_produto]['tipo_conversao'] = tipo_conversao;
			produtos[codigo_produto]['valor_conversao'] = valor_conversao;
			produtos[codigo_produto]['quantidade'] = quantidade;
			produtos[codigo_produto]['familia'] = objeto.familia;
			produtos[codigo_produto]['derivacao'] = objeto.derivacao;
			produtos[codigo_produto]['percentual_modificador'] = percentual_modificador;
			produtos[codigo_produto]['percentual_comissao'] = percentual_comissao;
			produtos[codigo_produto]['modificador'] = objeto.modificador;               
			produtos[codigo_produto]['ipi'] = objeto.ipi;
			produtos[codigo_produto]['unidade_medida'] = objeto.unidade_medida;
			produtos[codigo_produto]['segunda_unidade_medida'] = objeto.segunda_unidade_medida;
			produtos[codigo_produto]['local'] = objeto.local;
			produtos[codigo_produto]['TES'] = objeto.TES;
			produtos[codigo_produto]['CF'] = objeto.CF;
			produtos[codigo_produto]['ST'] = 0;
			produtos[codigo_produto]['IPI'] = 0;
			produtos[codigo_produto]['ICMS'] = 0;
			
			if(isAcimaLimite && objeto.modificador ==  'A') {
				salvar_sessao('status_pedido', 'B');
				salvar_sessao('motivo_bloqueio', 'Acrescimo acima do limite permitido.');
			} 
			
			if(isAcimaLimite && objeto.modificador ==  'D') {
				salvar_sessao('status_pedido', 'B');
				salvar_sessao('motivo_bloqueio', 'Desconto acima do limite permitido.');
			} 
			
			var produtos = serialize(produtos);
			
			salvar_produto_sessao(produtos);
			
			//-----------------//
			//--- Validação ---//
			//-----------------//

			var campos = [];
			campos['percentual_modificador']	=	'input[name=editar_percentual_modificador_' + codigo_produto + ']';
			campos['preco_venda'] 				=	'input[name=editar_preco_venda_' + codigo_produto + ']';
			campos['preco_unitario'] 		 	=	'input[name=editar_preco_unitario_' + codigo_produto + ']';
			campos['quantidade'] 				=	'input[name=editar_quantidade_' + codigo_produto + ']';
			campos['valor_conversao'] 			=	'input[name=editar_valor_conversao_' + codigo_produto + ']';
			campos['tipo_conversao'] 			=	'input[name=editar_tipo_conversao_' + codigo_produto + ']';
			
			if (!validar_campos(codigo_produto, serialize(campos))) {
				sucesso = false;
			}

			if(produtosArray.length == (iteracao + 1)){
				callback(sucesso);
			} else {
				iteracao = iteracao + 1;
				calculosFinalizacao(produtosArray, iteracao, sucesso, callback);
			}
		});
	});
}

function adicionar_produtos() {
	
    // Recalcular valores caso ouver alteração no cliente ou tabela de preços
    recalcular_precos();

    // Exibir valores do cabeçalho
    exibir_cabecalho();

    // Obter Produtos (Autocomplete)
    obter_produtos();

    exibir_produtos();

    // Função para os calculos do KEY UP
    calculos_automaticos('input[name=preco_venda]', 'input[name=preco_unitario]', 'input[name=percentual_modificador]', 'input[name=quantidade]', 'input[name=ipi]', null, 'input[name=total_item]', '#label_modificador', 'input[name=modificador]');
    
    alterarCabecalhoTabelaConteudo();
}

/**
* Metódo:		recalcular_precos
* 
* Descrição:	Função Utilizada para para recalcular preços e descontos/acréscimos quando a tabela de preços ou cliente é alterado
* 
* Data:			22/10/2013
* Modificação:	22/10/2013
* 
* @access		public
* @param		string 			codigo 	- codigo do produto

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function recalcular_precos() {
	
    var sessao_produtos = obter_produtos_sessao();
    if (sessao_produtos) {
        var total_produtos = Object.keys(sessao_produtos).length;
    }
    var indice = 0;

    if (sessao_produtos) {
        $.each(sessao_produtos, function(key, produto) {
        	console.log('SELECT * FROM produtos  WHERE tb_codigo = \''+obter_valor_sessao('tabela_precos')+'\' AND produto_codigo = '+produto.codigo);	
            db.transaction(function(x) {
                x.executeSql('SELECT * FROM produtos  WHERE tb_codigo = ? AND produto_codigo = ?', [obter_valor_sessao('tabela_precos'), produto.codigo], function(x, dados) {
                    if (dados.rows.length) {                    	
                        var dado = dados.rows.item(0);
                        var preco = aplicar_descontos_cabecalho(dado.ptp_preco);
                        var codigo_produto = produto.codigo;
                        
                        preco = aplicar_acrescimo_condicao_pagamento(preco,codigo_produto );
                        
                        var produtos = new Array();
                        produtos[codigo_produto] = new Array();
                        produtos[codigo_produto]['codigo'] = produto.codigo;
                        produtos[codigo_produto]['descricao'] = produto.descricao;
                        produtos[codigo_produto]['preco_tabela'] = produto.ptp_preco;
                        produtos[codigo_produto]['preco_unitario'] = preco;
                        produtos[codigo_produto]['quantidade'] = produto.quantidade;
                        produtos[codigo_produto]['percentual_modificador'] = produto.percentual_modificador;
                        produtos[codigo_produto]['modificador'] = produto.modificador;
                        produtos[codigo_produto]['familia'] = produto.familia;
                        produtos[codigo_produto]['derivacao'] = produto.derivacao;
                        produtos[codigo_produto]['ipi'] = produto.ipi;
                        produtos[codigo_produto]['unidade_medida'] = produto.unidade_medida;
                                               
                        produtos[codigo_produto]['local'] = produto.local;
                        produtos[codigo_produto]['TES'] = produto.TES;
                        produtos[codigo_produto]['CF'] = produto.CF;
                        produtos[codigo_produto]['ST'] = produto.ST;
                        produtos[codigo_produto]['IPI'] = produto.IPI;
                        produtos[codigo_produto]['ICMS'] = produto.ICMS;
                       
                        if(produtos[codigo_produto]['modificador'] == 'D') {
                        	produtos[codigo_produto]['preco_venda'] = parseFloat(preco) - parseFloat((preco * (produto.percentual_modificador / 100)));
                        } else if(produtos[codigo_produto]['modificador'] == 'A') {
                        	produtos[codigo_produto]['preco_venda'] = parseFloat(preco) + parseFloat((preco * (produto.percentual_modificador / 100)));
                        } else {
                        	produtos[codigo_produto]['preco_venda'] = parseFloat(preco);
                        }                        
                        
                        var valor_conversao = 1;
                        if(validar_digitos(produto.derivacao) && produto.derivacao > 0){   
                        	valor_conversao = produto.derivacao;
                        }
                        produtos[codigo_produto]['segunda_unidade_medida'] = null;
                        //Alterado para utilizar a derivação para se adequar a regra da Machs
                        produtos[codigo_produto]['tipo_conversao'] = '/';
                        produtos[codigo_produto]['valor_conversao'] = valor_conversao;
                        
                        var produtos = serialize(produtos);

                        salvar_produto_sessao(produtos);

                    } else {
                    	
                        var codigo_produto = remover_zero_esquerda(produto.codigo);

                        var produtos = new Array();
                        produtos[codigo_produto] = new Array();
                        produtos[codigo_produto]['codigo'] = produto.codigo;
                        produtos[codigo_produto]['descricao'] = produto.descricao;
                        produtos[codigo_produto]['preco_tabela'] = produto.preco_tabela;
                        produtos[codigo_produto]['preco_unitario'] = produto.preco_unitario;
                        produtos[codigo_produto]['preco_venda'] = produto.preco_venda
                        produtos[codigo_produto]['quantidade'] = produto.quantidade;
                        produtos[codigo_produto]['percentual_modificador'] = produto.percentual_modificador;
                        produtos[codigo_produto]['modificador'] = produto.modificador;
                        produtos[codigo_produto]['familia'] = produto.familia;
                        produtos[codigo_produto]['derivacao'] = produto.derivacao;
                        produtos[codigo_produto]['ipi'] = produto.ipi;
                        produtos[codigo_produto]['unidade_medida'] = produto.unidade_medida;
                        produtos[codigo_produto]['segunda_unidade_medida'] = null;
                        produtos[codigo_produto]['local'] = produto.local;
                        produtos[codigo_produto]['TES'] = produto.TES;
                        produtos[codigo_produto]['CF'] = produto.CF;
                        produtos[codigo_produto]['ST'] = produto.ST;
                        produtos[codigo_produto]['IPI'] = produto.IPI;
                        produtos[codigo_produto]['ICMS'] = produto.ICMS;
                        
                        
                        var valor_conversao = 1;
                        if(validar_digitos(produto.derivacao) && produto.derivacao > 0){   
                        	valor_conversao = produto.derivacao;
                        }
                        //Alterado para utilizar a derivação para se adequar a regra da Machs
                        produtos[codigo_produto]['tipo_conversao'] = '/';
                        produtos[codigo_produto]['valor_conversao'] = valor_conversao;
                        
                        var produtos = serialize(produtos);

                        salvar_produto_removido_sessao(produtos);

                        delete sessao_produtos[codigo_produto];

                        salvar_produtos_excluidos(serialize(sessao_produtos));
                    }

                    indice++;

                    if (indice == total_produtos) {
                        exibir_produtos();
                    }
                });
            });
        });
    }
}

/**
* Metódo:		verificar_existe
* 
* Descrição:	Função Utilizada para saber se existe o produto na sessão
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo 	- codigo do produto

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function verificar_existe(codigo) {
    var codigo_produto = remover_zero_esquerda(codigo);
    var sessao_produtos = obter_produtos_sessao();

    if (sessao_produtos[codigo_produto]) {
        return true;
    } else {
        return false;
    }
}

/**
* Metódo:		limpar_dados_produtos
* 
* Descrição:	Função Utilizada para apagar todos os campos ligados ao produto
* 
* Data:			19/10/2013
* Modificação:	23/10/2013
* 
* @access		public

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function limpar_dados_produtos() {
    $('input[name="produto"]').val("");
    $('input[name="codigo_produto"]').val("");
    $('input[name="descricao_produto"]').val("");
    $('input[name="preco_tabela"]').val("");
    $('input[name="preco_unitario"]').val("");
    $('input[name="preco_venda"]').val("");
    $('input[name="quantidade"]').val("");
    $('input[name="percentual_modificador"]').val("");
    $('input[name="modificador"]').val("");
    $('input[name="ipi"]').val("");
    $('input[name="total_item"]').val("");
    $('input[name="unidade_medida"]').val("");
    $('input[name="segunda_unidade_medida"]').val("");
    $('input[name="tipo_conversao"]').val("");
    $('input[name="valor_conversao"]').val("");
    $('input[name="local"]').val("");

    $('#trocar_produto').hide();
    $('input[name="produto"]').removeAttr('readonly');
    $('#erro_item').hide();
}

/**
 * Metódo:		calculos_automaticos
 * 
 * Descrição:	Função Utilizada realizar os calculos automaticos de desconto/acréscimo e preço via keyup para tudo 
 * 
 * Data:		19/10/2013
 * Modificação:	19/10/2013
 * 
 * @access		public
 * @param		string 			campo_preco_venda				-	campo input do preco de venda
 * @param		string 			campo_preco_unitario			-	campo input do preco unitario
 * @param		string 			campo_percentual_modificador	-	campo input do desconto/acréscimo
 * @param		string 			campo_quantidade				-	campo input do quantidade
 * @param		string 			campo_ipi						-	campo input do ipi
 * @param		string 			campo_total_geral				-	campo input do total geral
 * @param		string 			campo_total_item				-	campo input do ptotal do item
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function calculos_automaticos(campo_preco_venda, campo_preco_unitario, campo_percentual_modificador, campo_quantidade, campo_ipi, campo_total_geral, campo_total_item, campo_label_modificador, campo_modificador) {

    // Calcular Desconto/Acréscimo
    $(campo_preco_venda).keyup(function() {
    	
        var preco_unitario = converter_decimal($(campo_preco_unitario).val());
        var preco_venda = converter_decimal($(this).val());
        
        if(preco_venda > preco_unitario) {
        	$(campo_label_modificador).text('Acréscimo (%)');
        	$(campo_modificador).val("A");
        } else if(preco_venda == preco_unitario) {
        	$(campo_label_modificador).text('⠀');
        	$(campo_modificador).val("");
        } else {
        	$(campo_label_modificador).text('Desconto (%)');
        	$(campo_modificador).val("D");
        }
        
        var valor_diferenca = preco_unitario - preco_venda;
        var percentagem = valor_diferenca / preco_unitario * 100;

        if (percentagem < 0) {
            percentagem *= (-1);
        }

        // Iserindo percentagem no campo
        $(campo_percentual_modificador).val(number_format(percentagem, 3, ',', '.'));

        calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);

        exibir_totais();
    });

    // Calcular Preço de Venda pelo Desconto/Acréscimo
    $(campo_percentual_modificador).keyup(function() {
        var preco_unitario = converter_decimal($(campo_preco_unitario).val());
        var percentual_modificador = converter_decimal($(this).val());

        var percentagem = percentual_modificador / 100;
        var valor_diferenca = preco_unitario * percentagem;
        var preco_venda = preco_unitario - valor_diferenca;

        if (preco_venda < 0) {
            preco_venda = 0;
        }

        // Iserindo preco_venda no campo
        $(campo_preco_venda).val(number_format(preco_venda, 3, ',', '.'));

        calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);

        exibir_totais();
    });

    // Calcular Preço Total pela quantidade
    $(campo_quantidade).keyup(function() {
        calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);

        exibir_totais();
    });
}


function validar_quantidade_multipla_produto(quantidade_venda, valor_conversao, tipo_conversao){
	if(tipo_conversao == '/'){
		var quantidade_fechada =  quantidade_venda / valor_conversao;            	
	}else{
		var quantidade_fechada = quantidade_venda * valor_conversao;            	
	}

	if(quantidade_fechada > 0){    	
		return (quantidade_fechada % 1 === 0);
	}

	return true;
}

/**
 * Metódo:		validar_campos
 * 
 * Descrição:	Função Utilizada para exibir os erros 
 * 
 * Data:			19/10/2013
 * Modificação:	19/10/2013
 * 
 * @access		public
 * @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
 * @param		string 			mensagem_erro	- mensagem do erro
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function validar_campos(codigo_produto, campos) {
	
    var campos = unserialize(campos);
    var sucesso = true;
     
    
    console.log(JSON.stringify(campos));
    
    // Se desconto/acréscimo for vazio, inserir o valor 0
    if (!$(campos['percentual_modificador']).val()) {
        $(campos['percentual_modificador']).val(number_format(0, 3, ',', '.'));
    }
    
    if (!$(campos['codigo_produto']).val() && campos['codigo_produto']) // Validação do Produto
    {
        erro_itens('Selecione um Produto.');
        sucesso = false;
    } else if (!validar_decimal($(campos['preco_venda']).val()) || converter_decimal($(campos['preco_venda']).val()) <= 0) // Validação do Preço de Venda 
    {
        if (converter_decimal($(campos['preco_venda']).val()) <= 0) {
            erro_itens('Digite um valor para o <b>Preço de Venda</b>.', codigo_produto);
        } else {
            erro_itens('O valor "<b>' + $(campos['preco_venda']).val() + '</b>" no campo <b>Preço de Venda</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>1.100,250<b/>', codigo_produto);
        }

        sucesso = false;
    } else if (!validar_digitos($(campos['quantidade']).val()) || $(campos['quantidade']).val() <= 0) // Validação da Quantidade
    {
        if (!$(campos['quantidade']).val()) {
            erro_itens('Digite uma quantidade.', codigo_produto);
            sucesso = false;
        } else {
        	erro_itens('O valor "<b>' + $(campos['quantidade']).val() + '</b>" no campo <b>Quantidade</b> não contém apenas digitos.', codigo_produto);
            sucesso = false;
        }
    } else if(!validar_quantidade_multipla_produto($(campos['quantidade']).val(), $(campos['valor_conversao']).val(), $(campos['tipo_conversao']).val())){
		erro_itens('O produto  é múltiplo de  '+$(campos['valor_conversao']).val()+'.', codigo_produto);
        sucesso = false;
	}
    else if (!validar_decimal($(campos['percentual_modificador']).val())) // Validação do Desconto
    {
    	erro_itens('O valor "<b>' + $(campos['percentual_modificador']).val() + '</b>" no campo <b>Desconto</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>50,250<b/>', codigo_produto);
    	sucesso = false;
    } else if ((converter_decimal($(campos['preco_unitario']).val()) > converter_decimal($(campos['preco_venda']).val())) && (converter_decimal($(campos['percentual_modificador']).val()) > 99.990)) // Validação do Desconto
    {	
        erro_itens('O valor "<b>' + $(campos['percentual_modificador']).val() + '</b>" no campo <b>Modificador</b> não pode ser maior que 99.990%.', codigo_produto);
        sucesso = false;
    } else if (converter_decimal($(campos['percentual_modificador']).val()) > 100 && $(campos['preco_unitario']).val() > $(campos['preco_venda']).val()) // Validação do Desconto
    {
        erro_itens('O valor "<b>' + $(campos['percentual_modificador']).val() + '</b>" no campo <b>Modificador</b> não pode ser maior que 100.', codigo_produto);

        sucesso = false;
    }

    return sucesso;
}

/**
 * Metódo:		erro_itens
 * 
 * Descrição:	Função Utilizada para exibir os erros 
 * 
 * Data:			19/10/2013
 * Modificação:	19/10/2013
 * 
 * @access		public
 * @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
 * @param		string 			mensagem_erro	- mensagem do erro
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function erro_itens(mensagem_erro, codigo_produto) {
    var icone = '<img src="img/warning.png" style="vertical-align: middle" /> ';

    if (codigo_produto) {
        $('<tr class="erro_itens" style="background-color: #F0798E"><td colspan="10">' + icone + mensagem_erro + '</td></tr>').insertAfter($('.produto_' + codigo_produto));

        $('.produto_' + codigo_produto).css('background-color', '#F0798E');
    } else {

        $('#erro_item').html(icone + mensagem_erro);
        $('#erro_item').show('fast');
    }
}

/**
 * Metódo:		obter_produtos_sessao
 * 
 * Descrição:	Função Utilizada para obter os itens inseridos na sessão
 * 
 * Data:			19/10/2013
 * Modificação:	19/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_produtos_sessao() {
    var sessao_pedido = [];

    // Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
    if (sessionStorage[sessionStorage['sessao_tipo']]) {
        sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
    }

    if (sessao_pedido['produtos']) {
        return sessao_pedido['produtos'];
    } else {
        return false;
    }
}

/**
 * Metódo:		obter_produtos_removidos_sessao
 * 
 * Descrição:	Função Utilizada para obter os itens removidos na sessão
 * 
 * Data:		22/10/2013
 * Modificação:	22/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_produtos_removidos_sessao() {
    var sessao_pedido = [];

    // Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
    if (sessionStorage[sessionStorage['sessao_tipo']]) {
        sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
    }

    if (sessao_pedido['produtos_removidos']) {
        return sessao_pedido['produtos_removidos'];
    } else {
        return false;
    }
}

/**
 * Metódo:		exibir_totais
 * 
 * Descrição:	Função Utilizada para exibir os totais dos itens
 * 
 * Data:			29/10/2013
 * Modificação:	29/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function exibir_totais() {

    var total_preco_unitario = 0;
    var total_modificador = 0;
    var total_preco_venda = 0;
    var total_quantidade = 0;
    var total = 0;
    var total_ipi = 0;
    var total_geral = 0;

    var sessao_produtos = obter_produtos_sessao();

    if (sessao_produtos) {

        $.each(sessao_produtos, function(key, objeto) {

            // -------
            // Variaveis para os TOTAIS
        	total_quantidade += parseFloat(converter_decimal($('input[name=editar_quantidade_' + objeto.codigo + ']').val()));
            total_preco_unitario += parseFloat(converter_decimal($('input[name=editar_preco_unitario_' + objeto.codigo + ']').val())) * parseFloat(converter_decimal($('input[name=editar_quantidade_' + objeto.codigo + ']').val()));
            total_preco_venda += parseFloat(converter_decimal($('input[name=editar_preco_venda_' + objeto.codigo + ']').val()));            
            total += parseFloat(converter_decimal($('input[name=editar_total_item_' + objeto.codigo + ']').val()));
            total_geral += parseFloat(converter_decimal($('input[name=editar_total_geral_' + objeto.codigo + ']').val()));
            	
        });

        console.log('total_preco_unitario: '+total_preco_unitario);
        console.log('total: '+total);
        
        
        
        total_modificador =  (total * 100 / total_preco_unitario) -100 ;
        
        total_ipi = (total_geral - total) * 100 / total_geral;
    }

    var descricao_tipo_pedido = obter_descricao_pedido('upper');

    // Totais
    var html = '';
    html += '<td colspan="2">TOTAIS DO ' + descricao_tipo_pedido + '</td>';
    html += '<td align="right">' + number_format(total_preco_unitario, 3, ',', '.') + '</td>';
    html += '<td align="right">' + number_format(total_modificador, 3, ',', '.') + '</td>';
    html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
    html += '<td align="right">' + total_quantidade + '</td>';
    html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
    //html += '<td align="right">' + number_format(total_ipi, 3, ',', '.') +'</td>';
    //html += '<td align="right">' + number_format(total_geral, 3, ',', '.') + '</td>';
    html += '<td align="right"></td>';

    $('.novo_grid_rodape').html(html);
}

/**
 * Metódo:		exibir_produtos
 * 
 * Descrição:	Função Utilizada para exibir os itens dos produtos inseridos na sessao e Os itens excluídos (Um item é excluido quando o representante altera a tabela de preços e o item não existe nessa tabela)
 * 
 * Data:		19/10/2013
 * Modificação:	19/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function exibir_produtos() {

    var sessao_produtos = obter_produtos_sessao();
    if (sessao_produtos) {
        var total_produtos = Object.keys(sessao_produtos).length;
    } else {
        var total_produtos = 0;
    }

    if (total_produtos > 0) {
        $('.itens_pedido').empty();

        $.each(sessao_produtos, function(key, objeto) {
        	
            var html = '';
            html += '<td align="center">' + objeto.codigo + '</td>';
            html += '<td>' + objeto.descricao + '</td>';
            html += '<td align="right">' + number_format(objeto.preco_unitario, 3, ',', '.') + '<input type="hidden" name="editar_preco_unitario_' + objeto.codigo + '" value="' + number_format(objeto.preco_unitario, 3, ',', '.') + '" /></td>';
            html += '<td align="center"><label id="label_modificador_editar_' + objeto.codigo + '">';
            objeto.modificador == 'D' ? html += 'Desconto' : (objeto.modificador == 'A' ? html += 'Acréscimo' : null); 
            html += '</label><input name="editar_percentual_modificador_' + objeto.codigo + '" class="salvar_sessao_edicao" data-codigo_produto="' + objeto.codigo + '" type="text" size="13" value="' + number_format(objeto.percentual_modificador, 3, ',', '.') + '" readonly /></td>';
            html += '<input type="hidden" name="editar_modificador_' + objeto.codigo + '" value="' + objeto.modificador + '">';
            html += '<td align="center"><input name="editar_preco_venda_' + objeto.codigo + '" class="salvar_sessao_edicao" data-codigo_produto="' + objeto.codigo + '" type="text" size="13" value="' + number_format(objeto.preco_venda, 3, ',', '.') + '" readonly /></td>';
            html += '<td align="center"><input name="editar_quantidade_' + objeto.codigo + '" class="salvar_sessao_edicao" data-codigo_produto="' + objeto.codigo + '" type="text" size="8" value="' + objeto.quantidade + '" /></td>';
            html += '<td align="center" style="display:none;" ><input name="editar_tipo_conversao_' + objeto.codigo + '" class="salvar_sessao_edicao" data-codigo_produto="' + objeto.codigo + '" type="text" size="8" value="' + objeto.tipo_conversao + '" /></td>';
            html += '<td align="center" style="display:none;"><input name="editar_valor_conversao_' + objeto.codigo + '" class="salvar_sessao_edicao" data-codigo_produto="' + objeto.codigo + '" type="text" size="8" value="' + objeto.valor_conversao + '" /></td>';
            html += '<td align="right"><input name="editar_total_item_' + objeto.codigo + '" type="text" size="13" value="' + number_format(objeto.preco_venda * objeto.quantidade, 3, ',', '.') + '" readonly /></td>';
            //html += '<td align="right">' +  number_format(objeto.ipi, 3, ',', '.') + '<input type="hidden" name="editar_ipi_' + objeto.codigo + '" value="' + number_format(objeto.ipi, 3, ',', '.') + '" /></td>';
            //html += '<td align="center"><input name="editar_total_geral_' + objeto.codigo + '" type="text" size="13" value="' + number_format((parseFloat(objeto.preco_venda) + parseFloat(objeto.preco_venda * (objeto.ipi / 100))) * objeto.quantidade, 3, ',', '.')  + '" disabled="disabled" /></td>';
            html += '<td align="center"><a class="excluir_item botao_c_grid" href="' + objeto.codigo + '">Excluir</a></td>';

            $('.itens_pedido').append('<tr class="produto_' + objeto.codigo + '">' + html + '</tr>');

            // Chamar calculo automatico (Funções do keyup)
            calculos_automaticos('input[name=editar_preco_venda_' + objeto.codigo + ']', 'input[name=editar_preco_unitario_' + objeto.codigo + ']', 'input[name=editar_percentual_modificador_' + objeto.codigo + ']', 'input[name=editar_quantidade_' + objeto.codigo + ']', 'input[name=editar_ipi_' + objeto.codigo + ']', 'input[name=editar_total_geral_' + objeto.codigo + ']', 'input[name=editar_total_item_' + objeto.codigo + ']', '#label_modificador_editar_' + objeto.codigo, 'input[name=editar_modificador_' + objeto.codigo + ']');

        });

        $('.itens_pedido').append('<tr class="novo_grid_rodape"></tr>');

        exibir_totais();

    } else {
        $('.itens_pedido').empty();

        $('.itens_pedido').append('<tr><td colspan="10" style="color:red">Nenhum produto adicionado.</td></tr>');

        $('.itens_pedido').append('<tr class="novo_grid_rodape"></tr>');

        exibir_totais();
    }

    //-------------------
    //-- Itens Excluidos
    //-------------------

    var sessao_produtos_removidos = obter_produtos_removidos_sessao();
    if (sessao_produtos_removidos) {
        $('.itens_removidos').empty();
        var titulo = '<b>Produto removido por não existir na tabela de preços.</b>';
        $('.itens_removidos').append(titulo);

        $.each(sessao_produtos_removidos, function(key, objeto) {

            var html = '';
            html += '<li>' + objeto.codigo + ' - ' + objeto.descricao + '</li>';

            $('.itens_removidos').append('<ul>' + html + '</ul>');
        });
    }

    alterarCabecalhoListagem('#itens_pedido_tabela');
}

/**
 * Metódo:		calcular_total
 * 
 * Descrição:	Função Utilizada para calcular o valor total dos itens e exibi-los
 * 
 * Data:			19/10/2013
 * Modificação:	19/10/2013
 * 
 * @access		public
 * @param		string 			campo_preco_venda - input do preco de venda
 * @param		string 			campo_ipi
 * @param		string 			campo_quantidade
 * @param		string 			campo_total_geral
 * @param		string 			campo_total_item
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item) {
    var preco_venda = converter_decimal($(campo_preco_venda).val());
    var ipi = converter_decimal($(campo_ipi).val());
    //var valor_ipi				= preco_venda * (ipi / 100);
    var valor_ipi = 0;
    var quantidade = $(campo_quantidade).val();

    if (!quantidade) {
        quantidade = 1;
    }

    // Inserindo total_item no campo
    if (campo_total_item) {
        $(campo_total_item).val(number_format(parseFloat(preco_venda) * quantidade, 3, ',', '.'));
    }

    $(campo_total_geral).val(number_format((parseFloat(preco_venda) + parseFloat(valor_ipi)) * quantidade, 3, ',', '.'));
}


/**
 * Metódo:		obter_produtos
 * 
 * Descrição:	Função Utilizada para buscar os produtos na tabela de produtos onde a tabela de preços for igual a selecionada
 * 
 * Data:			17/10/2013
 * Modificação:	17/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_produtos() {
    $('#erro_item').hide();
    $('#carregando_produtos').show();

    if (info.empresa) {
        //var where = " AND empresa = '" + info.empresa + "'";
    }

    if (obter_valor_sessao('filial') && obter_valor_sessao('filial') != 'undefined') {
        var wheres = " AND (produto_filial = '" + obter_valor_sessao('filial') + "' OR produto_filial = ' ')";
    }

    db.transaction(function(x) {
        x.executeSql('SELECT * FROM produtos   WHERE tb_codigo = ?' + wheres, [obter_valor_sessao('tabela_precos')], function(x, dados) {
            if (dados.rows.length) {
                var produtos = [];
                
                for (i = 0; i < dados.rows.length; i++) {
                    var dado = dados.rows.item(i);

                    var preco = aplicar_descontos_cabecalho(dado.ptp_preco);
                    	
                    preco = aplicar_acrescimo_condicao_pagamento(preco,dado.produto_codigo ); 
                    
                    
                    produtos.push({
                        label: dado.produto_codigo + "/" + dado.produto_derivacao + ' - ' + dado.produto_descricao + ' - ESTOQUE ATUAL: ' + number_format(dado.etq_quantidade_atual, 0, ',', '.') + ' - PREÇO: R$ ' + number_format(preco, 3, ',', '.'),
                        codigo: dado.produto_codigo,
                        derivacao: dado.produto_derivacao,
                        preco: preco,
                        familia: dado.produto_familia,
                        preco_tabela: dado.ptp_preco,
                        ipi: dado.produto_ipi,
                        descricao: dado.produto_descricao,
                        unidade_medida: dado.produto_unidade_medida,
                        //CUSTOM
                        segunda_unidade_medida: dado.produto_segunda_unidade_medida,
                        tipo_conversao: dado.tipo_conversao,
                        valor_conversao: dado.valor_conversao,
                        
                        local: dado.produto_locpad
                    });

                }

                $('#carregando_produtos').hide();

                buscar_produtos(produtos);

            } else {
                $('#erro_item').text('Nenhum produto encontrado para à filial (' + obter_valor_sessao('filial') + ').');
                $('#carregando_produtos').hide();
                $('#erro_item').show();
            }
        });
    });

}


/**
 * Metódo:		buscar_produtos
 * 
 * Descrição:	Função Utilizada para pesquisar os produtos pelo autocomplete e adicionar os valores nos campso quando o produto for selecionado
 * 
 * Data:			17/10/2013
 * Modificação:	17/10/2013
 * 
 * @access		public
 * @param		array 			var produtos		- Todos os produtos
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function buscar_produtos(produtos) {
	
    $('input[name=produto]').autocomplete({
        minLength: 3,
        source: produtos,
        position: {
            my: "left bottom",
            at: "left top",
            collision: "none"
        },
        select: function(event, ui) {
        	$("#informacoes_produto").hide();
        	$("#informacoes_produto").html();
        	
        	$('input[name=quantidade]').focus();        	
            $('input[name=produto]').val(ui.item.label);
            $('input[name=descricao_produto]').val(ui.item.descricao);
            $('input[name=codigo_produto]').val(ui.item.codigo);
            $('input[name=familia]').val(ui.item.familia);
            $('input[name=derivacao]').val(ui.item.derivacao);
            $('input[name=preco_tabela]').val(number_format(ui.item.preco_tabela, 3, ',', '.'));
            $('input[name=preco_unitario]').val(number_format(ui.item.preco, 3, ',', '.'));
            $('input[name=preco_venda]').val(number_format(ui.item.preco, 3, ',', '.'));
            $('input[name=percentual_modificador]').val('0,000');
            $('input[name=modificador]').val('');            
            $('input[name=ipi]').val(number_format(ui.item.ipi, 3, ',', ''));
            $('input[name=unidade_medida]').val(ui.item.unidade_medida);
            $('input[name=segunda_unidade_medida]').val(ui.item.segunda_unidade_medida);
            $('input[name=local]').val(ui.item.local);
            $('input[name=quantidade]').html('');
            //Para se adequar a Machs foi alterada a conversão para utilizar a derivação            
            var valor_conversao = 1;
            if(validar_digitos(ui.item.derivacao) && ui.item.derivacao > 0){   
            	valor_conversao = ui.item.derivacao;
            }
           
            
            $('input[name=tipo_conversao]').val('/');
            $('input[name=valor_conversao]').val(valor_conversao);
            
            
            if(ui.item.tipo_conversao == '/'){
            	var quantidade_fechada = valor_conversao / 1;            	
            }else{
            	var quantidade_fechada = valor_conversao * 1;            	
            }
            
           
         
                   
	       if(quantidade_fechada > 1){
	        	   $("#informacoes_produto").html('Produto múltiplo de '+ quantidade_fechada);
	        	   $("#informacoes_produto").show();
	       }
         
           
            
           calcular_total();

            // Bloquear campo quando selecionar cliente
            $('input[name=produto]').attr('readonly', 'readonly');
            $('#trocar_produto').show();
            
            return false;

        }
    });
}

/**
 * Metódo:		aplicar_descontos_cabecalho
 * 
 * Descrição:	Função Utilizada para aplicar os descontos do cabeçalho (Desconto do Cliente, Regra de Desconto)
 * 
 * Data:			23/10/2013
 * Modificação:	23/10/2013
 * 
 * @access		public
 * @param		string 					preco_produto		- Preço do Produto
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function aplicar_descontos_cabecalho(preco_produto) {
    // Aplicando Desconto do "Cliente" no preço do produto
    if (obter_valor_sessao('desconto_cliente') > 0) {
        var preco = preco_produto - (preco_produto * (obter_valor_sessao('desconto_cliente') / 100));
    } else {
        var preco = preco_produto;
    }

    // Aplicando Desconto da "Regra de desconto" no preço do produto
    if (obter_valor_sessao('regra_desconto') > 0) {
        var preco = preco - (preco * (obter_valor_sessao('regra_desconto') / 100));
    }

  
    
   return preco;
}


function aplicar_acrescimo_condicao_pagamento(preco, codigo_produto){
	var acrescimo_condicao_pagamento = obter_valor_sessao('acrescimo_condicao_pagamento') ? obter_valor_sessao('acrescimo_condicao_pagamento') : parseFloat(0);
	
	preco = parseFloat(preco);
	preco  += (preco * parseFloat(acrescimo_condicao_pagamento) /100);
	
	return preco;
}


/**
 * Metódo:		exibir_cabecalho
 * 
 * Descrição:	Função Utilizada para exibir os valores do cabeçalho
 * 
 * Data:			16/10/2013
 * Modificação:	16/10/2013
 * 
 * @access		public
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function exibir_cabecalho() {
    // ------
    // Obter descrição da FILIAL e adicionar no cabeçalho
    obter_descricao('filiais', 'razao_social', 'codigo', obter_valor_sessao('filial'), '.filial');

    // ------
    // Obter descrição da TIPO DE PEDIDO e adicionar no cabeçalho
    var tipo_pedido = obter_valor_sessao('tipo_pedido');
    if (tipo_pedido == 'N') {
        $('.tipo_pedido').html('Venda Normal');
    } else if (tipo_pedido == '*') {
        $('.tipo_pedido').html('Orçamento');
    } else if (tipo_pedido == 'B') {
        $('.tipo_pedido').html('Bonificação');
    } else {
        $('.tipo_pedido').html('N/A');
    }

    // ------
    // Obter descrição do CLIENTE e adicionar no cabeçalho
    if (obter_valor_sessao('descricao_prospect')) {
        $('.cabecalho_cliente_prospect').html('Prospect');
        $('.cliente').html(obter_valor_sessao('descricao_prospect'));
    }

    // Obter descrição do CLIENTE e adicionar no cabeçalho
    if (obter_valor_sessao('descricao_cliente')) {
        $('.cabecalho_cliente_prospect').html('Cliente');
        $('.cliente').html(obter_valor_sessao('descricao_cliente'));
    }

    // Obter descrição da TABELA DE PREÇOS e adicionar no cabeçalho
    obter_descricao('tabelas_preco', 'descricao', 'codigo', obter_valor_sessao('tabela_precos'), '.tabela_precos');

    // ------
    // Obter descrição da FORMA DE PAGAMENTO e adicionar no cabeçalho
    obter_descricao('formas_pagamento', 'descricao', 'codigo', obter_valor_sessao('forma_pagamento'), '.forma_pagamento');

    // ------
    // Obter descrição da CONDIÇÃO DE PAGAMENTO e adicionar no cabeçalho
    obter_descricao('condicoes_pagamento', 'descricao', 'codigo', obter_valor_sessao('condicao_pagamento'), '.condicao_pagamento');

    // ------
    // Obter DESCONTO do CLIENTE e adicionar no cabeçalho
    $('.desconto_cliente').html(number_format(obter_valor_sessao('desconto_cliente'), 3, ',', '.') + ' %');

    // ------
    // Obter REGRA DE DESCONTO e adicionar no cabeçalho
    $('.regra_desconto').html(number_format(obter_valor_sessao('regra_desconto'), 3, ',', '.') + ' %');
}

/**
 * Metódo:		obter_descricao
 * 
 * Descrição:	Função Utilizada para obter a descrição e codigo de um valor na sessão
 * 
 * Data:			16/10/2013
 * Modificação:	16/10/2013
 * 
 * @access		public
 * @param		string 					tabela				- A tabela do navegador que será utilizada para realizar o "SELECT" do retorna de descrição
 * @param		string 					campo_descricao		- O campo "Descrição" da "tabela" passada no primeiro parametro
 * @param		string 					campo_codigo		- O campo "Codigo" da "tabela" passada no primeiro parametro
 * @param		string 					codigo				- O Codigo que será usado para comparação no "WHERE"
 * @param		string 					classe				- Classe html (.classe) que será adiciona o valor do retorno no html
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_descricao(tabela, campo_descricao, campo_codigo, codigo, classe) {
    db.transaction(function(x) {
        x.executeSql(
            'SELECT ' + campo_descricao + ' AS descricao, ' + campo_codigo + ' AS codigo FROM ' + tabela + ' WHERE ' + campo_codigo + ' = ?', [codigo],
            function(x, dados) {

                if (dados.rows.length) {
                    var dado = dados.rows.item(0);

                    $(classe).html(dado.codigo + ' - ' + dado.descricao);
                } else {
                    $(classe).html('N/A');
                }
            }
        );
    });
}

/**
 * Metódo:		salvar_produto_sessao
 * 
 * Descrição:	Função Utilizada para salvar produtos adicionados
 * 
 * Data:			16/10/2013
 * Modificação:	16/10/2013
 * 
 * @access		public
 * @param		string 					produtos_paramentro		- produto serializado
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_produto_sessao(produtos_paramentro) {
    var produtos_paramentro = unserialize(produtos_paramentro);

    var sessao_pedido = [];

    // Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
    if (sessionStorage[sessionStorage['sessao_tipo']]) {
        sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
    }

    if (sessao_pedido['produtos']) {
        var sessao_produtos = sessao_pedido['produtos'];

        var produtos = merge_options(sessao_produtos, produtos_paramentro);
    } else {
        var produtos = produtos_paramentro;
    }

    salvar_sessao('produtos', produtos);

}

/**
 * Metódo:		salvar_produto_sessao
 * 
 * Descrição:	Função Utilizada para salvar produtos removidos
 * 
 * Data:		16/10/2013
 * Modificação:	16/10/2013
 * 
 * @access		public
 * @param		string 					produtos_paramentro		- produto serializado
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_produto_removido_sessao(produtos_paramentro) {
    var produtos_paramentro = unserialize(produtos_paramentro);

    var sessao_pedido = [];

    // Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
    if (sessionStorage[sessionStorage['sessao_tipo']]) {
        sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
    }

    if (sessao_pedido['produtos_removidos']) {
        var sessao_produtos = sessao_pedido['produtos_removidos'];

        var produtos = merge_options(sessao_produtos, produtos_paramentro);
    } else {
        var produtos = produtos_paramentro;
    }

    salvar_sessao('produtos_removidos', produtos);

}

/**
 * Metódo:		salvar_produtos_excluidos
 * 
 * Descrição:	Função Utilizada para salvar produtos (Sem os removidos)
 * 
 * Data:			20/10/2013
 * Modificação:	20/10/2013
 * 
 * @access		public
 * @param		string 					produtos_paramentro		- produto serializado
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function salvar_produtos_excluidos(produtos_paramentro) {
    var produtos_paramentro = unserialize(produtos_paramentro);

    var produtos = produtos_paramentro;

    salvar_sessao('produtos', produtos);
}