$(document).ready(function(){

	//----------------------------
	// Exibir botão impostos
	//----------------------------
	if(config.ws_url_impostos)
	{
		$('#botao_impostos').show();
	}
	//----------------------------
	// Exibir botão impostos
	//----------------------------
	
	//----------------------------
	// BOTÃO SIMULAR IMPOSTOS
	//----------------------------
	$('#botao_impostos').click(function(e){
		e.preventDefault();
		
		//Sessão do pedido
		var sessao_pedido = [];

		//Obter sessão do pedido
		if(sessionStorage[sessionStorage['sessao_tipo']])
		{
			sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		}
				
		$.ajax({
			url: config.ws_url_impostos,
			type: 'POST',			
			data: {
				retorno: json_encode(sessao_pedido)
			},
			beforeSend: function(){			
				$('#msgImpostos').addClass('info');
				$('#msgImpostos').html('Aguarde simulação impostos em andamento...');
				$('#botao_impostos').attr('disabled', 'disabled');
			},
			success: function(dados) {	
				//-------------------------------
				// ITENS
				//-------------------------------				
				//-------------------------------
				// ITENS
				//-------------------------------
				
				//-------------------------------
				// OBTER SESSÃO DOS PRODUTOS
				//-------------------------------
				var sessao_produtos 	= obter_produtos_sessao();
				if(sessao_produtos)
				{
					var total_produtos 		= Object.keys(sessao_produtos).length;
				}
				var indice = 0;
				//-------------------------------
				// OBTER SESSÃO DOS PRODUTOS
				//-------------------------------
				
				if(dados.sucesso)
				{
					//Verificar itens retornados pelo WS - impostos
					$.each(dados.retorno.produtos, function(keyPedido, imposto){						
							console.log('Produto: '+imposto.PRODUTO);
							console.log('TES: '+imposto.TES);
							console.log('CF: '+imposto.CF);
							console.log('ST: '+imposto.ST);
							console.log('IPI: '+imposto.IPI);
							console.log('ICMS: '+imposto.ICMS);	
							
							//Verificar itens na sessão do pedido
							$.each(sessao_produtos, function(key, produto){
								//Verificar se é o mesmo código
								if(imposto.PRODUTO == produto.codigo)
								{
									
									var codigo_produto 	= remover_zero_esquerda(produto.codigo);
									
									var produtos = new Array();
									produtos[codigo_produto] = new Array();
									produtos[codigo_produto]['codigo'] 						= produto.codigo;
									produtos[codigo_produto]['descricao'] 					= produto.descricao;
									produtos[codigo_produto]['preco_tabela'] 				= produto.preco_tabela;
									produtos[codigo_produto]['preco_unitario'] 				= produto.preco_unitario;
									produtos[codigo_produto]['preco_venda'] 				= produto.preco_venda
									produtos[codigo_produto]['quantidade'] 					= produto.quantidade;
									produtos[codigo_produto]['percentual_modificador'] 		= produto.percentual_modificador;
									produtos[codigo_produto]['modificador'] 				= produto.modificador;	
									produtos[codigo_produto]['unidade_medida'] 				= produto.unidade_medida;
									produtos[codigo_produto]['local'] 						= produto.local;
									
									//Impostos
									produtos[codigo_produto]['TES'] 			= imposto.TES;
									produtos[codigo_produto]['CF'] 				= imposto.CF;
									produtos[codigo_produto]['ST'] 				= imposto.ST;
									produtos[codigo_produto]['IPI'] 			= imposto.IPI;
									produtos[codigo_produto]['ICMS'] 			= imposto.ICMS;
									
									var produtos = serialize(produtos);
									
									salvar_produto_sessao(produtos);
									
									indice++;
									
									if(indice == total_produtos)
									{
										exibir_produtos_finalizar();
										$('#msgImpostos').addClass('success');
										$('#msgImpostos').html('Simulação de impostos realizado com sucesso.');
										$('#botao_impostos').removeAttr('disabled');
									}
								}
								//Verificar se é o mesmo código
							});
						
					});
				}
			},
			error: function(jqXHR, textStatus,errorThrown) {
				$('#msgImpostos').addClass('warning');
				$('#msgImpostos').html('Não foi possível realziar simulação de impostos.');
				$('#botao_impostos').removeAttr('disabled');				
			}
		});
	});
	
	//----------------------------
	// BOTÃO FINALIZAR PEDIDO
	//----------------------------	
	$('#botao_finalizar').click(function(e){
	
		e.preventDefault();
		
		var descricao_tipo_pedido = obter_descricao_pedido();
		
		
		confirmar('Tem certeza que deseja FINALIZAR o ' + descricao_tipo_pedido + ' ?', 
			function () {
				$(this).dialog('close');
				//---------------------------------------------------------
				//-----------------------------------------------------------------------------------------------
				
				salvar_pedido();
				
				//-----------------------------------------------------------------------------------------------
				//---------------------------------------------------------
				$("#confirmar_dialog").remove();
				
			}
		);
	
	});

});

function finalizar()
{	
	exibir_produtos_finalizar();
	exibir_outras_informacoes();
	exibirTabelaParcelas();
}

function exibir_produtos_finalizar()
{
	var sessao_produtos = obter_produtos_sessao();
	
	if(obter_valor_sessao('tabela_precos') != obter_valor_sessao('tabela_preco_sugerida')) {
		var acrescimo_condicao_pagamento = obter_valor_sessao('acrescimo_condicao_pagamento') ? obter_valor_sessao('acrescimo_condicao_pagamento') : parseFloat(0);
	}
	
	if(sessao_produtos)
	{
		$('.itens_pedido_finalizar').empty();
		
		var total_preco_unitario 	= 0;
		var total_modificador 		= 0;
		var total_preco_venda 		= 0;
		var total_quantidade		= 0;
		var total					= 0;
		var total_ipi				= 0;
		var total_icms 				= 0;
		var total_st 				= 0;
		var total_geral				= 0;
		
		$.each(sessao_produtos, function(key, objeto){

			

			var html = '';
				html += '<td align="center">' + objeto.codigo + '</td>';
				html += '<td>' + objeto.descricao + '</td>';
				html += '<td align="right">' + number_format(objeto.preco_unitario, 3, ',', '.') + '<input type="hidden" name="editar_preco_unitario_' + objeto.codigo + '" value="' + number_format(objeto.preco_unitario, 3, ',', '.') + '" /></td>';
				html += '<td align="right">';
				
				var percentual_modificador = parseFloat(objeto.percentual_modificador);
				var preco_venda_final = parseFloat(objeto.preco_venda);
				
				if(objeto.modificador == 'A') {					
					html += '+';										
				} else if(objeto.modificador == 'D') {
					html += '-';					
				} 
				
				
				
				var percentual_modificador = parseFloat(objeto.percentual_modificador);
				var preco_venda_final = parseFloat(objeto.preco_venda);
				
				
				
				
				
				//percentual_modificador =  (objeto.preco_venda * 100 / objeto.preco_unitario) - 100; 
				
				console.log('percentual_modificador: '+percentual_modificador);
				
				
				
				
				html += number_format(percentual_modificador, 3, ',', '.') + '</td>';
				html += '<td align="right">' + number_format(preco_venda_final, 3, ',', '.') + '</td>';
				html += '<td align="right">' + objeto.quantidade + '</td>';
				
				//Total sem impostos
				html += '<td align="right">' + number_format(preco_venda_final * objeto.quantidade, 3, ',', '.')  + '</td>';
				
				//Impostos
				html += '<td align="right">' + (objeto.IPI ? number_format(objeto.IPI, 3, ',', '.') : '0,000') + '</td>';
				html += '<td align="right">' + (objeto.ICMS ? number_format(objeto.ICMS, 3, ',', '.') : '0,000') + '</td>';
				html += '<td align="right">' + (objeto.ST ? number_format(objeto.ST, 3, ',', '.') : '0,000') + '</td>';			
				
				//Total com Impostos
				var total_item_impostos = (preco_venda_final * objeto.quantidade) + (parseFloat(objeto.IPI) + parseFloat(objeto.ST));
				html += '<td align="right">' + number_format(total_item_impostos, 3, ',', '.')  + '</td>';	
				
				$('.itens_pedido_finalizar').append('<tr class="produto_' + objeto.codigo + '">' + html + '</tr>');
			
			// variaveis para os TOTAIS
			total_preco_unitario 	+= parseFloat(objeto.preco_unitario *  objeto.quantidade);
			total_preco_venda		+= parseFloat(preco_venda_final);
			total_quantidade		+= parseFloat(objeto.quantidade);
			total_ipi				+= parseFloat(objeto.IPI);
			total_icms				+= parseFloat(objeto.ICMS);
			total_st				+= parseFloat(objeto.ST);
			total					+= parseFloat(preco_venda_final * objeto.quantidade);
			total_geral				+= parseFloat(preco_venda_final * objeto.quantidade);
			total_modificador 		+= percentual_modificador;
			
		});
		
		total_preco_unitario = parseFloat(total_preco_unitario);
		total_modificador =  (total * 100 / total_preco_unitario) -100 ;
		
		//total_ipi				= (total_geral - total) * 100 / total_geral;
		console.log('total_preco_venda: '+total_preco_unitario+' total_preco_unitario: '+total);
		var descricao_tipo_pedido = obter_descricao_pedido('upper');
		
		var total_final = total_geral + (total_ipi + total_st);
		
		var html = '';
		html += '<td colspan="2">TOTAIS DE PRODUTOS</td>';
		html += '<td align="right">' + number_format(total_preco_unitario, 3, ',', '.') + '</td>';
		html += '<td align="right">' + number_format(total_modificador, 3, ',', '.') + '</td>';
		html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
		html += '<td align="right">' + total_quantidade + '</td>';
		
		//Total sem impostos
		html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
		
		//Impostos
		html += '<td align="right">' + number_format(total_ipi, 3, ',', '.')  + '</td>';
		html += '<td align="right">' + number_format(total_icms, 3, ',', '.')  + '</td>';
		html += '<td align="right">' +  number_format(total_st, 3, ',', '.')  + '</td>';
		
		//Total com impostos
		html += '<td align="right">' + number_format(total_final, 3, ',', '.') + '</td>';
		
		$('.itens_pedido_finalizar').append('<tr class="novo_grid_rodape">' + html + '</tr>');
		
		alterarCabecalhoListagem('#itens_pedido_finalizar_tabela');
		calcularValorParcela(total_final);
	}
}

function exibir_outras_informacoes()
{
	var sessao_pedido = [];
	
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	$('.pedido_cliente').html(sessao_pedido.pedido_cliente ? sessao_pedido.pedido_cliente : 'N/A');
	$('.data_entrega').html(sessao_pedido.data_entrega ? sessao_pedido.data_entrega : 'N/A');
	obter_evento(sessao_pedido.evento);
	//$('.tipo_frete').html(sessao_pedido.tipo_frete == 'F' ? 'FOB' : 'CIF');
	$('.tipo_frete').html(sessao_pedido.tipo_frete);
	obter_transportadora(sessao_pedido.codigo_transportadora);
	
	$('.cliente_entrega').html(sessao_pedido.descricao_cliente_entrega ? sessao_pedido.descricao_cliente_entrega : 'N/A');

	$('.mennota').html(sessao_pedido.mennota ? wordwrap(sessao_pedido.mennota, 35, " ", 1 ) : 'N/A');
	$('.observacao_comercial').html(sessao_pedido.observacao_comercial ? wordwrap(sessao_pedido.observacao_comercial, 35, " ", 1) : 'N/A');

}

function obter_evento(codigo)
{
	if(codigo)
	{
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM eventos WHERE id = ?', [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
					
						$('.evento').html(dado.id + ' - ' + dado.nome);
					}
					else
					{
						$('.evento').html('N/A');
					}
					
				}
			);
		});
	}
	else
	{
		$('.evento').html('N/A');
	}
}

function obter_transportadora(codigo)
{
	if(codigo)
	{
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM transportadoras WHERE codigo = ?', [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						$('.transportadora').html(dado.codigo + ' - ' + dado.nome);
					}
					else
					{
						$('.transportadora').html('N/A');
					}
				}
			);
		});
	}
	else
	{
		$('.transportadora').html('N/A');
	}
}

function salvar_pedido()
{
	var sessao_pedido = [];
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	//Definindo Tabela de pedidos ou or�amentos
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
	{
		var tabela 	= 'orcamentos';
		var tipo 	= 'O';
	}
	else
	{
		var tabela 	= 'pedidos_pendentes';
		var tipo 	= 'P';
	}

	var sessao_produtos = sessao_pedido.produtos;
	
	if(sessao_produtos)
	{
		var total_produtos 		= Object.keys(sessao_produtos).length;
	}
	else
	{
		var total_produtos 		= 0;
	}
	
	//-----------------
	var dados_cliente 							= sessao_pedido.dados_cliente;
	var dados_prospect							= sessao_pedido.dados_prospect;
	//var dados_transportadoras 					= sessao_pedido.dados_transportadoras;
	var dados_condicao_pagamento				= sessao_pedido.dados_condicao_pagamento;
	var dados_forma_pagamento					= sessao_pedido.dados_forma_pagamento;
	var dados_acrescimo_condicao_pagamento		= sessao_pedido.acrescimo_condicao_pagamento;
	var numero_itens 				= 1;
	
	
	 if(obter_valor_sessao('total_titulos_vencidos') > 0 ){     	
			sessao_pedido.status_pedido = 'B';
			console.log(sessao_pedido.motivo_bloqueio+' - '+!!sessao_pedido.motivo_bloqueio +'-'+!!!sessao_pedido.motivo_bloqueio)
			
			if(!!sessao_pedido.motivo_bloqueio){			
				sessao_pedido.motivo_bloqueio  += '<br/> Cliente com títulos em atraso.';
			}else{
				sessao_pedido.motivo_bloqueio  = '<br/> Cliente com títulos em atraso.';
			}
			
	}
	 
	 if(obter_valor_sessao('tipo_pedido') == 'B' ){     	
			sessao_pedido.status_pedido = 'B';
			if(!!sessao_pedido.motivo_bloqueio){			
				sessao_pedido.motivo_bloqueio  += '<br/> Pedido de Bonificação.';
			}else{
				sessao_pedido.motivo_bloqueio  = '<br/> Pedido de Bonificação.';
			}
			
	}
     
	 for(var i in tabelas_precos_sistema) {
  		if (tabelas_precos_sistema[i].codigo == sessao_pedido.tabela_precos) {
      		if (tabelas_precos_sistema[i].irregular == 'S') {
      			sessao_pedido.status_pedido = 'B';
      			if(!!sessao_pedido.motivo_bloqueio){			
    				sessao_pedido.motivo_bloqueio  += '<br/> Tabela de Preços Irregular.';
    			}else{
    				sessao_pedido.motivo_bloqueio  = '<br/> Tabela de Preços Irregular.';
    			}
      		}
      	}
     }
     

	if(total_produtos > 0)
	{
	
		var editar = sessao_pedido.editar;
	
		if(editar)
		{
			var id_pedido 		= sessao_pedido.id_pedido;
		}
		else
		{
			var id_pedido 		= uniqid();
		}
		
		if(sessao_pedido.converter_pedido_orcamento)
		{
			id_pedido 		= sessao_pedido.id_pedido;
		}
		
		var data_emissao 	= date('Ymd');
		var time_emissao 	= time();
		var produtos		= [];
	
		$.each(sessao_produtos, function(key, item){
			var campos 			= [];
			var interrogacoes  	= [];
			var valores		 	= [];
			
			campos.push('converter_pedido_orcamento');					interrogacoes.push('?');
			
			campos.push('empresa');										interrogacoes.push('?');
			campos.push('filial');										interrogacoes.push('?');
			campos.push('pedido_filial');								interrogacoes.push('?');
			campos.push('pedido_numero_item');							interrogacoes.push('?');
			campos.push('pedido_id_pedido'); 							interrogacoes.push('?');
			campos.push('pedido_data_emissao'); 						interrogacoes.push('?');
			campos.push('pedido_time_emissao'); 						interrogacoes.push('?');
			campos.push('pedido_id_usuario'); 							interrogacoes.push('?');
			campos.push('pedido_codigo_representante'); 				interrogacoes.push('?');
			campos.push('pedido_tabela_precos');						interrogacoes.push('?');
			campos.push('pedido_id_feira');								interrogacoes.push('?');
			campos.push('pedido_codigo_cliente'); 						interrogacoes.push('?');
			campos.push('pedido_loja_cliente');							interrogacoes.push('?');
			campos.push('pedido_forma_pagamento');						interrogacoes.push('?');
			campos.push('pedido_acrescimo_condicao_pagamento');			interrogacoes.push('?');
            campos.push('pedido_condicao_pagamento');                   interrogacoes.push('?');
			//campos.push('pedido_tipo_frete');							interrogacoes.push('?');
			//campos.push('pedido_codigo_transportadora');				interrogacoes.push('?');
			campos.push('pedido_tipo_venda');							interrogacoes.push('?');
			campos.push('pedido_mensagem_nota');						interrogacoes.push('?');
			campos.push('pedido_observacao_comercial');					interrogacoes.push('?');			
			campos.push('pedido_cliente_entrega');						interrogacoes.push('?');
			campos.push('pedido_loja_entrega');							interrogacoes.push('?');
			campos.push('pedido_data_entrega');							interrogacoes.push('?');
			campos.push('pedido_pedido_cliente');						interrogacoes.push('?');

			campos.push('pedido_latitude');								interrogacoes.push('?');
			campos.push('pedido_longitude');							interrogacoes.push('?');
			campos.push('pedido_versao');								interrogacoes.push('?');
			
			//-- Cliente/Prospect
			if(dados_prospect)
			{
				campos.push('pedido_id_prospects');							interrogacoes.push('?');
				campos.push('prospect_codigo');								interrogacoes.push('?');				
				campos.push('prospect_codigo_loja');						interrogacoes.push('?');
				campos.push('prospect_nome');								interrogacoes.push('?');
				campos.push('prospect_cgc');								interrogacoes.push('?');
				
				campos.push('prospect_nome_contato');						interrogacoes.push('?');
				campos.push('prospect_cargo_contato');						interrogacoes.push('?');
				campos.push('prospect_email_contato');						interrogacoes.push('?');
				campos.push('prospect_telefone_contato');					interrogacoes.push('?');
				
				campos.push('prospect_endereco');							interrogacoes.push('?');
				campos.push('prospect_bairro');								interrogacoes.push('?');
				campos.push('prospect_cep');								interrogacoes.push('?');
				campos.push('prospect_codigo_municipio');					interrogacoes.push('?');
				campos.push('prospect_estado');								interrogacoes.push('?');
				campos.push('prospect_telefone');							interrogacoes.push('?');
				campos.push('prospect_email');								interrogacoes.push('?');
			}
			else //Cliente
			{
				campos.push('cliente_codigo');								interrogacoes.push('?');				
				campos.push('cliente_loja');								interrogacoes.push('?');
				campos.push('cliente_nome');								interrogacoes.push('?');
				campos.push('cliente_cpf');									interrogacoes.push('?');
				campos.push('cliente_pessoa_contato');						interrogacoes.push('?');
				campos.push('cliente_endereco');							interrogacoes.push('?');
				campos.push('cliente_bairro');								interrogacoes.push('?');
				campos.push('cliente_cep');									interrogacoes.push('?');
				campos.push('cliente_cidade');								interrogacoes.push('?');
				campos.push('cliente_estado');								interrogacoes.push('?');
				campos.push('cliente_telefone');							interrogacoes.push('?');
				campos.push('cliente_email');								interrogacoes.push('?');
				campos.push('pedido_tipo_cliente');							interrogacoes.push('?');
				campos.push('pedido_desconto1');							interrogacoes.push('?');
			}
			
			//Transportadoras
			//campos.push('transportadora_codigo');						interrogacoes.push('?');		
			//campos.push('transportadora_nome');							interrogacoes.push('?');
			
			//Condições pagamento
			campos.push('condicoes_pagamento_codigo');					interrogacoes.push('?');
			campos.push('condicoes_pagamento_descricao');				interrogacoes.push('?');
			
			//Formas pagamento
			campos.push('forma_pagamento_codigo');						interrogacoes.push('?');
			campos.push('forma_pagamento_descricao');					interrogacoes.push('?');
			
			//Produtos
			campos.push('pedido_codigo_produto');						interrogacoes.push('?');
			campos.push('pedido_preco_unitario');						interrogacoes.push('?');
			campos.push('pedido_preco_venda');							interrogacoes.push('?');
			campos.push('pedido_quantidade');							interrogacoes.push('?');
			campos.push('pedido_percentual_modificador');				interrogacoes.push('?');
			campos.push('pedido_modificador');	        				interrogacoes.push('?');
			campos.push('pedido_comissao');								interrogacoes.push('?');
			campos.push('produto_descricao');							interrogacoes.push('?');
			campos.push('produto_ipi');									interrogacoes.push('?');
			campos.push('pedido_unidade_medida');						interrogacoes.push('?');
			campos.push('pedido_local');								interrogacoes.push('?');
			campos.push('produto_familia');								interrogacoes.push('?');
			campos.push('produto_derivacao');							interrogacoes.push('?');
			campos.push('produto_unidade_medida');						interrogacoes.push('?');
			campos.push('produto_segunda_unidade_medida');				interrogacoes.push('?');
			
			//Impostos
			campos.push('pedido_tipo_entrada_saida');					interrogacoes.push('?');
			campos.push('pedido_codigo_fiscal');						interrogacoes.push('?');
			campos.push('pedido_st');									interrogacoes.push('?');
			campos.push('pedido_ipi');									interrogacoes.push('?');
			campos.push('pedido_icms');									interrogacoes.push('?');
			
			// Valores Fixos
			campos.push('pedido_status');								interrogacoes.push('?');
			campos.push('pedido_motivo_bloqueio');					interrogacoes.push('?');
			campos.push('pedido_codigo_empresa');						interrogacoes.push('?');
			
			//------------------------
			
			valores.push(sessao_pedido.converter_pedido_orcamento);
			
			valores.push(info.empresa);
			valores.push(sessao_pedido.filial);
			valores.push(sessao_pedido.filial);
			valores.push(String(numero_itens));
			valores.push(id_pedido);
			valores.push(data_emissao);
			valores.push(time_emissao);
			valores.push(info.id_rep);
			valores.push(info.cod_rep);
			valores.push(sessao_pedido.tabela_precos);
			valores.push(sessao_pedido.evento);
			valores.push(sessao_pedido.codigo_cliente);
			valores.push(sessao_pedido.loja_cliente);
			valores.push(sessao_pedido.forma_pagamento);
			valores.push(sessao_pedido.acrescimo_condicao_pagamento);
            valores.push(sessao_pedido.condicao_pagamento);
			//valores.push(sessao_pedido.tipo_frete);
			//valores.push(sessao_pedido.codigo_transportadora);
			valores.push(sessao_pedido.tipo_pedido);
			
			valores.push(utf8_encode(sessao_pedido.mennota));
			valores.push(utf8_encode(sessao_pedido.observacao_comercial));			
			
			valores.push(sessao_pedido.codigo_cliente_entrega);
			valores.push(sessao_pedido.loja_cliente_entrega);
			valores.push(data_normal2protheus(sessao_pedido.data_entrega));
			valores.push(sessao_pedido.pedido_cliente);
			
			//Posição da venda
			valores.push(localStorage.getItem('gps_latitude'))
			valores.push(localStorage.getItem('gps_longitude'))
			//Versão			
			valores.push(localStorage.getItem('versao'));
			
			//-- Cliente/Prospect
			if(dados_prospect)
			{
				valores.push(dados_prospect.cgc);
				valores.push(dados_prospect.codigo);
				valores.push(dados_prospect.codigo_loja);
				valores.push(dados_prospect.nome);
				valores.push(dados_prospect.cgc);
				
				valores.push(dados_prospect.nome_contato);
				valores.push(dados_prospect.cargo_contato);
				valores.push(dados_prospect.email_contato);
				valores.push(dados_prospect.telefone_contato);
				
				valores.push(dados_prospect.endereco);
				valores.push(dados_prospect.bairro);
				valores.push(dados_prospect.cep);
				valores.push(dados_prospect.codigo_municipio);
				valores.push(dados_prospect.estado);
				valores.push(dados_prospect.telefone);
				valores.push(dados_prospect.email);
			}
			else //Cliente
			{
				valores.push(dados_cliente.codigo);
				valores.push(dados_cliente.loja);
				valores.push(dados_cliente.nome);
				valores.push(dados_cliente.cpf);
				valores.push(dados_cliente.pessoa_contato);
				valores.push(dados_cliente.endereco);
				valores.push(dados_cliente.bairro);
				valores.push(dados_cliente.cep);
				valores.push(dados_cliente.cidade);
				valores.push(dados_cliente.estado);
				valores.push(dados_cliente.telefone);
				valores.push(dados_cliente.email);
				valores.push(dados_cliente.tipo);
				valores.push(sessao_pedido.desconto_cliente);
			}
			
			//Transportadoras
			//valores.push(dados_transportadoras.codigo);
			//valores.push(dados_transportadoras.nome);
			
			//Condições Pagamento
			valores.push(dados_condicao_pagamento.codigo);
			valores.push(dados_condicao_pagamento.descricao);
			
			//Formas Pagamento
			valores.push(dados_forma_pagamento.codigo);
			valores.push(dados_forma_pagamento.descricao);
			
			//Produtos
			valores.push(item.codigo);
			
			
			
			if(item.preco_unitario)
			{
				valores.push(item.preco_unitario);
			}
			else
			{
				valores.push(item.preco_tabela);
				
			}			
			valores.push(item.preco_venda);
			valores.push(item.quantidade);
			valores.push(item.percentual_modificador);
			valores.push(item.modificador);
			valores.push(item.percentual_comissao);
			valores.push(item.descricao);
			valores.push(item.ipi);
			valores.push(item.unidade_medida);
			valores.push(item.local);
			valores.push(item.familia);
			valores.push(item.derivacao);
			valores.push(item.unidade_medida);
			valores.push(item.segunda_unidade_medida);
			
			//Impostos
			valores.push(item.TES);
			valores.push(item.CF);
			valores.push(item.ST);
			valores.push(item.IPI);
			valores.push(item.ICMS);
		
			// Valores Fixos	
			valores.push(sessao_pedido.status_pedido ? sessao_pedido.status_pedido : 'L');
			valores.push(sessao_pedido.motivo_bloqueio);
			valores.push(info.empresa);
			
			//------------------------------------
			//---- PRODUTOS
			produtos.push(item.codigo);
			//------------------------------------
		
			if(editar)
			{
			
				db.transaction(function(x) {
					x.executeSql('UPDATE ' + tabela + ' SET ' + campos.join(' = ?, ') + ' = ? WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto = "' + item.codigo + '"', valores, function(e, resultado){
						if(!resultado.rowsAffected)
						{							
							x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
								
								total_produtos--;
								
								if(total_produtos == 0)
								{
									finalizar_pedido_editado(id_pedido, sessao_pedido.filial, produtos);
								}
							},
							function (e, t){
								
							});
							
						}
						else
						{
							total_produtos--;
							
							if(total_produtos == 0)
							{
								finalizar_pedido_editado(id_pedido, sessao_pedido.filial, produtos);
							}
						}
					},
					function (e, t){
						
					});
				});

			}
			else
			{
			
				db.transaction(function(x) {
					x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
						
						
						total_produtos--;
						
						if(total_produtos == 0)
						{
							// Se o sistema estiver transformando orçamento em pedido, exluir orçamento apos 
							if(sessao_pedido.converter_pedido_orcamento)
							{
								x.executeSql('DELETE FROM orcamentos WHERE pedido_id_pedido = "' + id_pedido + '"', [], function(){
									
									exportar_parcelas(sessao_pedido, id_pedido, function(){
										// Apagando pedido da sessao
										location.href="pedidos_espelho.html#" + id_pedido + '|' + sessao_pedido.filial + '|' + tipo;
										sessionStorage[sessionStorage['sessao_tipo']] = '';
									});
								
								});
							}
							else
							{
								exportar_parcelas(sessao_pedido, id_pedido, function(){
									// Apagando pedido da sessao
									location.href="pedidos_espelho.html#" + id_pedido + '|' + sessao_pedido.filial + '|' + tipo;
									sessionStorage[sessionStorage['sessao_tipo']] = '';
								});
							}
						}
					},
					function (e, t){
						
					});
				});
				
			}

			numero_itens++;
		
		});
	}
}

function exportar_parcelas(sessao_pedido, id_pedido, callback) {
	var where = "WHERE pedido_parcela_id_pedido = '" + id_pedido + "'";
	var parcelas = sessao_pedido.parcelas;
	var total = Object.keys(parcelas).length;
	var i = 0;
	db.transaction(function(x) {
		x.executeSql('DELETE FROM pedido_parcela ' + where, [], function(){
			$.each(parcelas, function(index, parcela) {
				var valores = [];
				valores.push(id_pedido);
				valores.push(parseInt(parcela.numero));
				valores.push(parcela.valor);
				valores.push(parcela.data);
				x.executeSql('INSERT INTO pedido_parcela (pedido_parcela_id_pedido, pedido_parcela_numero_parcela, pedido_parcela_valor_parcela, pedido_parcela_data_vencimento, exportado, editado, erro) VALUES (?, ?, ?, ?, null, 0, 0)', valores, function(){
					i++;
					if(total == i) {
						callback();
					}
				});
			});
		});
	});
}

function finalizar_pedido_editado(id_pedido, filial, produtos)
{

	//Definindo Tabela de pedidos ou orçamentos
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
	{
		var tabela = 'orcamentos';
		var tipo = 'O';
	}
	else
	{
		var tabela = 'pedidos_pendentes';
		var tipo = 'P';
	}

	db.transaction(function(x) {
		var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto NOT IN ("' + produtos.join('", "') + '")', [], function(){
			exportar_parcelas(sessao_pedido, id_pedido, function(){
				// Apagando pedido da sessao
				location.href="pedidos_espelho.html#" + id_pedido + '|' + filial + '|' + tipo;
				sessionStorage[sessionStorage['sessao_tipo']] = '';
			});
		});
	});							
}
