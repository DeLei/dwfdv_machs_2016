/**
* Metódo:		sincronizar_modulo
* 
* Descrição:	Função Utilizada para chamar o modulo que será sincronizado
* 
* Data:			17/09/2013
* Modificação:	17/09/2013
* 
* @access		public
* @param		string 		$modulo					- Modulo que sera sincronizado
* @param		string 		$descricao_modulo		- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_modulo			- Proximo modulo que ser asincronizado quando finalizar o primeiro
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_modulo(modulo, descricao_modulo, proxima_modulo, parametros)
{
	
	// Salvando informações das sincronizações
	db.transaction(function(x) {
		x.executeSql('INSERT INTO sincronizacoes (timestamp, modulo, sucesso) VALUES (?, ?, ?)', [time(), modulo, 0]);
	});

	// Carregando
	$('#carregando').show();
	$('#btnCancelar').show();
	$('#btnSincronizar').hide();
	$('#carregando').find('span:first').html('Sincronizando ' + descricao_modulo + '…');

	//----------------------------------------------------------------------------------------------
	// Chamando o total de pacotes via ajax, e depois sincronizar dados do pacote
	//---------------------------------------------------------
	var ajax = $.ajax({
		url: localStorage.getItem('caminho_local') + modulo + '_total.json',
		success: function(dados) {
			
			var total_pacotes = dados.total;
			
			//-----------
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM ' + modulo, [],
				function(x, dados){
				
					// A tabela existe, entao apaga os dados e contia a sincronização
				
					
					//1º Tentamos deletar os dados exportados das tabelas (Como somente as tabelas de exportação tem os campos "exportado", ira ocorrer um erro de sql)
					//2ª Se ocorrer um erro de SQL, vamos deletar os dados das tabelas que não existem os campos "exportado" 
					
					var where = '';
					
					if(modulo == 'pedidos_pendentes') {
						//Exclui somente os pedidos que já estão na SC5
						where = ' AND pedido_data_emissao <= "' + date('Ymd', strtotime('-15 days')) + '"';
					}
					
					x.executeSql('DELETE FROM ' + modulo + ' WHERE exportado = \'1\'' + where, [],
					function(){
						//console.log('--==-- Apagando dados antigos do modulo ' + modulo + ' (EXPORTADO)');
						sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
					},
					function(){
						x.executeSql('DELETE FROM ' + modulo, [], function(){
							//console.log('--==-- Apagando dados antigos do modulo ' + modulo);
							sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
						});
					});
					
				},
				function(){ // Erro de SQL, irá continuar a sincronização normalmente
					sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
				});
			});
			
		},
	    error: function(XMLHttpRequest, textStatus, errorThrown){
		       
	    	mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.'+
	    			"\n"+
	    			"Se o problema persistir entre em contato com seu gestor comercial."
	    	);
	    	
	    	
	    }
	
	});
	
	//----------------------------------------------------------
	//----------------------------------------------------------------------------------------------
}

/**
* Metódo:		verificar_campos
* 
* Descrição: 		Verifica se os campos retornados pelo WS existem no banco de dados local
* 					se não existir gerar SQL ALTER e cria os campos todos para tipo TEXT.
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir)
{
	db.transaction(function(x) {
		
		var total_itens = dados.length;
		var item_atual 	= 0;
		var campos 		= [];
		
		//--------------------
		$.each(dados, function(i, objeto) {
			
			
			//--------------------
			$.each(objeto, function(indice, valor) {
				
				if(indice != 'rownum') 
				{
					campos.push(indice);
				}
				
			});
			//--------------------
			
			return false;
		});
		
		
		//console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
		//console.log('TABELA: '+ tabela);
		//console.log('CAMPOS: '+ campos.join(', '));
		//console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
		
		
		x.executeSql('SELECT ' + campos.join(', ') + ' FROM ' + tabela, [], function(){
			if(inserir)
			{
				inserir_dados(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao);
			}
			else
			{
				finalizar_sincronizacao(tabela, proxima_funcao);
			}
			
		},
		function(xe, erro)
		{
			if(erro.code == 5)
			{
				var mensagem_erro = erro.message;
				var retorno = mensagem_erro.split(":");
				var campo = trim(retorno[1]);
				campo =  str_replace(')','',campo);
				if(campo)
				{
					//console.log(' -- ERRO COLUNA -- ');
					//console.log('Coluna não existe: ' + campo);
					
					x.executeSql('ALTER TABLE ' + tabela + ' ADD ' + campo + ' TEXT', [], function () {
						//console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Tabela alterada com sucesso.');						
						verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir);

						
					}, function(xy, error){
						//console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Não foi possível alterar: ' + error.message);
					});
					
					//console.log(' -- FIM -- ');
					
				}
				else
				{
					//console.log(erro);
				}
			}
			
		});

	});
}

/**
* Metódo:		inserir_dados
* 
* Descrição:	Função Utilizada para inserir os dados json na tabela SQLLITE
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function inserir_dados(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao)
{
	
	
	var total_itens = dados.length;
	var item_atual = 0;
	
	

	db.transaction(function(x) {
	
		$.each(dados, function(i, objeto) {
			var campos 			= [];
			var interrogacoes  	= [];
			var valores		 	= [];
		
			
		
			$.each(objeto, function(indice, valor) {
				
				if(indice != 'rownum') //o campo rownum, vem automatico por conta do LIMIT no sqlsrv, entao por padrao, vamos remover esse retorno
				{
					campos.push(indice);
					interrogacoes.push('?');
					valores.push(valor);
				}
				
			});
						
			x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(a1, a2){
	
				item_atual++;
				
				
				var percontagem_item = percentagem_pacotes / total_itens;
				
				$( "#progressbar" ).progressbar({value: parseFloat($('#progressbar').attr('aria-valuenow')) + percontagem_item});

				if(total_itens == item_atual)
				{
					//console.log('Pacote ' + pacote + ' foi finalizado!');
					
					$( "#progressbar" ).progressbar({value: round(percentagem_pacotes * pacote)});
				
					if(pacote < total_pacotes)
					{
						pacote++;
						sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao);
					}
					else
					{
						finalizar_sincronizacao(tabela, proxima_funcao);
					}
				}
				
			},
			function(x, erro){
				//console.log(' ----- ');
				//console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
				//console.log(erro);
				//console.log('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ") VALUES ('" + valores.join("', '") + "');");
				//console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
				//console.log(' ----- ');
			});
			
		});
		

	});

	
}


/**
* Metódo:		sincronizar_dados
* 
* Descrição:	Função Utilizada para salvar dados do web service no banco do navegador
* 
* Data:			17/09/2013
* Modificação:	17/09/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao, parametros)
{

	var percentagem_pacotes = 100 / total_pacotes; //Calculo de porcentagem por pacote
	if(pacote == 1)
	{
		$( "#progressbar" ).progressbar({value: 0});
	}
	
	//Mensagem no console
	//console.log('Sincronizando ' + tabela + ' | Pacote = ' + pacote + ' | Total de Pacotes = ' + total_pacotes);
	
	var ajax = $.ajax({
		url: localStorage.getItem('caminho_local') + tabela + '_' + pacote + '.json',
		data: parametros,
		success: function(dados) {
			
			// Criar Tabela
			criar_tabela(tabela, dados, true);
			
			// Salvar Dados
			if(verificar_dados(dados))
			{
				
				verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, true);
			
				
			}
			else
			{
				verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, false);				
			}
			
		}
	});
	
	ajax.fail(function () {
		apprise('A sincronização não foi totalmente concluída. Você deseja tentar novamente?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				window.location = 'sincronizar.html';
			}
			else
			{
				window.location = 'index.html';
			}
		});
	});
	
}

/**
* Metódo:		finalizar_sincronizacao
* 
* Descrição:	Função Utilizada para finalizar um pacote, e chamar a proxima funcao
* 
* Data:			22/09/2013
* Modificação:	22/09/2013
* 
* @access		public
* @param		string 		$tabela							- Tabela que esta sendo sincronizada para ser exibida no console
* @param		string 		$proxima_funcao					- Proxima funcao que sera chamada quando finalziar a sincronização de um pacote
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function finalizar_sincronizacao(tabela, proxima_funcao)
{
	var modulos = new Object();	
		modulos['empresas'] 							= 'Importação de Empresas';
		modulos['filiais'] 								= 'Importação de Filiais';
		modulos['clientes'] 							= 'Importação de Clientes';
		modulos['orcamentos'] 							= 'Importação de Orçamentos';
		modulos['exportacao_prospects'] 				= 'Exportação de Prospects';
		modulos['exportacao_orcamentos'] 				= 'Exportação de Orçamentos';
		modulos['prospects'] 							= 'Importação de Prospects';		
		modulos['exportacao_historico_clientes'] 		= 'Exportação de Históricos de Clientes';
		modulos['historico_clientes'] 					= 'Importação de Históricos de Clientes';
		modulos['exportacao_historico_prospects'] 		= 'Exportação de Históricos de Prospects';		
		modulos['historico_prospects'] 					= 'Importação de Históricos de Prospects';
		modulos['orcamentos'] 							= 'Importação de Orçamentos';
		modulos['exportacao_orcamentos'] 				= 'Exportação de Orçamentos';
		modulos['tabelas_preco'] 						= 'Importação de Tabelas de Preço';
		modulos['representante_tabela_preco'] 			= 'Importação de Representante X Tabelas de Preço';
		modulos['produtos'] 							= 'Importação de Produtos';
		modulos['formas_pagamento'] 					= 'Importação de Formas de Pagamento';
		modulos['condicoes_pagamento'] 					= 'Importação de Condições de Pagamento';
		modulos['pedido_parcela'] 						= 'Importação das Parcelas dos Pedidos';
		modulos['prazo_parcela'] 						= 'Importação dos Prazos das Parcelas';
		modulos['municipios'] 							= 'Importação de Municípios';
		modulos['pedidos_processados'] 					= 'Importação de Pedidos Processados';
		modulos['pedidos_pendentes'] 					= 'Importação de Pedidos Pendentes';
		modulos['exportacao_pedidos_pendentes'] 		= 'Exportação de Pedidos';
		modulos['noticias'] 							= 'Importação de Notícias';
		modulos['derivacoes_produtos'] 					= 'Importação de Derivação de Produtos';
		modulos['exportacao_pendencias_mensagens'] 		= 'Exportação de Pendências Mensagens';
		modulos['exportacao_pendencias'] 				= 'Exportação de Pendências';
		modulos['pendencias'] 							= 'Importação de Pendências';
		modulos['pendencias_usuarios'] 					= 'Importação de Pendências Destinatários';
		modulos['pendencias_mensagens'] 				= 'Importação de Pendências Mensagens';
		modulos['exportacao_agenda'] 					= 'Exportação de Agenda';
		modulos['agenda'] 								= 'Importação de Agenda';
		modulos['transportadoras'] 						= 'Transportadoras';
		modulos['eventos'] 								= 'Eventos';
		modulos['notas_fiscais']						= 'Notas Fiscais';
		modulos['titulos'] 								= 'Títulos';
		modulos['acrescimo_familia'] 					= 'Acréscimos das Famílias de Produtos.';
		modulos['desconto_familia'] 					= 'Descontos das Famílias de Produtos.';
		modulos['exportacao_pedido_parcela'] 			= 'Exportação das Parcelas dos Pedidos';
		
	//Salvando Informações da sincronização
	db.transaction(function(x) {
		x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, tabela]);
		
		if(tabela == 'produtos'){
			localStorage.setItem('data_atualizacao_estoque', time() + 60 * 60);
		}
	});

	//Mensagem para o usuário
	$('#'+tabela).after('<span class="ui-icon-white ui-icon-check"></span> ');
	$('#modulos-sincronizados').prepend('<li><b>Sucesso.</b> - '+modulos[tabela]+'</li>').show();
	
	//Proxima funcao
	if(proxima_funcao)
	{
		eval(proxima_funcao);
	}
	else
	{
		mensagem('Sincronização concluída com sucesso.', "window.location = 'index.html';");																			
	}

}

//-- -- -- -- -- -- -- -- -- -- -- --

/**
* Metódo:		exportar_dados
* 
* Descrição:	Função Utilizada para enviar dados d eum modulo do DWFORÇA DE VENDAS para o WEBSERVICE
* 
* Data:			24/09/2013
* Modificação:	25/09/2013
* 
* @param		string 		$codigo_modulo					- ID do modulo (Campo Chave) para identificar a chave
* @param		string 		$modulo							- Modulo que sera sincronizado (nome da tabela no WS)
* @param		string 		$descricao_modulo				- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_funcao					- Proxima funcao que sera chamada quando finalziar a sincronização de um pacote
* @param		array 		$codigo							- Array utilizado para especificar quais dados serão enviados (EXP: vamos enviar somento o prospect "54321" => codigos[0] = '54321';)
*
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function exportar_dados(codigo_modulo, modulo, descricao_modulo, proxima_funcao, exportar_codigos)
{
	// Carregando
	$('#carregando').show();
	$('#btnCancelar').show();
	$('#btnSincronizar').hide();
	$('#carregando').find('span:first').html('Enviando ' + descricao_modulo + '…');
	$( "#progressbar" ).progressbar({value: 30});
	
	db.transaction(function(x) {
		if(modulo == 'pedidos_pendentes' || modulo == 'orcamentos') {
			var where = ' WHERE exportado IS NULL';

			if(exportar_codigos && exportar_codigos.length > 0) {
				where += ' AND ' + codigo_modulo + " IN ('" + implode('\', \'', exportar_codigos) + "')";
			}

			x.executeSql('SELECT ' + codigo_modulo + ' FROM ' + modulo + where + ' GROUP BY ' + codigo_modulo, [], function(x, dados) {
				var pedidos = new Array();
				
				for(var i = 0; i < dados.rows.length; i++){
					pedidos[i] = dados.rows.item(i);
				}
				
				if(pedidos.length > 0) {
					
					enviar_pedidos_orcamentos(pedidos, modulo, codigo_modulo, descricao_modulo, false, proxima_funcao);
					
				} else {
					
					finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
					
				}
			},function (){ // Se a tabela não existir entrar nessa condição
				
				//console.log('erro ao exportar modulo: '+ modulo);
				
				finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
			});
		} else {
			var where = '';

			if(modulo != 'pedidos_pendentes') {
				where += ' WHERE exportado IS NULL';
			}

			if(exportar_codigos && exportar_codigos.length > 0) {
				if(where == '') {
					where += ' WHERE ';
				} else {
					where += ' AND ';
				}
				where += codigo_modulo + " IN ('" + implode('\', \'', exportar_codigos) + "')";
			}
			
			x.executeSql('SELECT * FROM ' + modulo + where, [], function(x, dados) {
				
				var total_registros = dados.rows.length;
				
				//console.log('Total = ' + total_registros);
				
				if (total_registros)
				{
					
					var retorno_json = new Array();
					for(var registro = 0; registro < total_registros; registro++)
					{
						retorno_json[registro] = dados.rows.item(registro);
					}
					
					var ajax = $.ajax({
						url: config.ws_url + modulo + '/importar',
						type: 'POST',
						data: {
							retorno: json_encode(retorno_json)
						},
						success: function(dados) {
							if(dados.sucesso == 'ok')
							{
								//Apagando os dados com erro na sessao
								localStorage.setItem('erros_' + modulo, '');
								
								if(exportar_codigos && exportar_codigos.length > 0)
								{
									// Marcando os dados especificados no "exportar_codigos" como exportados
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\')', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										});
									});
								}
								else
								{
									// Marcando todos os dados como exportados
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										});
									});
								}

							} else if(dados.erro) {
								
								var codigos_sessao = [];
								var codigos = [];
								$.each(dados.erro, function(i, codigo) {
									codigos.push("'" + codigo + "'");
									
									if(is_numeric(codigo))
									{
										codigos_sessao.push(codigo);
									}
									
								});
								
								//Gravando na sessao dos dados com erro
								localStorage.setItem('erros_' + modulo, codigos_sessao);
								
								
								var descricoes = [];
								$.each(dados.erro_descricao, function(i, descricao) {
									descricoes.push(descricao);
								});
							
								db.transaction(function(x) {
								
									//Precisamos marcar quais dados estao com erro, e depois apagar todos os dados que não estao com erro
									x.executeSql('UPDATE ' + modulo + ' SET erro = \'1\' WHERE ' + codigo_modulo + ' IN (' + codigos.join(', ') + ')');
									
									if(exportar_codigos && exportar_codigos.length > 0)
									{
										// Marcando os dados especificados no "exportar_codigos" como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\') AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									}
									else
									{
										// Marcando todos os dados como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									}
									
									
									apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível sincronizar:<b><br />(' + descricoes.join(', ') + ').</b><br />Entre em contato com TI da sua empresa.', {
										'textYes': 'OK'
									}, function (dado) {
									
										$( "#progressbar" ).progressbar({value: 100});
										finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										
									});

									
								});
								
							}
						},
					    error: function(XMLHttpRequest, textStatus, errorThrown){
					       
					    	mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.'+
					    			"\n"+
					    			"Se o problema persistir entre em contato com seu gestor comercial."
					    	);
					    	
					    	
					    }
						
					});
					
					
				}
				else
				{
					db.transaction(function(x) {
						var where = '';
						
						if(modulo == 'pedidos_pendentes') {
							//Exclui somente os pedidos que já estão na SC5
							where = ' AND pedido_data_emissao <= "' + date('Ymd', strtotime('-15 days')) + '"';
						}
						
						x.executeSql('DELETE FROM ' + modulo + where);
						
						$( "#progressbar" ).progressbar({value: 100});
						finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
					});
				}
				
			},
			function (){ // Se a tabela não existir entrar nessa condição
				finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
			});
		}
		
		
	});
}

function enviar_pedidos_orcamentos(pedidos, modulo, codigo_modulo, descricao_modulo, iteracao_atual, proxima_funcao){
	if(iteracao_atual == false) {
		var iterador_pedido = 0;
	} else {
		var iterador_pedido = iteracao_atual;
	}
	
	$('#carregando').find('span:first').html('Enviando pedido ' + pedidos[iterador_pedido][codigo_modulo] + '…');
	
	var iterador_maximo_pedido = pedidos.length - 1;
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM ' + modulo + ' WHERE ' + codigo_modulo + ' = ?', [pedidos[iterador_pedido][codigo_modulo]], function(x, dados) {
			var pedido = new Array();
			for(var i = 0; i < dados.rows.length; i++){
				var dado = dados.rows.item(i);
				
				pedido[i] = dado;
			}

			var ajax = $.ajax({
				url: config.ws_url + modulo + '/importar',
				type: 'POST',
				data: {
					retorno: json_encode(pedido)
				},
				success: function(dados) {
					if(dados.sucesso == 'ok') {
						
						db.transaction(function(x) {
							x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\', erro = NULL WHERE ' + codigo_modulo + ' = ?', [pedidos[iterador_pedido][codigo_modulo]], function(){
								
								if(iterador_pedido >= iterador_maximo_pedido){
									
									finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
									
								} else {
									
									enviar_pedidos_orcamentos(pedidos, modulo, codigo_modulo, descricao_modulo, iterador_pedido + 1, proxima_funcao);
									
								}
								
							}, function(){
								
								alert('Erro ao atualizar o status do pedido ' + pedidos[iterador_pedido][codigo_modulo]);
								
							});
						});
						
					} else if(dados.erro){
						
						alert('Erro ao gravar o pedido' + pedidos[iterador_pedido][codigo_modulo]);
						
						db.transaction(function(x) {
							x.executeSql('UPDATE ' + modulo + ' SET erro = \'1\', exportado = NULL WHERE ' + codigo_modulo + ' = ?', [pedidos[iterador_pedido][codigo_modulo]], function(){
								
								if(iterador_pedido >= iterador_maximo_pedido){
									
									finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
									
								} else {
									
									enviar_pedidos_orcamentos(pedidos, modulo, codigo_modulo, descricao_modulo, iterador_pedido + 1, proxima_funcao);
									
								}
								
							}, function(){
								
								alert('Erro ao atualizar o status do pedido ' + pedidos[iterador_pedido][codigo_modulo]);
								
							});
						});
						
					}
				},
				error: function(){
					
					alert('Erro ao enviar o pedido ' + pedidos[iterador_pedido][codigo_modulo]);
					
					db.transaction(function(x) {
						x.executeSql('UPDATE ' + modulo + ' SET erro = \'1\', exportado = NULL WHERE ' + codigo_modulo + ' = \'' + pedidos[iterador_pedido][codigo_modulo] + '\'', [], function(){
					
							if(iterador_pedido >= iterador_maximo_pedido){
							
								finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
								
							} else {
								
								enviar_pedidos_orcamentos(pedidos, modulo, codigo_modulo, descricao_modulo, iterador_pedido + 1, proxima_funcao);
							
							}
							
						}, function(e, x){
							
							//console.log(x);
							alert('Erro ao atualizar o status do pedido ' + pedidos[iterador_pedido][codigo_modulo]);
							
						});
					});
					
				}
			});
		},
		function (){ // Se a tabela não existir entrar nessa condição
			finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
		});
	});
}
