$(document).ready(function() {
	
});

function calcularDescontoFlex (familiaProduto, percentualModificador, modificador, callback) {
	if (modificador == 'D') {
		getPercentualComissao(familiaProduto, percentualModificador, 'desconto_familia',
			function(percentual_comissao) {
			callback(percentual_comissao);
		});
	} else if (modificador == 'A') {
		getPercentualComissao(familiaProduto, percentualModificador, 'acrescimo_familia',
			function(percentual_comissao) {
			callback(percentual_comissao);
		});
	} else {
		callback(0);
	}
}

function getLimiteDesconto(familiaProduto, callback) {
	var where = " WHERE familia_produto = " + familiaProduto;
	db.transaction(function(x) {
		x.executeSql("SELECT MAX(faixa_inicial) AS limite FROM desconto_familia " + where, [], function(x, dados) {
			if(dados.rows.length) {
				callback(dados.rows.item(0).limite);
			} else {
				callback(999);
			}
		})
	});
}

function getLimiteAcrescimo(familiaProduto, callback) {
	var where = " WHERE familia_produto = " + familiaProduto;
	db.transaction(function(x) {
		x.executeSql("SELECT MAX(faixa_inicial) AS limite FROM acrescimo_familia " + where, [], function(x, dados) {
			if(dados.rows.length) {
				callback(dados.rows.item(0).limite);
			} else {
				callback(999);
			}
		})
	});
}

function getPercentualComissao (familiaProduto, percentualModificador, tabela, callback) {
	var where = " WHERE	familia_produto = " + familiaProduto + " AND faixa_inicial <= " + percentualModificador + " AND faixa_final >= " + percentualModificador;
	db.transaction(function(x) {
		x.executeSql("SELECT percentual_comissao FROM " + tabela + where, [], function(x, dados) {
			if(dados.rows.length) {
				callback(dados.rows.item(0).percentual_comissao);
			} else {
				callback(0);
			}
		})
	});
}

function isModifcadorAcimaLimite(familiaProduto, percentualModificador, modificador, callback) {
	if(modificador == 'A') {
		getLimiteDesconto(familiaProduto, function(limite) {
			if(parseFloat(percentualModificador) > parseFloat(limite)) {
				callback(true);
			} else {
				callback(false);
			}
		});
	} else {
		getLimiteDesconto(familiaProduto, function(limite) {
			if(parseFloat(percentualModificador) > parseFloat(limite)) {
				callback(true);
			} else {
				callback(false);
			}
		});
	}
}