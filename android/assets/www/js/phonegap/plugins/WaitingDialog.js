//window.plugins.waitingDialog

function WaitingDialog() {
}

WaitingDialog.prototype.fecharDialog = function() {
	cordova.exec(null, null, "WaitingDialog", "fecharDialog", []);
}

WaitingDialog.prototype.mostrarDialog = function(text) {
	cordova.exec(null, null, "WaitingDialog", "mostrarDialog", [text]);
}

cordova.addConstructor(function() {
	if(!window.plugins) {
		window.plugins = {};
	}

	// shim to work in 1.5 and 1.6
	if (!window.Cordova) {
		window.Cordova = cordova;
	};

	window.plugins.waitingDialog = new WaitingDialog();
});