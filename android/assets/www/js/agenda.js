$(document).ready(function() {

    "use strict";

    db.transaction(function(x) {
		x.executeSql("SELECT * FROM agenda ORDER BY timestamp DESC",  [], function(x, dados) {
			
			var i = 0;
			var compromissos = [];
			for (i = 0; i < dados.rows.length; i++)
			{				
				compromissos.push(dados.rows.item(i));
			}
			
			var options = {
				compromissos: 	compromissos,
				view: 			'month',
				tmpl_path: 		'js/bootstrap/calendar/tmpls/',
				tmpl_cache: 	false,
				day: 			date('Y-m-d'),
				language: 		'pt-BR',
				holidays: {
					'01-01': 'Ano Novo',
					'12-02': 'Carnaval',
					'29-03': "Sexta-feira da Paixão",
					'21-04': "Tiradentes",
					'01-05': "Dia do Trabalho",
					'30-05': "Corpus Christi",
					'07-09': "Independência do Brasil",
					'12-10': "Nossa Senhora Aparecida",
					'02-11': "Finados",
					'15-11': "Proclamação da República",
					'25-12': "Natal",
				},
				first_day: 2,
				onAfterEventsLoad: function(events) {	
					//Aqui
				},
				onAfterViewLoad: function(view) {
					$('.page-header h2').text(this.getTitle());
					$('.btn-group button').removeClass('active');
					$('button[data-calendar-view="' + view + '"]').addClass('active');
				},
				classes: {
					months: {
						general: 'label'
					}
				}
			};

			var calendar = $('#calendar').calendar(options);
				calendar.setOptions({first_day: 2});
		        calendar.setLanguage('pt-BR');
		        calendar.view();

			$('.btn-group button[data-calendar-nav]').each(function() {
				var $this = $(this);
				$this.click(function() {
					calendar.navigate($this.data('calendar-nav'));
				});
			});

			$('.btn-group button[data-calendar-view]').each(function() {
				var $this = $(this);
				$this.click(function() {
					calendar.view($this.data('calendar-view'));
				});
			});
			
		}, function(t, e){		
			
		});
	});
});