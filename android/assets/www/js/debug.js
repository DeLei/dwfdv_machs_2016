$(document).ready(function() {
	
	$("#executar").click(function(){
		var sql = $("#sql").val();
		
		executarSql(sql);
	});
	
	$("#limpar").click(function(){
		$("#sql").val('');
		$("#resultSql").html('');
	});
    
});

function executarSql(sql) {
	
	var comando = sql.toUpperCase();
	
	var tipo_comando = null;
	if(strstr(comando, 'SELECT')) {
		tipo_comando = 'SELECT';
	}
	
	var html = '<h2>Aguarde, processando informação...</h2>';
	
	$('#resultSql').html(html);
	
	db.transaction(function(x) {
		x.executeSql(sql, [], function(x, dados) {
			
			if(tipo_comando == 'SELECT') {
				if (dados.rows.length) {
					var cabecalho = dados.rows.item(0);
					
					var html = '<table class="normal">';
						html += '<thead>';
					
					$.each(cabecalho, function(campo, valor){
						html += '<th>' + campo + '</th>';
					});
					
					html += '</thead><tbody>';
					
					for (i = 0; i < dados.rows.length; i++) {
						var dado = dados.rows.item(i);
						
						html += '<tr>';
							$.each(dado, function(campo, valor) {
								html += '<td>' + valor + '</td>';
							});
						html += '</tr>';
					}
					html += '</tbody></table>';
					
					$('#resultSql').html(html);
				
				} else {
					
					var html = '<h2>Nenhum registro encontrado.</h2>';
					
					$('#resultSql').html(html);
				}
				
			} else {
				
				var html = '<h2>Comando executado com sucesso.</h2>';
				
				$('#resultSql').html(html);
				
			}
			
		}, function(e, x) {
			var html = '<h2>Erro ' + x.code + ' - ' + x.message + '</h2>';
			
			$('#resultSql').html(html);
		});
	});
}
