var modulos = {
		
		
	empresas: {
		tabela: 'empresas',
		nome: 'Empresas'
	},
	filiais: {
		tabela: 'filiais',
		nome: 'Filiais'
	},
	clientes: {
		tabela: 'clientes',
		nome: 'Clientes'
	},
	historico_clientes: {
		tabela: 'historico_clientes',
		nome: 'Histórico de Clientes'
	},
	tabelas_preco: {
		tabela: 'tabelas_preco',
		nome: 'Tabelas de Preços'
	},
	representate_tabela_preco: {
		tabela: 'representante_tabela_preco',
		nome: 'Representante X Tabelas de Preços'
	},
	produtos: {
		tabela: 'produtos',
		nome: 'Produtos'
	},
	condicoes_pagamento: {
		tabela: 'condicoes_pagamento',
		nome: 'Condições de Pagamento'
	},
	formas_pagamento: {
		tabela: 'formas_pagamento',
		nome: 'Formas de Pagamento'
	},
	parcela_pedido: {
		tabela: 'parcela_pedido',
		nome: 'Parcelas dos Pedidos'
	},
	prazo_parcela: {
		tabela: 'prazo_parcela',
		nome: 'Prazos das Parcelas'
	},
	acrescimo_familia: {
		tabela: 'acrescimo_familia',
		nome: 'Acréscimo das Famílias de Produto.'
	},
	desconto_familia: {
		tabela: 'desconto_familia',
		nome: 'Desconto das Famílias de Produto.'
	},
	municipios: {
		tabela: 'municipios',
		nome: 'Municípios'
	},
	pedidos_processados: {
		tabela: 'pedidos_processados',
		nome: 'Pedidos Processados'
	},
	pedidos_pendentes: {
		tabela: 'pedidos_pendentes',
		nome: 'Pedidos Pendentes'
	},
	pendencias:{
		tabela: 'pendencias',
		nome: 'Pendências'
	},
	pendencias_mensagens:{
		tabela: 'pendencias_mensagens',
		nome: 'Pendências Mensagens'
	},
	pendencias_usuarios:{
		tabela: 'pendencias_usuarios',
		nome: 'Pendências Usuários'
	},
	noticias:{
		tabela: 'noticias',
		nome: 'Notícias'
	},
	derivacoes_produtos:{
		tabela: 'derivacoes_produtos',
		nome: 'Derivação de Produtos'
	},
	informacoes_do_representante: {
		tabela: 'informacoes_do_representante',
		nome: 'Informações do Representante'
	},
	sincronizacoes: {
		tabela: 'sincronizacoes',
		nome: 'Sincronizações'
	}
};

$(document).ready(function() {
	
	
	if($('.versao_apk').length > 0){
		$('.versao_apk').html(config.versao);
	}
	
	
	
	//-------------------------------------------------------
	// Verificar se existe atualização
	//-------------------------------------------------------
	$('#verificarAtualizacao').click(function(e){
		//Para link
		e.preventDefault();
		
		//Remover
		$('#atualizacao_dwfdv').remove();
		
		//SHOW
		
		
		//Verificar versão por ajax
		if(window.navigator.onLine)
		{
		
			
			$.ajax({
				type: 	"GET",
				url: 	config.ws_url + 'atualizacao/ultima_versao',
				data: { versao : config.versao },
				success: function(data) {
			
					
					if(data.situacao == '001')
					{
						
						document.addEventListener("deviceready", function(){
							//Avisar que existe pendencias							
							if (typeof plugins !== "undefined") 
							{
			                    plugins.localNotification.add({
			                        date : new Date(),
			                        message : 'O sistema encontrou uma nova versão. Versão: '+ data.versao.versao,
			                        ticker : 'O sistema encontrou uma nova versão.',
			                        repeatDaily : false,
			                        id : 4
			                    });
			                    
							}else{
								
							}
						}, false);
					
						var html = '<div id="atualizacao_dwfdv" class="info message"><div class="clear"></div>';
							html += '	<b>Atualização de Software</b> - O sistema encontrou uma nova versão.<br /><br />';
							html += '	<p>Versão em execução atualmente: <b>' + config.versao + '</b>.<br />';
							html += '	Versão disponível para atualização: <b>' + data.versao.versao + '</b>.</p>';
							html += '	<br />';
							html += '	<p>Deseja atualizar a versão?<br /><br />';
							html += ' 	<button id="btn_atualizar" type="button" class="btn btn-large btn-success">SIM</button> ou ';
							html += ' 	<button id="btn_naoAtualizar"type="button" class="btn btn-large btn-warning">AGORA NÃO</button>';
							html += ' 	</p>';
							html += '</div>';
							
						$('#conteudo').before(html);	
					}
					else
					{
						mensagem('<b>Atualização de Software</b> - ' + data.mensagem);
					}
					
				}
			});
		}
		else
		{
			
			mensagem('Sem acesso a <strong>Internet</strong>.');
		}
	});
	//-------------------------------------------------------
	// Verificar se existe atualização
	//-------------------------------------------------------
	
	
	$.each(modulos, function (i, v) {
		db.transaction(function (x) {
			x.executeSql(
				'SELECT * FROM sincronizacoes WHERE modulo = ? ORDER BY timestamp DESC LIMIT 1', [v.tabela], function(x, dados) {
					if (dados.rows.length)
					{
						var sinc = dados.rows.item(0);
						
						$('table tbody').append('<tr><td><strong>' + v.nome + '</strong></td><td>' + date('d/m/Y H:i:s', sinc.timestamp) + '</td><td>' + (sinc.sucesso == 1 ? 'SIM' : 'NÃO') + '</td><td><div id="remover_'+v.tabela+'">Normal</div></td></tr>');
					}
					else if(v.tabela != 'sincronizacoes' && v.tabela != 'informacoes_do_representante')
					{
						$('table tbody').append('<tr><td><strong>' + v.nome + '</strong></td><td>  </td><td> NÃO </td><td><div id="remover_'+v.tabela+'">Cancelado</div></td></tr>');
					}
					
					alterarCabecalhoTabelaResolucao();
					
				}
			);
		});
	});
	
});