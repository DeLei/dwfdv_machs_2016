$(document).ready(function() {
	db.transaction(function(x) {
		// obter feiras
		x.executeSql(
			'SELECT DISTINCT id, nome FROM eventos ORDER BY nome ASC', [], function(x, dados) {
				
				$('select[name=id_feira]').append('<option value> Nenhum </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=id_feira]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
					}
				}
			}
		);
		
		//Obter Filiais
		x.executeSql(
			'SELECT DISTINCT codigo, razao_social FROM filiais ORDER BY codigo ASC', [], function(x, dados) {
				
				$('select[name=filial]').append('<option value> Selecione... </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
					}
				}
			}
		);
		
	});
	

	//Cancelar
	$('#cancelar').click(function() {
		document.location = 'prospects.html';
	});
	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	
	// Obter Municipios
	
	$('select[name=estado]').live('change', function(){
		$('select[name=codigo_municipio]').attr('disabled', 'disabled');
		$('select[name=codigo_municipio]').html('<option value> Carregando... </option>');
		
		var estado = $(this).val();
		
		// obter Municipios
		db.transaction(function(x) {
		
			
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
					
					$('select[name=codigo_municipio]').empty();
					$('select[name=codigo_municipio]').removeAttr('disabled');
					$('select[name=codigo_municipio]').append('<option value> Selecione... </option>');
					if (dados.rows.length)
					{
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							$('select[name=codigo_municipio]').append('<option value="' + dado.codigo + '">' + dado.nome + '</option>');
						}
					}
					else
					{
						$('select[name=codigo_municipio]').empty();
						$('select[name=codigo_municipio]').attr('disabled', 'disabled');
						$('select[name=codigo_municipio]').append('<option value> Selecione um Estado </option>');
					}
					
				}
			);
			
		});
		
	});
	
	$('select[name=codigo_municipio]').live('change', function(){
		
		var nome_municipio = $('select[name=codigo_municipio] option:selected').html();
		$('input[name=nome_municipio]').val(nome_municipio);
		
	});
	
	
	$('select[name=tipo]').live('change', function(){
		
		if($(this).val() == 'L')
		{
			$('input[name=inscricao_rural]').addClass('obrigatorio');
		}
		else
		{
			$('input[name=inscricao_rural]').removeClass('obrigatorio');
		}
	});
	
	
	// enviar form
	
	$('form').submit(function() {
	
	
		if (!$('select[name=filial]').val())
		{
			mensagem('Selecione uma <strong>Filial</strong>.');
		}
		else if (!$('select[name=tipo]').val())
		{
			mensagem('Selecione uma <strong>Tipo</strong> de Prospect.');
		}
		else if (!$('input[name=nome]').val())
		{
			mensagem('Preencha o campo <strong>Nome / Razão social</strong>.');
		}
		else if (!$('input[name=nome_fantasia]').val())
		{
			mensagem('Preencha o campo <strong>Nome Fantasia</strong>.');
		}
		else if (!$('input[name=cgc]').val() || !validar_cpf_cnpj($('input[name=cgc]').val()))
		{
			mensagem('Preencha um <strong>CPF / CNPJ</strong> válido.');
		}
		else if ($('select[name=tipo]').val() == 'L' && !$('input[name=inscricao_rural]').val())
		{
			mensagem('Preencha o campo <strong>Inscrição Rural</strong>.');
		}
		else if (!$('input[name=ddd]').val())
		{
			mensagem('Preencha o campo <strong>DDD</strong>.');
		}
		else if (!validar_ddd($('input[name=ddd]').val()))
		{
			mensagem('Preencha um <strong>DDD</strong> válido.');
		}
		else if (!$('input[name=telefone]').val())
		{
			mensagem('Preencha o campo <strong>Telefone</strong>.');
		}
		else if (!validar_telefone($('input[name=telefone]').val()))
		{
			mensagem('Preencha um <strong>Telefone</strong> válido.');
		}
		else if ($('input[name=fax]').val() && !validar_telefone($('input[name=fax]').val()))
		{
			mensagem('Preencha um <strong>Fax</strong> válido.');
		}
		else if (!validar_email($('input[name=email]').val()))
		{
			mensagem('Preencha um <strong>E-mail NF-e</strong> válido.');
		}
		else if ($('input[name=email_contato]').val() && !validar_email($('input[name=email_contato]').val()))
		{
			mensagem('Preencha um <strong>E-mail</strong> válido.');
		}
		else if (!validar_cep($('input[name=cep]').val()))
		{
			mensagem('Digite um CEP válido.');
		}
		else if (!$('input[name=endereco]').val())
		{
			mensagem('Preencha um <strong>Endereço</strong> válido.');
		}
		else if (!$('input[name=bairro]').val())
		{
			mensagem('Preencha um <strong>Bairro</strong> válido.');
		}
		else if (!$('select[name=estado]').val())
		{
			mensagem('Preencha um <strong>Estado</strong> válido.');
		}
		else if (!$('select[name=codigo_municipio]').val())
		{
			mensagem('Preencha um <strong>Município</strong> válido.');
		}
		else
		{
			// -----------
			// Isentos
			if (!$('input[name=inscricao_estadual]').val()){
				$('input[name=inscricao_estadual]').val('ISENTO');
			}
			
			if (!$('input[name=inscricao_municipal]').val()){
				$('input[name=inscricao_municipal]').val('ISENTO');
			}
			
			if (!$('input[name=inscricao_rural]').val()){
				$('input[name=inscricao_rural]').val('ISENTO');
			}
			// Isentos
			// -----------
			
		
			db.transaction(function(x) {
				x.executeSql(
					'SELECT codigo FROM prospects WHERE cgc = ?', [$('input[name=cgc]').val()], function(x, dados) {
						if ($('input[name=cgc]').val() && dados.rows.length)
						{
							mensagem('O <strong>CPF / CNPJ</strong> digitado já existe.');
						}
						else
						{
							x.executeSql(
								'SELECT codigo FROM prospects ORDER BY codigo DESC LIMIT 1', [], function(x, dados) {
							
									var codigo = uniqid();

									
									var tmp_1 = ['codigo', 'codigo_representante'];
									var tmp_2 = ['?', '?'];
									var tmp_3 = [codigo, info.cod_rep];
									var telefone = '';
									
									$('input[name], textarea[name], select[name]').each(function() {
										var campo_nome 	= $(this).attr('name');
										var campo_valor = $(this).val();
										
										
										if(campo_nome == 'data_nascimento')
										{
											if(campo_valor)
											{
												campo_valor = data_normal2protheus(campo_valor);
											}
										}
										
										
										
										
										tmp_1.push(campo_nome);
										tmp_2.push('?');
										tmp_3.push(campo_valor);
									});
									
									tmp_1.push('data_emissao');
									tmp_2.push('?');
									tmp_3.push(date('Ymd'));
									
									tmp_1.push('time_emissao');
									tmp_2.push('?');
									tmp_3.push(time());
									
									tmp_1.push('empresa');
									tmp_2.push('?');
									tmp_3.push(info.empresa);
									
									tmp_1.push('codigo_empresa');
									tmp_2.push('?');
									tmp_3.push(info.empresa);
									
									tmp_1.push('empresa');
									tmp_2.push('?');
									tmp_3.push(info.empresa);
									
									tmp_1.push('latitude');
									tmp_2.push('?');
									tmp_3.push(localStorage.getItem('gps_latitude'));
									
									tmp_1.push('longitude');
									tmp_2.push('?');
									tmp_3.push(localStorage.getItem('gps_longitude'));
									
									tmp_1.push('versao');
									tmp_2.push('?');
									tmp_3.push(localStorage.getItem('versao'));
									
									x.executeSql('INSERT INTO prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3);
									
									window.location = 'prospects_visualizar.html#' + codigo;
								}
							);
						}
					}
				);
			});
		}
		
		return false;
	});
});
