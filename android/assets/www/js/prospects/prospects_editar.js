var prospect;

$(document).ready(function() {
	db.transaction(function(x) {
		// obter feiras
		x.executeSql(
			'SELECT DISTINCT id, nome FROM eventos ORDER BY nome ASC', [], function(x, dados) {
				
				$('select[name=id_feira]').append('<option value> Nenhum </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=id_feira]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
					}
				}
			}
		);
		
		//Obter Filiais
		x.executeSql(
			'SELECT DISTINCT codigo, razao_social FROM filiais ORDER BY codigo ASC', [], function(x, dados) {
				
				$('select[name=filial]').append('<option value> Selecione... </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
					}
				}
			}
		);
		
		// obter prospect
		var codigo = parse_url(location.href).fragment;
		
		x.executeSql(
			'SELECT * FROM prospects WHERE codigo = ?', [codigo], function(x, dados) {
				if (dados.rows.length)
				{
				
					prospect = dados.rows.item(0);
					
					
					
					$('input[name=cgc]').attr('readonly', 'readonly');
					
					
					
					$('#cancelar').attr('href', 'prospects_visualizar.html#' + prospect.codigo);
					
					var municipio = '';
					
					$.each(prospect, function(i, v) {
					
						if ($('[name=' + i + ']').length)
						{
							if(i == 'estado')
							{
								estado = v;
								obter_municipios(v, municipio);
							}
							
							if(i == 'codigo_municipio')
							{
								municipio = v;
							}
							
							if(i == 'data_nascimento')
							{
								if(v)
								{
									v = protheus_data2data_normal(v);
								}
							}
							
							if(i == 'telefone')
							{
								if(v)
								{
									v = remover_caracteres_telefone(v);
								}

							}
							
							if(i == 'fax')
							{
								if(v)
								{
									v = remover_caracteres_telefone(v);
								}
							}
							
							
						
							$('[name=' + i + ']').val(v);
						}
					});
				}
				else
				{
					window.location = 'index.html';
				}
			}
		);
	});
	
	// copiar endereço
	
	var inputs = ['cep', 'endereco', 'numero', 'complemento', 'bairro', 'cidade', 'estado'];
	
	$('#copiar_end_cob').click(function() {
		var _this = this;
		
		$.each(inputs, function(i, v) {
			if ($(_this).is(':checked'))
			{
				$('[name=' + v + ']').val($('[name=' + v + '_cobranca]').val());
			}
			else
			{
				$('[name=' + v + ']').val('');
			}
		});
	});
	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	
	// Obter Municipios
	
	$('select[name=estado]').live('change', function(){
		$('select[name=codigo_municipio]').attr('disabled', 'disabled');
		$('select[name=codigo_municipio]').html('<option value> Carregando... </option>');
		
		var estado = $(this).val();
		
		obter_municipios(estado);
		
	});
	
	function obter_municipios(estado, municipio)
	{
		// obter Municipios
		db.transaction(function(x) {
		
			
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
					
					$('select[name=codigo_municipio]').empty();
					$('select[name=codigo_municipio]').removeAttr('disabled');
					$('select[name=codigo_municipio]').append('<option value> Selecione... </option>');
					if (dados.rows.length)
					{
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							var selected = '';
							
							if(municipio == dado.codigo)
							{
								selected = 'selected="selected"';
							}
							
							$('select[name=codigo_municipio]').append('<option value="' + dado.codigo + '" ' + selected + '>' + dado.nome + '</option>');
						}
					}
					else
					{
						$('select[name=codigo_municipio]').empty();
						$('select[name=codigo_municipio]').attr('disabled', 'disabled');
						$('select[name=codigo_municipio]').append('<option value> Selecione um Estado </option>');
					}
					
				}
			);
			
		});
	}

	
	$('select[name=codigo_municipio]').live('change', function(){
		
		var nome_municipio = $('select[name=codigo_municipio] option:selected').html();
		$('input[name=nome_municipio]').val(nome_municipio);
		
	});
	
	$('select[name=tipo]').live('change', function(){
		
		if($(this).val() == 'L')
		{
			$('input[name=inscricao_rural]').addClass('obrigatorio');
		}
		else
		{
			$('input[name=inscricao_rural]').removeClass('obrigatorio');
		}
	});
	
	//Cancelar
	$('#cancelar').click(function() {
		document.location = $(this).attr('href');
	});
	
	// enviar form
	
	
	$('form').submit(function() {
	
		if (!$('select[name=filial]').val())
		{
			mensagem('Selecione uma <strong>Filial</strong>.');
		}
		else if (!$('select[name=tipo]').val())
		{
			mensagem('Selecione uma <strong>Tipo</strong> de Prospect.');
		}
		else if (!$('input[name=nome]').val())
		{
			mensagem('Preencha o campo <strong>Nome / Razão social</strong>.');
		}
		else if (!$('input[name=nome_fantasia]').val())
		{
			mensagem('Preencha o campo <strong>Nome Fantasia</strong>.');
		}
		else if (!$('input[name=cgc]').val() || !validar_cpf_cnpj($('input[name=cgc]').val()))
		{
			mensagem('Preencha um <strong>CPF / CNPJ</strong> válido.');
		}
		else if ($('select[name=tipo]').val() == 'L' && !$('input[name=inscricao_rural]').val())
		{
			mensagem('Preencha o campo <strong>Inscrição Rural</strong>.');
		}
		else if (!$('input[name=ddd]').val())
		{
			mensagem('Preencha o campo <strong>DDD</strong>.');
		}
		else if (!validar_ddd($('input[name=ddd]').val()))
		{
			mensagem('Preencha um <strong>DDD</strong> válido.');
		}
		else if (!$('input[name=telefone]').val())
		{
			mensagem('Preencha o campo <strong>Telefone</strong>.');
		}
		else if (!validar_telefone($('input[name=telefone]').val()))
		{
			mensagem('Preencha um <strong>Telefone</strong> válido.');
		}
		else if ($('input[name=fax]').val() && !validar_telefone($('input[name=fax]').val()))
		{
			mensagem('Preencha um <strong>Fax</strong> válido.');
		}
		else if (!validar_email($('input[name=email]').val()))
		{
			mensagem('Preencha um <strong>E-mail NF-e</strong> válido.');
		}
		else if ($('input[name=email_contato]').val() && !validar_email($('input[name=email_contato]').val()))
		{
			mensagem('Preencha um <strong>E-mail</strong> válido.');
		}
		else if (!validar_cep($('input[name=cep]').val()))
		{
			mensagem('Digite um CEP válido.');
		}
		else if (!$('input[name=endereco]').val())
		{
			mensagem('Preencha um <strong>Endereço</strong> válido.');
		}
		else if (!$('input[name=bairro]').val())
		{
			mensagem('Preencha um <strong>Bairro</strong> válido.');
		}
		else if (!$('select[name=estado]').val())
		{
			mensagem('Preencha um <strong>Estado</strong> válido.');
		}
		else if (!$('select[name=codigo_municipio]').val())
		{
			mensagem('Preencha um <strong>Município</strong> válido.');
		}
		else
		{
			// -----------
			// Isentos
			if (!$('input[name=inscricao_estadual]').val()){
				$('input[name=inscricao_estadual]').val('ISENTO');
			}
			
			if (!$('input[name=inscricao_municipal]').val()){
				$('input[name=inscricao_municipal]').val('ISENTO');
			}
			
			if (!$('input[name=inscricao_rural]').val()){
				$('input[name=inscricao_rural]').val('ISENTO');
			}
			// Isentos
			// -----------
			
			db.transaction(function(x) {
				x.executeSql(
					'SELECT codigo FROM prospects WHERE cgc = ? AND codigo != ?', [$('input[name=cgc]').val(), prospect.codigo], function(x, dados) {
						if ($('input[name=cgc]').val() && dados.rows.length)
						{
							mensagem('O <strong>CPF / CNPJ</strong> digitado já existe.');
						}
						else
						{
							x.executeSql(
								'SELECT codigo FROM prospects ORDER BY codigo DESC LIMIT 1', [], function(x, dados) {
							
									var tmp = new Array();
							
									tmp.push("editado = '1'");
									tmp.push("exportado = NULL");
									
									$('input[name], textarea[name], select[name]').each(function() {
										
										if(($(this).attr('name') != 'empresas') && ($(this).attr('name') != 'data_nascimento'))
										{
											tmp.push($(this).attr('name') + ' = "' + $(this).val() + '"');
										}
										
										
										if($(this).attr('name') == 'data_nascimento')
										{
											if($(this).val())
											{
												tmp.push($(this).attr('name') + ' = "' + data_normal2protheus($(this).val()) + '"');
											}
										}
										
										
										
										
										
										
									});
									
									tmp.push('codigo_empresa = "' + info.empresa + '"');
									
									x.executeSql('UPDATE prospects SET ' + tmp.join(', ') + ' WHERE codigo = ?', [prospect.codigo], function(x, dados){
									
										window.location = 'prospects_visualizar.html#' + prospect.codigo;
									
									});
							
									
								}
							);
						}
					}
				);
			});
		}
		
		return false;
	});
	
	/*
	$('form').submit(function() {
		if (!$('input[name=nome]').val())
		{
			mensagem('Digite uma Razão social.');
		}
		else if ($('input[name=cgc]').val() && !validar_cnpj($('input[name=cgc]').val()))
		{
			mensagem('Digite um CNPJ válido.');
		}
		else if ($('input[name=email]').val() && !validar_email($('input[name=email]').val()))
		{
			mensagem('Digite um E-mail válido.');
		}
		else if ($('input[name=cep]').val() && !validar_cep($('input[name=cep]').val()))
		{
			mensagem('Digite um CEP válido.');
		}
		else if ($('input[name=cep_cobranca]').val() && !validar_cep($('input[name=cep_cobranca]').val()))
		{
			mensagem('Digite um CEP de cobrança válido.');
		}
		else if ($('input[name=telefone]').val() && !validar_telefone($('input[name=telefone]').val()))
		{
			mensagem('Digite um telefone válido.');
		}
		else
		{
			db.transaction(function(x) {
				x.executeSql(
					'SELECT id FROM prospects WHERE cgc = ? AND cgc != ?', [$('input[name=cgc]').val(), prospect.cgc], function(x, dados) {
						if ($('input[name=cgc]').val() && dados.rows.length)
						{
							mensagem('O CNPJ digitado já existe.');
						}
						else
						{
							var tmp = prospect.adicionado_offline ? new Array() : new Array('editado_offline = 1');
							
							$('input[name], textarea[name], select[name]').each(function() {
								tmp.push($(this).attr('name') + ' = "' + $(this).val() + '"');
							});
							
							x.executeSql('UPDATE prospects SET ' + tmp.join(', ') + ' WHERE id = ?', [prospect.id]);
							
							window.location = 'prospects_visualizar.html#' + prospect.id;
						}
					}
				);
			});
		}
		
		return false;
	});
	*/
});
