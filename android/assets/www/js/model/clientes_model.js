function obter_cliente(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa, callback){
	var cliente = [];
	db.transaction(function(x) {
		x.executeSql(
			"SELECT * FROM clientes WHERE codigo = ? AND loja = ? AND filial = ? AND empresa = ? ", [codigo_cliente.toString(), codigo_loja.toString(), codigo_filial.toString(), codigo_empresa.toString()], 
			function(x, dados) 
			{		
				
				if( dados.rows.length > 0){
					cliente = dados.rows.item(0);
				}
				
				
				callback(cliente);
			}, function(transaction, error) {
				alert("Error : " + error.message);
			}
		)
	});
	

}


function obter_cliente_pedido(pedido, callback){
	var pedido;
	
	obter_cliente(pedido.pedido_codigo_cliente, pedido.pedido_loja_cliente, pedido.pedido_filial, pedido.empresa, function(cliente){
		
		pedido.cliente = cliente;
		callback(pedido);
		
	});
	
	
	
}


function salvar_pedido_venda_cliente(id_pedido, pedido, callback){
	var campos = [];
	var interrogacoes = [];
	var valores = [];
	
	campos.push('codigo_cliente');		
	interrogacoes.push('?');
	valores.push(pedido.codigo_cliente);
		
	campos.push('codigo_empresa');		
	interrogacoes.push('?');
	valores.push(pedido.dados_cliente.empresa);
		
	campos.push('codigo_filial');		
	interrogacoes.push('?');
	valores.push(pedido.filial);
		
	campos.push('codigo_vendedor');		
	interrogacoes.push('?');
	valores.push(info.cod_rep);
		
	campos.push('data');				
	interrogacoes.push('?');
	valores.push(obterDataAtual());
	
	campos.push('codigo_motivo');		
	interrogacoes.push('?');
	valores.push(null);
	
	campos.push('codigo_pedido');		
	interrogacoes.push('?');
	valores.push(id_pedido);
	
	campos.push('exportado');		
	interrogacoes.push('?');
	valores.push(0);
	
	
	campos.push('editado');		
	interrogacoes.push('?');
	valores.push(0);
	
	campos.push('erro');		
	interrogacoes.push('?');
	valores.push(0);
	
	
	
	db.transaction(function(x) {
		x.executeSql("INSERT INTO pedido_venda (" + campos.join(', ') + ") VALUES ("+ interrogacoes.join(', ') +")", valores, 
			function() 
			{
				callback();				
			}, function(transaction, error) {
				
			
				alert("Error : " + error.message);
			}
			
			);
	});



}


function salvar_nao_venda_cliente(codigo_cliente,  filial_cliente, empresa_cliente,codigo_motivo, callback){
	var campos = [];
	var interrogacoes = [];
	var valores = [];
	
	campos.push('codigo_cliente');		
	interrogacoes.push('?');
	valores.push(codigo_cliente);
		
	campos.push('codigo_empresa');		
	interrogacoes.push('?');
	valores.push(empresa_cliente);
		
	campos.push('codigo_filial');		
	interrogacoes.push('?');
	valores.push(filial_cliente);
		
	campos.push('codigo_vendedor');		
	interrogacoes.push('?');
	valores.push(info.cod_rep);
		
	campos.push('data');				
	interrogacoes.push('?');
	valores.push(obterDataAtual());
	
	campos.push('codigo_motivo');		
	interrogacoes.push('?');
	valores.push(codigo_motivo);
	
	campos.push('codigo_pedido');		
	interrogacoes.push('?');
	valores.push(null);
	
	campos.push('exportado');		
	interrogacoes.push('?');
	valores.push(0);
	
	
	campos.push('editado');		
	interrogacoes.push('?');
	valores.push(0);
	
	campos.push('erro');		
	interrogacoes.push('?');
	valores.push(0);
		
	db.transaction(function(x) {
		x.executeSql("INSERT INTO pedido_venda (" + campos.join(', ') + ") VALUES ("+ interrogacoes.join(', ') +")", valores, 
			function() 
			{
				
				callback();				
			}, function(transaction, error) {
				
				
				alert("Error : " + error.message);
			}
			
			);
	});
	
	
	
}


