package com.developweb.dwfdv.Plugin.macaddress;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.net.wifi.WifiManager;

/**
 * This class echoes a string called from JavaScript.
 */
public class MacAddressPlugin extends CordovaPlugin 
{
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException 
    {
        if (action.equals("getMacAddress")) 
        {
        	String macAddress = this.getMacAddress();        	
            this.retornarMacAddress(macAddress, callbackContext);
            return true;
        }
        return false;
    }

    private void retornarMacAddress(String macAddress, CallbackContext callbackContext) 
    {
        if (macAddress != null && macAddress.length() > 0) 
        { 
            callbackContext.success(macAddress);
        } 
        else 
        {
            callbackContext.error("Espera um argumento não-vazio.");
        }
    }
    
    /**
	* Gets the mac address.
	*
	* @return the mac address
	*/
    private String getMacAddress() 
    {
        String macAddress = null;
        
        WifiManager wm 	= (WifiManager) this.cordova.getActivity().getSystemService(Context.WIFI_SERVICE);
        macAddress		= wm.getConnectionInfo().getMacAddress();

        if (macAddress == null || macAddress.length() == 0) 
        {
            macAddress = "00:00:00:00:00:00";
        }

        return macAddress;
    }
}
