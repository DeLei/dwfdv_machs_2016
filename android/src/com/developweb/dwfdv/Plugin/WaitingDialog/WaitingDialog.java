package com.developweb.dwfdv.Plugin.WaitingDialog;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.LOG;
import org.json.JSONArray;
import org.json.JSONException;
import android.app.ProgressDialog;

public class WaitingDialog extends CordovaPlugin {

	private ProgressDialog waitingDialog = null;

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if ("mostrarDialog".equals(action)) 
		{
			String text = "Por favor aguarde...;";
			try 
			{
				text = args.getString(0);
			} catch (Exception e) 
			{
				LOG.d("WaitingDialog", "Text parameter not valid, using default");
			}
			showWaitingDialog(text);
			callbackContext.success();
			return true;
		} 
		else if ("fecharDialog".equals(action)) 
		{
			hideWaitingDialog();
			callbackContext.success();
			return true;
		}
		return false;
	}

	public void showWaitingDialog(String text) {
		waitingDialog = ProgressDialog.show(this.cordova.getActivity(), "Atenção", text);
		LOG.d("WaitingDialog", "Dialog shown, waiting hide command");
	}

	public void hideWaitingDialog()
	{
		LOG.d("WaitingDialog", "Função hide foi chamada.");
		
		if (waitingDialog != null) 
		{
			waitingDialog.hide();
			waitingDialog.dismiss();
			LOG.d("WaitingDialog", "Dialog dismissed");
			waitingDialog = null;
		} 
		else 
		{
			LOG.d("WaitingDialog", "Nothing to dismiss");
		}
	}

}